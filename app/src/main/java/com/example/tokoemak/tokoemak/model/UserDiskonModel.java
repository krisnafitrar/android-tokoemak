package com.example.tokoemak.tokoemak.model;

public class UserDiskonModel {
    String status;

    public UserDiskonModel() {
    }

    public UserDiskonModel(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
