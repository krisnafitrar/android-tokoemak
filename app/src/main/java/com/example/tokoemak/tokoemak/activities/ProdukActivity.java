package com.example.tokoemak.tokoemak.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.tokoemak.tokoemak.R;
import com.example.tokoemak.tokoemak.adapter.ProdukAdapter;
import com.example.tokoemak.tokoemak.app.AppController;
import com.example.tokoemak.tokoemak.commons.SharedPreferences;
import com.example.tokoemak.tokoemak.databases.Database;
import com.example.tokoemak.tokoemak.model.LokasiModel;
import com.example.tokoemak.tokoemak.model.OrderModel;
import com.example.tokoemak.tokoemak.model.ProdukModel;
import com.example.tokoemak.tokoemak.util.Server;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.rbrooks.indefinitepagerindicator.IndefinitePagerIndicator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import es.dmoral.toasty.Toasty;
import io.supercharge.shimmerlayout.ShimmerLayout;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ProdukActivity extends AppCompatActivity {
    //deklarasi varibel produk
    private String idkategori;
    private TextView jumlahProduk;
    private RecyclerView rvProduk;
    private ArrayList<ProdukModel> modelList;
    public ProdukAdapter mAdapter;

    //variabel request
    private String tag_json_obj = "json_obj_req";
    private String url_produk = Server.URL_PRODUK;

    //variabel lain
    private ImageView backArrow,btnSearch,btnShowFav;
    private TextView lokasiKonsumen,locCurrentUser,locHistoryUser,qtyOnCart,slashCart,priceOnCart;
    private LinearLayout btnFilter,btnSorting;
    private SharedPreferences sharedPreferences;
    private HashMap<String,String> user = new HashMap<String, String>(  );
    private String idUser;
    private double hisLat,hisLong;
    private ShimmerLayout shimmerLayout,shimmerLokasi;
    public RelativeLayout inflateCart;

    //custom font
    private Typeface styleFont;

    //permission
    private static final int REQUEST_CODE = 1;

    //bottom sheet
    private LinearLayout linearLokasi;

    //location
    public double latitude,longitude,tmpLat,tmpLong;
    private String userAddress;
    private String[] street;
    private String[] jalan;
    private String isUsed;
    private String alamat = "";
    private boolean is_used;
    public String city;

    //search
    private EditText edtKeyword;
    private String keyword;
    private RadioGroup rbGroup;
    private ImageView imgSort,imgFilter;
    private TextView titleSort,titleFilter,textVoucherProduk,textUsingVoucher;
    private boolean terendah,tertinggi,relevan;
    private String typeSort = "";
    private boolean found = false;
    private List<OrderModel>cartList = new ArrayList<>();
    private List<OrderModel> totalPrice = new ArrayList<>();
    private Locale localeID = new Locale("in", "ID");
    private NumberFormat formatHarga = NumberFormat.getCurrencyInstance( localeID );
    private List<LokasiModel> lokList = new ArrayList<>();
    private LinearLayout linearLayoutCoupon;


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext( CalligraphyContextWrapper.wrap( newBase ) );
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        CalligraphyConfig.initDefault( new CalligraphyConfig.Builder().setDefaultFontPath( "fonts/roboto.ttf" )
                .setFontAttrId( R.attr.fontPath )
                .build());
        setContentView( R.layout.activity_produk );

        //mencari view pada layout
        loadViews();
        getUserDetail();
        setTypeface();
        onClickMethod();

        //get intent
        if(getIntent() != null){
            idkategori = getIntent().getStringExtra( "idkategori" );
        }
        modelList = new ArrayList<>();
        initProduk("get", "" );
        btnSearch.setEnabled( false );

        btnSearch.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!isNetworkAvailable()){
                    setToastMessage( "Tidak ada koneksi internet" );
                }else{
                    modelList.clear();
                    keyword = edtKeyword.getText().toString().toLowerCase();
                    shimmerLayout.setVisibility( View.VISIBLE );
                    shimmerLayout.startShimmerAnimation();
                    initProduk( "search", "" );
                    if(found == false){
                        shimmerLayout.stopShimmerAnimation();
                        shimmerLayout.setVisibility( View.GONE );
                    }
                }
            }
        } );

        edtKeyword.addTextChangedListener( new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                modelList.clear();
                if(s.length() > 0){
                   btnSearch.setImageResource( R.drawable.ic_search_success_24dp );
                   btnSearch.setEnabled( true );
                }else{
                    btnSearch.setImageResource( R.drawable.ic_search_grey_24dp );
                    btnSearch.setEnabled( false );
                    modelList.clear();
                    shimmerLayout.setVisibility( View.VISIBLE );
                    shimmerLayout.startShimmerAnimation();
                    if(terendah && !tertinggi && !relevan){
                        initProduk( "sort","ASC" );
                    }else if(tertinggi && !terendah && !relevan){
                        initProduk( "sort","DESC" );
                    }else{
                        initProduk("get", "" );
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        } );

    }


    private void onClickMethod() {
        btnShowFav.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( ProdukActivity.this,FavoriteActivities.class );
                intent.putExtra( "lat",tmpLat );
                intent.putExtra( "long", tmpLong );
                startActivity( intent );
            }
        } );

        inflateCart.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( ProdukActivity.this,CartActivity.class );
                intent.putExtra( "lat", tmpLat );
                intent.putExtra( "long", tmpLong );
                startActivity( intent );
            }
        } );

        lokasiKonsumen.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadBottomSheetMaps();
            }
        } );

        //fungsi back
        backArrow.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        } );

        btnFilter.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setToastMessage( "Masih tahap pengembangan" );
            }
        } );

        btnSorting.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadBottomSheetSort();
            }
        } );

    }


    private void getUserDetail() {
        sharedPreferences = new SharedPreferences( this );
        user = sharedPreferences.getUserDetails();
        idUser = user.get( sharedPreferences.KEY_IDUSER );
    }


    private void setTypeface() {
        styleFont = Typeface.createFromAsset( getAssets(),"fonts/Poppins-Medium.ttf" );
        Typeface bFont = Typeface.createFromAsset( getAssets(), "fonts/Poppins-Bold.ttf" );
        jumlahProduk.setTypeface( styleFont );
        lokasiKonsumen.setTypeface( styleFont );
        qtyOnCart.setTypeface( bFont );
        slashCart.setTypeface( bFont );
        priceOnCart.setTypeface( bFont );
        textUsingVoucher.setTypeface( bFont );
        textVoucherProduk.setTypeface( bFont );
    }


    private void loadViews() {
        jumlahProduk = findViewById( R.id.jumlahTotalProduk );
        rvProduk = findViewById( R.id.rv_produk );
        backArrow = findViewById( R.id.backArrowProduk );
        lokasiKonsumen = findViewById( R.id.lokasiKonsumen );
        btnFilter = findViewById( R.id.linearFilter );
        btnSorting = findViewById( R.id.linearSort );
        edtKeyword = findViewById( R.id.keywordSearchProduk );
        btnSearch = findViewById( R.id.btnSearchBarangKategori );
        imgSort = findViewById( R.id.imgSort );
        imgFilter = findViewById( R.id.imgFilter );
        titleSort = findViewById( R.id.textSort );
        titleFilter = findViewById( R.id.textFilter );
        shimmerLayout = findViewById( R.id.shimmerLayout );
        shimmerLokasi = findViewById( R.id.shimmerLokasi );
        inflateCart = findViewById( R.id.inflateCart );
        qtyOnCart = findViewById( R.id.qtyOnCart );
        slashCart = findViewById( R.id.slashCart );
        priceOnCart = findViewById( R.id.priceOnCart );
        btnShowFav = findViewById( R.id.btnShowFav );
        linearLayoutCoupon = findViewById( R.id.linearLayoutCouponProduk );
        textVoucherProduk = findViewById( R.id.textVoucherProduk );
        textUsingVoucher = findViewById( R.id.textUsingVoucherProduk );

    }


    private void loadBottomSheetSort() {
        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog( ProdukActivity.this );
        bottomSheetDialog.setContentView( R.layout.layout_sorting );
        bottomSheetDialog.setCanceledOnTouchOutside( true );

        rbGroup = bottomSheetDialog.findViewById( R.id.rbGroup );
        TextView title = bottomSheetDialog.findViewById( R.id.titleUrutkan );
        final RadioButton rbTerendah = bottomSheetDialog.findViewById( R.id.rbTerendah );
        final RadioButton rbTertinggi = bottomSheetDialog.findViewById( R.id.rbTertinggi );
        final RadioButton rbRelevansi = bottomSheetDialog.findViewById( R.id.rbRelevansi );


        if(terendah && !tertinggi && !relevan){
            rbTerendah.setChecked( true );
        }else if(tertinggi && !terendah && !relevan){
            rbTertinggi.setChecked( true );
        }else{
            rbRelevansi.setChecked( true );
        }

        title.setTypeface( styleFont );
        rbTerendah.setTypeface( styleFont );
        rbTertinggi.setTypeface( styleFont );
        rbRelevansi.setTypeface( styleFont );

        rbGroup.setOnCheckedChangeListener( new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                doSwitchRadio(checkedId);
            }
        } );

        bottomSheetDialog.show();
    }


    private void doSwitchRadio(int checkedId) {
        switch (checkedId){
            case R.id.rbTerendah:
                terendah = true;
                tertinggi = false;
                relevan = false;
                btnSorting.setBackgroundResource( R.drawable.btn_filter_active );
                titleSort.setTextColor( Color.WHITE );
                imgSort.setImageResource( R.drawable.ic_import_export_white_24dp );
                modelList.clear();
                shimmerLayout.setVisibility( View.VISIBLE );
                shimmerLayout.startShimmerAnimation();
                initProduk( "sort","ASC" );
                break;
            case R.id.rbTertinggi:
                tertinggi = true;
                terendah = false;
                relevan = false;
                btnSorting.setBackgroundResource( R.drawable.btn_filter_active );
                titleSort.setTextColor( Color.WHITE );
                imgSort.setImageResource( R.drawable.ic_import_export_white_24dp );
                modelList.clear();
                shimmerLayout.setVisibility( View.VISIBLE );
                shimmerLayout.startShimmerAnimation();
                initProduk( "sort","DESC" );
                break;
            case R.id.rbRelevansi:
                relevan = true;
                terendah = false;
                tertinggi = false;
                btnSorting.setBackgroundResource( R.drawable.btn_produk_filter );
                titleSort.setTextColor( Color.BLACK );
                imgSort.setImageResource( R.drawable.ic_import_export_black_24dp );
                modelList.clear();
                shimmerLayout.setVisibility( View.VISIBLE );
                shimmerLayout.startShimmerAnimation();
                initProduk( "get","" );
                break;

        }
    }


    private void initProduk(String action, String type) {
        //load produk
        shimmerLayout.startShimmerAnimation();
        loadProduk(idkategori,action,type);
        rvProduk.setLayoutManager( new GridLayoutManager( ProdukActivity.this,2 ) );
        rvProduk.setHasFixedSize( true );
        mAdapter = new ProdukAdapter( ProdukActivity.this,modelList );
        rvProduk.setAdapter( mAdapter );
        rvProduk.setNestedScrollingEnabled( false );
    }


    private void loadBottomSheetMaps()
    {
        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog( ProdukActivity.this );
        bottomSheetDialog.setContentView( R.layout.layout_bottom_sheet_maps );
        bottomSheetDialog.setCanceledOnTouchOutside( true );

        LinearLayout btnPilihPeta = bottomSheetDialog.findViewById( R.id.linearPilihPeta );
        LinearLayout btnLokasiSekarang = bottomSheetDialog.findViewById( R.id.linearLokasiSekarang );
        LinearLayout btnLokasiHistory = bottomSheetDialog.findViewById( R.id.linearLokasiHistory );

        TextView textPilihLokasi = bottomSheetDialog.findViewById( R.id.textPilihLokasi );
        TextView textPilihPeta = bottomSheetDialog.findViewById( R.id.textPilihPeta );
        TextView textLokasimu = bottomSheetDialog.findViewById( R.id.textLokasimu );
        TextView textLokasiHistory = bottomSheetDialog.findViewById( R.id.textlokasiHistory );

        locCurrentUser = bottomSheetDialog.findViewById( R.id.locCurrentUser );
        locHistoryUser = bottomSheetDialog.findViewById( R.id.locHistoryUser );

        //set typeface (font style)
        textPilihLokasi.setTypeface( styleFont );
        textPilihPeta.setTypeface( styleFont );
        textLokasimu.setTypeface( styleFont );
        textLokasiHistory.setTypeface( styleFont );

        //setLokasi
        locCurrentUser.setText( userAddress.substring( 0,59 ) + " ..." );

        if(alamat.equals( "" )){
            textLokasiHistory.setText( street[0] );
            locHistoryUser.setText( userAddress.substring( 0,59 ) + " ..." );
        }else{
            textLokasiHistory.setText( jalan[0] );
            locHistoryUser.setText( alamat.substring( 0,59 ) + " ..." );
        }


        btnPilihPeta.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( ProdukActivity.this, MapsActivity.class );
                intent.putExtra( "lat",tmpLat );
                intent.putExtra( "long", tmpLong );
                startActivity( intent );
                bottomSheetDialog.dismiss();
            }
        } );

        btnLokasiSekarang.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    new Database( ProdukActivity.this ).setIsUsed( idUser,0 );
                    is_used = false;
                    tmpLat = latitude;
                    tmpLong = longitude;
                    lokasiKonsumen.setText( street[0] );
                    setToastMessage( "Berhasil menetapkan lokasi" );
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        } );

        btnLokasiHistory.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    new Database( ProdukActivity.this ).setIsUsed( idUser,1 );
                    is_used = true;
                    tmpLat = hisLat;
                    tmpLong = hisLong;
                    lokasiKonsumen.setText( jalan[0] );
                    setToastMessage( "Berhasil menetapkan lokasi" );
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        } );

        bottomSheetDialog.show();
    }

    private boolean checkPermission()
    {
        int permissionState = ContextCompat.checkSelfPermission( ProdukActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION );
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission()
    {
        boolean shouldRationale = ActivityCompat.shouldShowRequestPermissionRationale( this,Manifest.permission.ACCESS_FINE_LOCATION );

        if(shouldRationale){
            ActivityCompat.requestPermissions( this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION}
                    , REQUEST_CODE);
        }
    }

    private void loadProduk(final String id, final String actt, final String type) {
        final String kode = id;
        String urlWithParams = Server.REST_API_PRODUK + "?idkategori=" + kode + "&act=" + actt + "&keyword=" + keyword + "&typesort=" + type;
        jumlahProduk.setText( "Produk tidak ditemukan" );
        linearLayoutCoupon.setVisibility( View.GONE );
        StringRequest stringRequest = new StringRequest( Request.Method.GET, urlWithParams, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    JSONArray jsonArray = new JSONArray( response );
                    for(int i = 0; i < jsonArray.length(); i++){
                        try{
                            JSONObject data = jsonArray.getJSONObject( i );
                            ProdukModel model = new ProdukModel();
                            model.setKode_barang( data.getString( "idbarang" ) );
                            model.setNama( data.getString( "namabarang" ) );
                            model.setJenis( data.getString("namajenis"));
                            model.setHarga( data.getString( "harga_jual" ) );
                            model.setStok( data.getString( "stok" ) );
                            model.setGambar(data.getString("gambar"));
                            model.setDeskripsi( data.getString( "deskripsi" ) );
                            model.setDiskon( data.getString( "diskon" ) );
                            modelList.add(model);
                            jumlahProduk.setText( jsonArray.length() + " Produk Ditemukan" );
                            found = true;
                            shimmerLayout.stopShimmerAnimation();
                            shimmerLayout.setVisibility( View.GONE );
                            detectCoupon();
//                            if(getIntent() != null & getIntent().getBooleanExtra( "isDiskon",false ) == true){
//                                textVoucherProduk.setText( "Diskon " + getIntent().getStringExtra( "nominalDiskon" ) + " terpasang" );
//                                linearLayoutCoupon.setVisibility( View.VISIBLE );
//                            }else{
//                                linearLayoutCoupon.setVisibility( View.GONE );
//                            }
                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                    mAdapter.notifyDataSetChanged();
                }catch (JSONException exception){
                    exception.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d( "volley", "error : " + error.getMessage() );
            }
        } );

        AppController.getInstance().addToRequestQueue(stringRequest,tag_json_obj);
    }


    private void detectCoupon(){
        linearLayoutCoupon.setVisibility( View.GONE );
        String iduser = user.get( sharedPreferences.KEY_IDUSER );
        String urlWithParams = Server.REST_API_PROMO + "?iduser=" + iduser + "&act=get";
        StringRequest stringRequest = new StringRequest( Request.Method.GET, urlWithParams, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject data = new JSONObject( response );
                    String jumlahDiskon = formatHarga.format( Integer.parseInt( data.getString( "diskon" ) ) );
                    String[] diskonVoucher = jumlahDiskon.split( "\\." );
                    textVoucherProduk.setText( "Diskon " + diskonVoucher[0] + "rb" + " terpasang" );
                    linearLayoutCoupon.setVisibility( View.VISIBLE );
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        } );

        AppController.getInstance().addToRequestQueue( stringRequest,"json_obj_req" );
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case REQUEST_CODE:{
                //jika request di cancel, array nya kosong
                if(grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    //request permission di izinkan oleh user
                }else{
                    //request tidak diizinkan dan matikan fungsi yang berkaitan
                    requestPermission();
                }
                return;
            }
        }

    }


    @Override
    protected void onStart() {
        super.onStart();
    }


    @Override
    protected void onResume() {
        super.onResume();
        if(!checkPermission()){
            requestPermission();
            onBackPressed();
            setToastMessage( "Izinkan akses lokasi" );
        }else{
            //panggil fungsi untuk mengatur lokasi
            shimmerLokasi.startShimmerAnimation();
            setAddressOfUser();
            checkLocation();
            getHistoryLoc();
            setVisibilityCart();
            checkConnection();
            detectCoupon();
        }
    }


    private void setAddressOfUser() {
        List<LokasiModel> getStatus = new ArrayList<>();

        getStatus = new Database( this ).getLocation();

        if(getStatus.size() > 0){
            if(getStatus.get( 0 ).getIsUsed() > 0){
                is_used = true;
            }else{
                is_used = false;
            }
        }else{
            is_used = false;
        }
    }


    private void setVisibilityCart() {
        if(checkCart() > 0){
            inflateCart.setVisibility( View.VISIBLE );
            setValueCart();
        }else{
            inflateCart.setVisibility( View.GONE );
        }
    }


    private void checkConnection() {
        if(isNetworkAvailable()){
            shimmerLokasi.stopShimmerAnimation();
            shimmerLokasi.setVisibility( View.GONE );
            lokasiKonsumen.setVisibility( View.VISIBLE );
            lokasiKonsumen.setEnabled( true );
        }else{
            lokasiKonsumen.setEnabled( false );
        }
    }


    public void setValueCart()
    {
        qtyOnCart.setText( checkCart() + " item" );
        priceOnCart.setText( formatHarga.format(getTotalPrice()) + " (est)" );
    }


    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    @Override
    protected void onStop() {
        super.onStop();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    private void checkLocation()
    {
        LocationManager locManager = (LocationManager) getSystemService( Context.LOCATION_SERVICE );

        if(locManager.isProviderEnabled( LocationManager.GPS_PROVIDER )){
            if (ActivityCompat.checkSelfPermission( this, Manifest.permission.ACCESS_FINE_LOCATION ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission( this, Manifest.permission.ACCESS_COARSE_LOCATION ) != PackageManager.PERMISSION_GRANTED) {
                onBackPressed();
            }else{
                //perizinan di izinkan oleh user
                getCurrentAddress();
            }
        }else{
            setToastMessage( "Aktifkan GPS anda" );
            onBackPressed();
        }

    }


    private void getCurrentAddress()
    {
        LocationManager locationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
        FusedLocationProviderClient fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        if(ActivityCompat.checkSelfPermission( this, Manifest.permission.ACCESS_FINE_LOCATION ) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission( this, Manifest.permission.ACCESS_COARSE_LOCATION ) != PackageManager.PERMISSION_GRANTED){
            return;
        }else{
            fusedLocationProviderClient.getLastLocation()
                    .addOnSuccessListener( new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            if(location != null){
                                //arahkan ke fungsi ini dengan parameter location
                                setUserAddress(location);
                            }
                        }
                    } ).addOnFailureListener( new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    setToastMessage( e.getMessage() );
                }
            } );

            LocationListener locationListener = new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {

                }

                @Override
                public void onStatusChanged(String provider, int status, Bundle extras) {

                }

                @Override
                public void onProviderEnabled(String provider) {

                }

                @Override
                public void onProviderDisabled(String provider) {

                }
            };

            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,2000,10,locationListener);
        }
    }


    private void setUserAddress(Location location) {
        latitude = location.getLatitude();
        longitude = location.getLongitude();

        Geocoder userCurrentLocation = new Geocoder( this, Locale.getDefault() );
        List<Address>addressList;

        try{
            addressList = userCurrentLocation.getFromLocation( latitude,longitude,1 );
            userAddress = addressList.get( 0 ).getAddressLine( 0 );
            street = userAddress.split( "," );
            if(!is_used){
                lokasiKonsumen.setText( street[0] );
                tmpLat = latitude;
                tmpLong = longitude;
            }
        }catch (NullPointerException e){
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



    private void getHistoryLoc()
    {
        lokList = new Database( ProdukActivity.this ).getLocation();
        if(lokList.size() > 0){
            setHistoryLoc( Double.valueOf( lokList.get( 0 ).getLatitude() ),Double.valueOf( lokList.get( 0 ).getLongitude() ) );
        }
    }


    private void setHistoryLoc(double hisLat, double hisLong) {
        Geocoder historyLoc = new Geocoder( this,Locale.getDefault() );
        List<Address>addresses;

        try{
            addresses = historyLoc.getFromLocation( hisLat,hisLong,1 );
            alamat = addresses.get( 0 ).getAddressLine( 0 );
            jalan = alamat.split( "," );
            this.hisLat = hisLat;
            this.hisLong = hisLong;
            if(is_used){
                lokasiKonsumen.setText( jalan[0] );
                tmpLat = hisLat;
                tmpLong = hisLong;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void setToastMessage(String msg)
    {
        Toast toasty = Toasty.normal( this, msg);
        toasty.setGravity( Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0 );
        toasty.show();
    }


    public int checkCart()
    {
        int x = 0;

        cartList = new Database( ProdukActivity.this ).getCarts();

        for (OrderModel orderModel:cartList)
            x += Integer.parseInt( orderModel.getJumlah() );

        return x;

    }


    public int getTotalPrice()
    {
        int x = 0;

        totalPrice = new Database( ProdukActivity.this ).getCarts();

        for (OrderModel model:totalPrice)
            x += (Integer.parseInt( model.getHarga() ) * Integer.parseInt( model.getJumlah() ));

        return x;
    }

}
