package com.example.tokoemak.tokoemak.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.tokoemak.tokoemak.R;
import com.example.tokoemak.tokoemak.activities.CartActivity;
import com.example.tokoemak.tokoemak.activities.ItemActivity;
import com.example.tokoemak.tokoemak.activities.ProdukActivity;
import com.example.tokoemak.tokoemak.fragments.HomeFragment;
import com.example.tokoemak.tokoemak.model.MenuKategoriModel;

import java.util.ArrayList;

public class MenuKategoriAdapter extends RecyclerView.Adapter<MenuKategoriAdapter.ViewProcessHolder> {
    private HomeFragment mContext;
    private ArrayList<MenuKategoriModel>mList;

    public MenuKategoriAdapter(HomeFragment mContext, ArrayList<MenuKategoriModel> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @NonNull
    @Override
    public ViewProcessHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from( mContext.getActivity() ).inflate( R.layout.kategori_menu,viewGroup,false );
        return new ViewProcessHolder( view );
    }

    @Override
    public void onBindViewHolder(@NonNull ViewProcessHolder viewProcessHolder, int i) {
        ImageView gambar;
        TextView text;
        LinearLayout layoutMenu;
        final MenuKategoriModel menuKategoriModel = mList.get( i );

        gambar = viewProcessHolder.gambarMenu;
        text = viewProcessHolder.textMenu;
        layoutMenu = viewProcessHolder.linearLayout;

        Glide.with( mContext )
                .load( menuKategoriModel.getGambar() )
                .into( gambar );
        text.setText( menuKategoriModel.getNama() );

        layoutMenu.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( mContext.getActivity(),ProdukActivity.class );
                intent.putExtra( "idkategori",menuKategoriModel.getKode() );
                mContext.startActivity( intent );
            }
        } );


    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewProcessHolder extends RecyclerView.ViewHolder {
        ImageView gambarMenu;
        TextView textMenu;
        LinearLayout linearLayout;
        public ViewProcessHolder(@NonNull View itemView) {
            super( itemView );
            gambarMenu = itemView.findViewById( R.id.gambarIcon );
            textMenu = itemView.findViewById( R.id.namaIcon );
            linearLayout = itemView.findViewById( R.id.linearMenu );
        }
    }
}
