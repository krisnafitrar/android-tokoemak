package com.example.tokoemak.tokoemak.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.tokoemak.tokoemak.R;
import com.example.tokoemak.tokoemak.app.AppController;
import com.example.tokoemak.tokoemak.commons.SharedPreferences;
import com.example.tokoemak.tokoemak.databases.Database;
import com.example.tokoemak.tokoemak.model.LokasiModel;
import com.example.tokoemak.tokoemak.util.Server;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.location.places.ui.SupportPlaceAutocompleteFragment;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import es.dmoral.toasty.Toasty;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private ImageView backArrow;
    private TextView lokasiPengiriman;
    private TextView lokasi;
    private double latitudeku,longitudeku;
    private LatLng lokasiUser;
    private SupportMapFragment mapFragment;
    private List<LatLng> mListPoint;
    private Places places;
    private SharedPreferences sharedPreferences;
    private List<Address> mList,addressList;
    private Geocoder myLocation;
    private Button setLokasi;
    private String lat,longi;
    private ConnectivityManager connectivityManager;
    private LocationManager locationManager;
    private double newLat,newLong;
    private HashMap<String,String> user = new HashMap<String, String>(  );
    private FirebaseDatabase db = FirebaseDatabase.getInstance();
    private String nomorUser;
    private HashMap<Float,Float> jarakUser = new HashMap<Float, Float>(  );
    private String wilayah;
    private String []jalan;
    private String idUser;
    private List<LokasiModel>arrayList = new ArrayList<>();

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext( CalligraphyContextWrapper.wrap( newBase ) );
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        CalligraphyConfig.initDefault( new CalligraphyConfig.Builder().setDefaultFontPath( "fonts/Poppins-Regular.ttf" )
                .setFontAttrId( R.attr.fontPath )
                .build());
        setContentView( R.layout.activity_maps );
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById( R.id.map );
        mapFragment.getMapAsync( this );

        //init view
//        searchLocation = findViewById( R.id.linearSearch )
        backArrow = findViewById( R.id.backArrowMaps );

        mListPoint = new ArrayList<>(  );
        connectivityManager = (ConnectivityManager) getSystemService( Context.CONNECTIVITY_SERVICE );

        //init view
        initViews();

        // Initialize sharedpref
        sharedPreferences = new SharedPreferences( this );
        myLocation = new Geocoder( this, Locale.getDefault() );
        user = sharedPreferences.getUserDetails();
        jarakUser = sharedPreferences.getJarak();
        nomorUser = user.get( sharedPreferences.KEY_NUMBER );
        idUser = user.get( sharedPreferences.KEY_IDUSER );

        if (!com.google.android.libraries.places.api.Places.isInitialized()) {
            com.google.android.libraries.places.api.Places.initialize(getApplicationContext(), getString(R.string.google_maps_key), Locale.US);
        }

        // Initialize the AutocompleteSupportFragment.
        AutocompleteSupportFragment autocompleteFragment = (AutocompleteSupportFragment)
                getSupportFragmentManager().findFragmentById(R.id.autocomplete_fragment);

// Specify the types of place data to return.
        autocompleteFragment.setPlaceFields(Arrays.asList( com.google.android.libraries.places.api.model.Place.Field.ID, com.google.android.libraries.places.api.model.Place.Field.NAME));

// Set up a PlaceSelectionListener to handle the response.
        autocompleteFragment.setOnPlaceSelectedListener( new com.google.android.libraries.places.widget.listener.PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(@NonNull com.google.android.libraries.places.api.model.Place place) {

            }

            @Override
            public void onError(@NonNull Status status) {

            }
        } );


        backArrow.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        } );

        if(getIntent() != null){
          latitudeku = getIntent().getDoubleExtra( "lat", 0 );
          longitudeku = getIntent().getDoubleExtra( "long", 0 );
          lat = String.valueOf( latitudeku );
          longi = String.valueOf( longitudeku );
        }

        try {
            String alamatUserNow;
            addressList = myLocation.getFromLocation( latitudeku,longitudeku, 1 );
            alamatUserNow = addressList.get( 0 ).getAddressLine( 0 );
            jalan = alamatUserNow.split( "," );
            lokasiPengiriman.setText( jalan[0] );
            lokasi.setText( alamatUserNow.substring( 0,69 ) + " ...");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void initViews() {
        lokasi = findViewById( R.id.lokUser );
        setLokasi = findViewById( R.id.btnSetLokasi );
        lokasiPengiriman = findViewById( R.id.textLokasiPengiriman );
        TextView textTitle = findViewById( R.id.textPilihLewatPeta );
        Typeface typeface = Typeface.createFromAsset( getAssets(), "fonts/Poppins-Medium.ttf" );
        lokasiPengiriman.setTypeface( typeface );
        textTitle.setTypeface( typeface );
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult( requestCode, resultCode, data );
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        lokasiUser = new LatLng( latitudeku,longitudeku );

        //custom map
        googleMap.setMapStyle( new MapStyleOptions( getResources().getString(R.string.style_json) ) );


        if(mListPoint.size() == 0){
            mMap.addMarker( new MarkerOptions().position( lokasiUser ).title( "Lokasi user" ) )
                    .setIcon( BitmapDescriptorFactory.fromResource( R.drawable.locationpin ) );
        }


        mMap.setOnMapClickListener( new GoogleMap.OnMapClickListener() {
            MarkerOptions marker = new MarkerOptions();
            @Override
            public void onMapClick(LatLng latLng) {
                List<Address> addressList;
                newLat = latLng.latitude;
                newLong = latLng.longitude;

                double latPoint;
                double longPoint;
                double jarakMeter;
                double jarakKilo;
                String alamatUser;

                latPoint = -7.1460516;
                longPoint = 111.8768421;


                Location startPoint = new Location( "startPoint" );
                startPoint.setLatitude( latPoint );
                startPoint.setLongitude( longPoint );

                Location endPoint = new Location( "endPoint" );
                endPoint.setLatitude( newLat );
                endPoint.setLongitude( newLong );

                jarakMeter = startPoint.distanceTo( endPoint );
                jarakKilo = jarakMeter / 1000;
                DecimalFormat df = new DecimalFormat( "#.#" );
                DecimalFormatSymbols dfs = DecimalFormatSymbols.getInstance();
                dfs.setDecimalSeparator( '.' );
                df.setDecimalFormatSymbols( dfs );

                try {
                    addressList = myLocation.getFromLocation( newLat,newLong, 1 );
                    wilayah = addressList.get( 0 ).getLocality();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if(wilayah.equals( "Kecamatan Bojonegoro" )){

                    if(mMap != null){
                        mMap.clear();
                    }
                        if(mListPoint.size() == 1){
                            mListPoint.clear();
                        }
                        mListPoint.add( latLng );
                        marker.position( latLng );

                        if(mListPoint.size() == 1){
                            marker.icon( BitmapDescriptorFactory.fromResource( R.drawable.locationpin ) );
                        }

                        mMap.addMarker( marker );

                        lat = String.valueOf( newLat );
                        longi = String.valueOf( newLong );

                        try {
                            mList = myLocation.getFromLocation( newLat,newLong, 1 );
                            alamatUser = mList.get( 0 ).getAddressLine( 0 );
                            jalan = alamatUser.split( "," );
                            lokasiPengiriman.setText( jalan[0] );
                            lokasi.setText( alamatUser.substring( 0,69 ) + " ..." );
                            sharedPreferences.setJarak( Float.valueOf( df.format( jarakKilo ) ) );
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }else{
                        setToastMessage( "Wilayah diluar jangkauan" );
                    }
            }
        } );

        locationManager = (LocationManager)getSystemService( Context.LOCATION_SERVICE );

        setLokasi.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(locationManager.isProviderEnabled( LocationManager.GPS_PROVIDER )){
                    setUserLocation();
                }else{
                    Toast.makeText( MapsActivity.this, "Mohon nyalakan GPS anda!", Toast.LENGTH_LONG ).show();
                }
            }
        } );

        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom( lokasiUser,15 ),2000,null);
    }


    private void setUserLocation() {
        LokasiModel lokasiModel = new LokasiModel();
        lokasiModel.setIduser( idUser );
        lokasiModel.setLatitude( lat );
        lokasiModel.setLongitude( longi );
        lokasiModel.setIsUsed( 0 );

        arrayList = new Database( MapsActivity.this ).getLocation();

        try{
            if(arrayList.size() < 1){
                new Database( MapsActivity.this ).addLocation( lokasiModel );
                setToastMessage( "Berhasil menetapkan lokasi" );
            }else{
                new Database( MapsActivity.this ).updateLatLong( idUser,lat,longi );
                setToastMessage( "Berhasil merubah lokasi" );
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    private void setToastMessage(String msg)
    {
        Toast toasty = Toasty.normal( this, msg);
        toasty.setGravity( Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0 );
        toasty.show();
    }

}
