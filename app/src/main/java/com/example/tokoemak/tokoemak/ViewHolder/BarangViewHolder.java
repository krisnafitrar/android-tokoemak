package com.example.tokoemak.tokoemak.ViewHolder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.example.tokoemak.tokoemak.Interface.ItemClickListener;
import com.example.tokoemak.tokoemak.R;

public class BarangViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    public ImageView gambar;
    public TextView namaBarang, hargaBarang;
    private ItemClickListener itemClickListener;
    public Button btnLihat;

    public BarangViewHolder(@NonNull View itemView) {
        super( itemView );
        gambar = (ImageView) itemView.findViewById( R.id.img_itemBarang );
        namaBarang = (TextView) itemView.findViewById( R.id.nama_itemBarang );
        hargaBarang = (TextView) itemView.findViewById( R.id.harga_barang );
        btnLihat = (Button)itemView.findViewById( R.id.btnLihatDetail );
        itemView.setOnClickListener( this );
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public void onClick(View v) {
        itemClickListener.onClick( v , getAdapterPosition(), false );
    }
}
