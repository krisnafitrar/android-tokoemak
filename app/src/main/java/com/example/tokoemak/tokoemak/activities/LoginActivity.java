package com.example.tokoemak.tokoemak.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.tokoemak.tokoemak.R;
import com.example.tokoemak.tokoemak.app.AppController;
import com.example.tokoemak.tokoemak.commons.CurrentUser;
import com.example.tokoemak.tokoemak.commons.SharedPreferences;
import com.example.tokoemak.tokoemak.model.UserModel;
import com.example.tokoemak.tokoemak.util.Server;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import dmax.dialog.SpotsDialog;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView buatAkun, titleLogin;
    private Button btnLogin;
    private RelativeLayout root;
    private SharedPreferences sharedPreferences;
    private String phoneku;
    private boolean status;
    private String url = Server.REST_API_USERS;
    private ConnectivityManager connectivityManager;
    private static final String TAG = LoginActivity.class.getSimpleName();
    private static final String TAG_MESSAGE = "message";
    private String tag_json_obj = "json_obj_req";
    private MaterialEditText phone;
    private ImageView btnBack,btnHelp;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext( CalligraphyContextWrapper.wrap( newBase ) );
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        CalligraphyConfig.initDefault( new CalligraphyConfig.Builder().setDefaultFontPath( "fonts/Poppins-Regular.ttf" )
                .setFontAttrId( R.attr.fontPath )
                .build());
        setContentView( R.layout.activity_login );

        connectivityManager = (ConnectivityManager) getSystemService( Context.CONNECTIVITY_SERVICE );
        {
            if (connectivityManager.getActiveNetworkInfo() != null
                    && connectivityManager.getActiveNetworkInfo().isConnected()
                    && connectivityManager.getActiveNetworkInfo().isAvailable()) {

            } else {
                Toast.makeText( this, "Akses internet tidak aktif", Toast.LENGTH_LONG ).show();
            }
        }

        //init
        phone = findViewById( R.id.textBoxPhoneSignIn );
        titleLogin = findViewById( R.id.title_login );
        Typeface style = Typeface.createFromAsset( getAssets(), "fonts/Poppins-Medium.ttf" );
        titleLogin.setTypeface( style );
        buatAkun = findViewById( R.id.text_BuatAkun );
        btnBack = findViewById( R.id.btnBackLogin );
        btnHelp = findViewById( R.id.btnHelp );
        buatAkun.setOnClickListener( this );
        btnLogin = findViewById( R.id.btn_login );
        btnLogin.setOnClickListener( this );
        root = findViewById( R.id.root_login );

        btnLogin.setTypeface( style );
        btnLogin.setEnabled( false );
        btnLogin.setBackgroundResource( R.color.light_gray );

        phone.addTextChangedListener( new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() > 0){
                    btnLogin.setBackgroundResource( R.color.colorAccent );
                    btnLogin.setEnabled( true );
                }else{
                    btnLogin.setBackgroundResource( R.color.light_gray );
                    btnLogin.setEnabled( false );
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        } );

        btnHelp.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText( LoginActivity.this, "Login harus pakai nomor HP", Toast.LENGTH_SHORT ).show();
            }
        } );

        btnBack.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        } );

        sharedPreferences = new SharedPreferences( this );

        if(sharedPreferences.isLoggedIn()){
            sharedPreferences.checkLogin();
            finish();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.text_BuatAkun:
                startActivity( new Intent( LoginActivity.this, SignUpActivity.class ) );
                break;
            case R.id.btn_login:
                if (connectivityManager.getActiveNetworkInfo() != null
                        && connectivityManager.getActiveNetworkInfo().isConnected()
                        && connectivityManager.getActiveNetworkInfo().isAvailable()) {
                    showLogin();
                } else {
                    Toast.makeText( this, "Akses internet tidak aktif", Toast.LENGTH_LONG ).show();
                }
                break;
        }
    }

    private void showLogin() {
        phoneku = phone.getText().toString();
        final android.app.AlertDialog alertDialog = new SpotsDialog( LoginActivity.this );

        if(TextUtils.isEmpty( phone.getText().toString() )) {
            phone.setError( "Isikan no handphone " );
            phone.requestFocus();
            Snackbar.make( root, "Isikan no handphone terlebih dahulu", Snackbar.LENGTH_SHORT ).show();
            return;
        } else {
            checkLogin(phoneku);
        }
    }

    private String getTanggal() {
        DateFormat dateFormat = new SimpleDateFormat("dd MMMM YYYY");
        Date date = new Date();

        return dateFormat.format(date);
    }

    private String getWaktu() {
        DateFormat dateFormat = new SimpleDateFormat("HH:mm");
        Date date = new Date();
        return dateFormat.format(date);
    }

    private void checkLogin(final String phoneku) {
        final android.app.AlertDialog alertDialog = new SpotsDialog( LoginActivity.this );
        alertDialog.show();
        String urlwithparams = url + "?nomor=0" + phoneku;
        StringRequest stringRequest = new StringRequest( Request.Method.GET, urlwithparams,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        alertDialog.dismiss();
                        try{
                            JSONObject jsonObject = new JSONObject( response );
                            status = jsonObject.getBoolean( "status" );
                            //check for error in JSON
                            if(status == true){
                                String idUser,nama,email;
                                idUser = jsonObject.getString( "id" );
                                nama = jsonObject.getString( "nama" );
                                email = jsonObject.getString( "email" );
                                phone.setText( "" );
//                                sharedPreferences.createLoginSession( idUser,nama, "0" + phoneku,email,getTanggal() + "," + getWaktu() );
                                Intent intent = new Intent( LoginActivity.this, VerifikasiActivity.class );
                                intent.putExtra( "nomortlp","0" + phoneku );
                                intent.putExtra( "iduser",idUser );
                                intent.putExtra( "nama",nama );
                                intent.putExtra( "email",email );
                                startActivity( intent );
                                finish();
                            }else{
                                Toast.makeText(getApplicationContext(),
                                        jsonObject.getString(TAG_MESSAGE), Toast.LENGTH_LONG).show();
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, "Login Error: " + error.getMessage());
                        Toast.makeText(getApplicationContext(),
                                error.getMessage(), Toast.LENGTH_LONG).show();

                        alertDialog.dismiss();
                    }
                } );
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(stringRequest, tag_json_obj);
    }

}
