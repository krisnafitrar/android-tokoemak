package com.example.tokoemak.tokoemak.ViewHolder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.tokoemak.tokoemak.Interface.ItemClickListener;
import com.example.tokoemak.tokoemak.R;
import com.varunest.sparkbutton.SparkButton;

public class FiturViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private ItemClickListener itemClickListener;
    public ImageView imgFitur;
    public SparkButton likeFitur;
    public TextView namaFitur,descFitur, orderNow;

    public FiturViewHolder(@NonNull View itemView) {
        super( itemView );
        imgFitur = itemView.findViewById( R.id.imgFitur );
        namaFitur = itemView.findViewById( R.id.namaFitur );
        descFitur = itemView.findViewById( R.id.desFitur );
        orderNow = itemView.findViewById( R.id.orderNow );
        likeFitur = itemView.findViewById( R.id.likeFitur );
        itemView.setOnClickListener( this );
    }




    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public void onClick(View v) {
        itemClickListener.onClick( v , getAdapterPosition(), false );
    }
}
