package com.example.tokoemak.tokoemak.model;

public class BannerModel {
    String gambar;

    public BannerModel() {
    }

    public BannerModel(String gambar) {
        this.gambar = gambar;
    }

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }
}
