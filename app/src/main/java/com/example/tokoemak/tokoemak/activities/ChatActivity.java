package com.example.tokoemak.tokoemak.activities;

import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tokoemak.tokoemak.Interface.ItemClickListener;
import com.example.tokoemak.tokoemak.R;
import com.example.tokoemak.tokoemak.ViewHolder.ChatViewHolder;
import com.example.tokoemak.tokoemak.commons.SharedPreferences;
import com.example.tokoemak.tokoemak.model.ChatModel;
import com.example.tokoemak.tokoemak.model.UserModel;
import com.example.tokoemak.tokoemak.service.ListenChat;
import com.example.tokoemak.tokoemak.service.ListenOrder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import es.dmoral.toasty.Toasty;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ChatActivity extends AppCompatActivity {
    private TextView btnSend;
    private Toolbar toolbar;
    private FirebaseDatabase db;
    private DatabaseReference reference;
    private SharedPreferences sharedPreferences;
    private HashMap<String,String> userDetail = new HashMap<String,String>();
    private String nomorUser;
    private EditText editText;
    private FirebaseRecyclerAdapter<ChatModel,ChatViewHolder> adapter;
    private FirebaseRecyclerOptions<ChatModel>options;
    private RecyclerView recyclerView;
    private Typeface typeface;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ScrollView scrollView;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext( CalligraphyContextWrapper.wrap( newBase ) );
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        CalligraphyConfig.initDefault( new CalligraphyConfig.Builder().setDefaultFontPath( "fonts/Poppins-Regular.ttf" )
                .setFontAttrId( R.attr.fontPath )
                .build());
        setContentView( R.layout.activity_chat );

        //init view
        scrollView = findViewById( R.id.scrollChat );
        typeface = Typeface.createFromAsset( getAssets(), "fonts/Poppins-Medium.ttf" );
        btnSend = findViewById( R.id.btnSendMessage );
        btnSend.setTypeface( typeface );
        swipeRefreshLayout = findViewById( R.id.swipeRefresh );

        sharedPreferences = new SharedPreferences( this );
        userDetail = sharedPreferences.getUserDetails();
        nomorUser = userDetail.get( sharedPreferences.KEY_NUMBER );
        editText = findViewById( R.id.messageUser );
        recyclerView = findViewById( R.id.rv_chat );

        toolbar = findViewById( R.id.toolbarChat );
        toolbar.setNavigationIcon( R.drawable.ic_arrow_back_black_24dp );
        toolbar.setNavigationOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        } );

        db = FirebaseDatabase.getInstance();
        reference = db.getReference("chat").child( nomorUser );

        btnSend.setBackgroundResource( R.drawable.ic_send_grey_24dp );
        btnSend.setEnabled( false );

        scrollView.post( new Runnable() {
            @Override
            public void run() {
                scrollView.fullScroll( ScrollView.FOCUS_DOWN );
            }
        } );

        editText.addTextChangedListener( new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() == 0){
                    btnSend.setBackgroundResource( R.drawable.ic_send_grey_24dp );
                    btnSend.setEnabled( false );
                }else{
                    btnSend.setBackgroundResource( R.drawable.ic_send_success_24dp );
                    btnSend.setEnabled( true );
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        } );

        btnSend.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!isNetworkAvailable()){
                    Toast toasty = Toasty.normal( ChatActivity.this, "Connection is unstable", getResources().getDrawable( R.drawable.nointernet ) );
                    toasty.setGravity( Gravity.TOP|Gravity.CENTER_HORIZONTAL,0,0 );
                    toasty.show();
                }else{
                    ChatModel chatModel = new ChatModel( nomorUser,editText.getText().toString(),getWaktu() );
                    reference.child( String.valueOf( System.currentTimeMillis() ) )
                            .setValue( chatModel );
                    editText.setText( "" );
                    scrollView.post( new Runnable() {
                        @Override
                        public void run() {
                            scrollView.fullScroll( ScrollView.FOCUS_DOWN );
                        }
                    } );
                }
            }
        } );

        loadRecyclerViewChat();

        swipeRefreshLayout.setColorScheme(R.color.steel_blue,R.color.orange,R.color.colorAccent);
        swipeRefreshLayout.setOnRefreshListener( new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(!isNetworkAvailable()){
                    swipeRefreshLayout.setRefreshing( true );
                }else{
                    loadRecyclerViewChat();
                    swipeRefreshLayout.setRefreshing( false );
                }
            }
        } );

    }

    private void loadRecyclerViewChat() {
        options = new FirebaseRecyclerOptions.Builder<ChatModel>()
                    .setQuery(reference,ChatModel.class).build();

        adapter = new FirebaseRecyclerAdapter<ChatModel, ChatViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull final ChatViewHolder holder, final int position, @NonNull ChatModel model) {
                //left side
                holder.username.setText( model.getUsername() );
                holder.message.setText( model.getMessage() );

                //right side
                holder.usernameRight.setText( model.getUsername() );
                holder.messageRight.setText( model.getMessage() );

                if(holder.username.getText().toString().equals( "Admin" )){
                    //set invisible chat kanan
                    holder.username.setVisibility( View.GONE );
                    holder.cardChatRight.setVisibility( View.GONE );
                    holder.usernameRight.setVisibility( View.GONE );
                    holder.messageRight.setVisibility( View.GONE );
                }else{
                    //set invisible chat kiri
                    holder.usernameRight.setVisibility( View.GONE );
                    holder.cardChat.setVisibility( View.GONE );
                    holder.username.setVisibility( View.GONE );
                    holder.message.setVisibility( View.GONE );
                }

                holder.cardChatRight.setLongClickable( true );
                holder.cardChatRight.setOnLongClickListener( new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        final AlertDialog.Builder alertDialog = new AlertDialog.Builder( ChatActivity.this );
                        LayoutInflater inflater = getLayoutInflater();
                        View viewku = inflater.inflate( R.layout.unsend_message,null );
                        alertDialog.setView( viewku );

                        TextView unsend,copy;
                        unsend = viewku.findViewById( R.id.unsendMessage );
                        copy = viewku.findViewById( R.id.copyText );

                        final AlertDialog show = alertDialog.show();

                        unsend.setOnClickListener( new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                reference.child( adapter.getRef( position ).getKey() ).removeValue();
                                show.dismiss();
                            }
                        } );

                        copy.setOnClickListener( new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                ClipboardManager clipboardManager = (ClipboardManager)getSystemService( CLIPBOARD_SERVICE );
                                ClipData clipData = ClipData.newPlainText( "label",holder.message.getText().toString() );
                                clipboardManager.setPrimaryClip( clipData );
                                show.dismiss();
                            }
                        } );


                        return true;
                    }
                } );

                holder.setItemClickListener( new ItemClickListener() {
                    @Override
                    public void onClick(View view, final int position, boolean isLongClick) {

                    }
                } );
            }

            @NonNull
            @Override
            public ChatViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                View view = LayoutInflater.from( viewGroup.getContext() ).inflate( R.layout.layout_chat, viewGroup, false );
                return new ChatViewHolder( view );
            }
        };

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager( this );
        RecyclerView.LayoutManager rvLayoutManager = linearLayoutManager;
        recyclerView.setLayoutManager( rvLayoutManager );
        recyclerView.setNestedScrollingEnabled( false );
        adapter.startListening();
        recyclerView.setAdapter( adapter );
    }

    private String getWaktu(){
        DateFormat dateFormat = new SimpleDateFormat( "HH:mm" );
        Date date = new Date();
        return dateFormat.format( date );
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private void setNotifIntenentOff()
    {
        if(!isNetworkAvailable()){
            Toast toasty = Toasty.normal( ChatActivity.this, "Connection is unstable", getResources().getDrawable( R.drawable.nointernet ) );
            toasty.setGravity( Gravity.TOP|Gravity.CENTER_HORIZONTAL,0,0 );
            toasty.show();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(adapter != null){
            adapter.startListening();
        }

        setNotifIntenentOff();

    }

    @Override
    protected void onResume() {
        super.onResume();
        if(adapter != null){
            adapter.startListening();
        }

        setNotifIntenentOff();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (adapter != null){
            adapter.stopListening();
        }
    }
}
