package com.example.tokoemak.tokoemak.model;

public class BeritaModel {
    private String judul,link,gambar;

    public BeritaModel(String judul, String link, String gambar) {
        this.judul = judul;
        this.link = link;
        this.gambar = gambar;
    }

    public BeritaModel() {
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }
}
