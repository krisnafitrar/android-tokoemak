package com.example.tokoemak.tokoemak.adapter;

import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.example.tokoemak.tokoemak.R;
import com.example.tokoemak.tokoemak.activities.FavoriteActivities;
import com.example.tokoemak.tokoemak.activities.ProdukDetail;
import com.example.tokoemak.tokoemak.app.AppController;
import com.example.tokoemak.tokoemak.model.ProdukModel;
import com.example.tokoemak.tokoemak.util.Server;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import io.supercharge.shimmerlayout.ShimmerLayout;

public class FavoriteAdapter extends RecyclerView.Adapter<FavoriteAdapter.ViewProcessHolder> {
    private FavoriteActivities mContext;
    private ArrayList<ProdukModel> mList;
    private com.example.tokoemak.tokoemak.commons.SharedPreferences sharedPreferences;
    private HashMap<String,String> user = new HashMap<String, String>();
    private String idUser;
    private Locale localeID = new Locale("in", "ID");
    private NumberFormat formatHarga = NumberFormat.getCurrencyInstance( localeID );

    public FavoriteAdapter(FavoriteActivities mContext, ArrayList<ProdukModel> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @NonNull
    @Override
    public FavoriteAdapter.ViewProcessHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from( mContext ).inflate( R.layout.layout_produk_fav,viewGroup,false );
        ViewProcessHolder viewProcessHolder = new ViewProcessHolder( view );
        return viewProcessHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull FavoriteAdapter.ViewProcessHolder viewProcessHolder, int i) {
        final ProdukModel produkModel = mList.get( i );
        LinearLayout linearLayout = viewProcessHolder.linearLayout;
        ImageView btnLove,gambar;
        TextView hargaProduk,nama,jenis,hargadiskon,diskonLabel;
        Button btnLihat;
        ShimmerLayout shimmerLayout;


        sharedPreferences = new com.example.tokoemak.tokoemak.commons.SharedPreferences( mContext );
        user = sharedPreferences.getUserDetails();
        idUser = user.get( sharedPreferences.KEY_IDUSER );

        btnLove = viewProcessHolder.btnLove;
        gambar = viewProcessHolder.gambar;
        hargaProduk = viewProcessHolder.harga;
        nama = viewProcessHolder.nama;
        btnLihat = viewProcessHolder.btnLihat;
        hargadiskon = viewProcessHolder.hargadiskon;
        shimmerLayout = viewProcessHolder.shimmerLayout;
        diskonLabel = viewProcessHolder.diskon;

        Glide.with( mContext )
                .load(produkModel.getGambar())
                .into( gambar );
        if(Integer.parseInt( produkModel.getDiskon() ) > 1){
            shimmerLayout.setVisibility( View.VISIBLE );
            shimmerLayout.startShimmerAnimation();
            diskonLabel.setText( produkModel.getDiskon() + "% Off" );
            hargaProduk.setText( formatHarga.format(Integer.parseInt( produkModel.getHarga() ) -
                    ((Integer.parseInt( produkModel.getDiskon() ) * Integer.parseInt( produkModel.getHarga() ))
                            / 100 ) ));
            hargadiskon.setVisibility( View.VISIBLE );
            hargadiskon.setText( formatHarga.format(Integer.parseInt( produkModel.getHarga() ) ) );
            hargadiskon.setPaintFlags(hargadiskon.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }else {
            shimmerLayout.setVisibility( View.GONE );
            hargaProduk.setText( formatHarga.format(Integer.parseInt( produkModel.getHarga() ) ) );
            hargadiskon.setVisibility( View.GONE );
        }

        nama.setText( produkModel.getNama() );

        hargaProduk.setTypeface( Typeface.createFromAsset( mContext.getAssets(),"fonts/Poppins-Medium.ttf" ) );

        btnLove.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteFav(idUser,produkModel.getKode_barang());
                mContext.initFav();
                if(mList.size() < 1){
                    mContext.inflateCart.setVisibility( View.GONE );
                }
            }
        } );

        btnLihat.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               gotoDetail(produkModel);
            }
        } );

        linearLayout.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoDetail( produkModel );
            }
        } );


    }

    private void gotoDetail(ProdukModel produkModel) {
        Intent intent = new Intent( mContext,ProdukDetail.class );
        intent.putExtra( "gambar",produkModel.getGambar() );
        intent.putExtra( "nama",produkModel.getNama() );
        intent.putExtra( "jenis", produkModel.getJenis() );
        intent.putExtra( "harga", produkModel.getHarga() );
        intent.putExtra( "diskon", produkModel.getDiskon() );
        intent.putExtra( "stok",produkModel.getStok() );
        intent.putExtra( "deskripsi", produkModel.getDeskripsi() );
        intent.putExtra( "kode_barang",produkModel.getKode_barang() );
        intent.putExtra( "latitude",mContext.mLat );
        intent.putExtra( "longitude",mContext.mLong );
        mContext.startActivity( intent );
    }


    private void deleteFav(final String idUser, final String kode_barang) {

        StringRequest stringRequest = new StringRequest( Request.Method.POST, Server.REST_API_PRODUK_FAV, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    JSONObject jsonObject = new JSONObject( response );
                    boolean success = jsonObject.getBoolean( "status" );

                    if(success){
                        System.out.println("BERHASIL DELETE FAVORIT");
                    }else{
                        System.out.println("GAGAL DELETE FAVORIT");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        } ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id", idUser);
                params.put( "idbarang",kode_barang);
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue( stringRequest,"json_obj_req" );
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewProcessHolder extends RecyclerView.ViewHolder {
        LinearLayout linearLayout;
        ImageView btnLove,gambar;
        TextView harga,nama,diskon,hargadiskon;
        Button btnLihat;
        ShimmerLayout shimmerLayout;

        public ViewProcessHolder(@NonNull View itemView) {
            super( itemView );
            linearLayout = itemView.findViewById( R.id.linearProdukFav );
            btnLove = itemView.findViewById( R.id.btnLoveProduk );
            gambar = itemView.findViewById( R.id.gambarFavProduk );
            harga = itemView.findViewById( R.id.hargaFavProduk );
            nama = itemView.findViewById( R.id.namaFavProduk );
            btnLihat = itemView.findViewById( R.id.btnLihatFav );
            shimmerLayout = itemView.findViewById( R.id.shimmerBadgesDiskonFav );
            diskon = itemView.findViewById( R.id.diskonLabelFav );
            hargadiskon = itemView.findViewById( R.id.hargaDiskonFav );
        }
    }
}
