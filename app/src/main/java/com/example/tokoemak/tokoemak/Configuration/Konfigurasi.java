package com.example.tokoemak.tokoemak.Configuration;

public class Konfigurasi {

    //mendefinisikan url dengan tipe data string
    public static final String URL_ADD="http://192.168.43.228/Android/user/tambahUser.php";
    public static final String URL_GET_ALL = "http://192.168.43.228/Android/user/bacaAllUser.php";
    public static final String URL_GET_USER = "http://192.168.43.228/Android/user/bacaUser.php?phone=";
    public static final String URL_UPDATE_USER = "http://192.168.43.228/Android/user/updateUser.php";
    public static final String URL_DELETE_USER = "http://192.168.43.228/Android/user/deleteUser.php?id=";

    //key untuk hashmap
    public static final String KEY_USER_ID = "id";
    public static final String KEY_USER_NAMA = "nama";
    public static final String KEY_USER_PHONE = "phone"; //desg itu variabel untuk posisi
    public static final String KEY_USER_EMAIL = "email"; //salary itu variabel untuk gajih

    //Json tags
    public static final String TAG_JSON_ARRAY="result";
    public static final String TAG_ID = "id";
    public static final String TAG_NAMA = "nama";
    public static final String TAG_PHONE = "phone";
    public static final String TAG_EMAIL = "email";

    public static final String USER_ID = "user_id";
}
