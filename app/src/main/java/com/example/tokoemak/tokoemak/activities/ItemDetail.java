package com.example.tokoemak.tokoemak.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.example.tokoemak.tokoemak.R;
import com.example.tokoemak.tokoemak.commons.SharedPreferences;
import com.example.tokoemak.tokoemak.databases.Database;
import com.example.tokoemak.tokoemak.model.BarangModel;
import com.example.tokoemak.tokoemak.model.OrderModel;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import es.dmoral.toasty.Toasty;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ItemDetail extends AppCompatActivity {
    private TextView namaItem, hargaItem,informasiBarang,stokBarang,barangTerjual;
    private ImageView gambarItem,closeBottomSheet;
    private ElegantNumberButton numberButton;
    private String itemId = "";
    private FirebaseDatabase database;
    private DatabaseReference reference;
    private CollapsingToolbarLayout collapsingToolbarLayout;
    private android.support.v7.widget.Toolbar toolbar;
    private BarangModel currentBarang;
    private Geocoder geocoder;
    private List<Address> addressList;
    private double latitude,longitude;
    private String kotaUser;
    private CoordinatorLayout layout;
    private View bottomSheet;
    private BottomSheetBehavior bottomSheetBehavior;
    private Button btnOke, btnTutup;
    LinearLayout btnPesanSekarang;
    List<OrderModel> mList = new ArrayList<>(  );
    String jumlah;
    SharedPreferences sharedPreferences;
    Typeface styleFonts;
    String namaProduk,hargaProduk,gambarProduk,stokProduk,terjualProduk,namaProduk2,kode_barang;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext( CalligraphyContextWrapper.wrap( newBase ) );
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        CalligraphyConfig.initDefault( new CalligraphyConfig.Builder().setDefaultFontPath( "fonts/Muli.ttf" )
                .setFontAttrId( R.attr.fontPath )
                .build());
        setContentView( R.layout.activity_item_detail );

        //init Typeface
        styleFonts = Typeface.createFromAsset( getAssets(), "fonts/MuliBold.ttf" );

        //init layout
        layout = findViewById( R.id.layoutCoordinator );

        //init sharedpreferences
        sharedPreferences = new SharedPreferences( this );

        //init
        geocoder = new Geocoder( this, Locale.getDefault() );

        //init toolbar
        toolbar = findViewById(R.id.toolbarDetail);
        toolbar.inflateMenu( R.menu.menu_toolbar );
        toolbar.setNavigationIcon( R.drawable.ic_keyboard_arrow_left_black_24dp);
        toolbar.setNavigationOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        } );


        //init bottomsheet
        closeBottomSheet = findViewById( R.id.closeBottomSheet );
        btnOke = findViewById( R.id.btnOkeDeh );
        btnTutup = findViewById( R.id.btnTutupSheet );
        bottomSheet = findViewById( R.id.bottomSheet );
        bottomSheetBehavior = BottomSheetBehavior.from( bottomSheet );
        bottomSheetBehavior.setHideable( true );
        btnPesanSekarang = findViewById( R.id.btnPesanSekarang );

        //Firebase Database
        database = FirebaseDatabase.getInstance();
        reference = database.getReference("item");

        //init View
        namaItem = findViewById( R.id.namaBarangDetail );
        informasiBarang = findViewById( R.id.informasiBarang );
        hargaItem = findViewById( R.id.hargaBarangDetail );
        stokBarang = findViewById( R.id.stokBarang );
        barangTerjual = findViewById( R.id.terjual );
        numberButton = findViewById( R.id.number_button );
        collapsingToolbarLayout = findViewById( R.id.collapsingDetail );
        gambarItem = findViewById( R.id.img_itemBackDetail );

        //init custom fonts
        namaItem.setTypeface( styleFonts );
        informasiBarang.setTypeface( styleFonts );

        //init close bottomsheet
        closeBottomSheet.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetBehavior.setState( BottomSheetBehavior.STATE_HIDDEN );
            }
        } );

        btnOke.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetBehavior.setState( BottomSheetBehavior.STATE_HIDDEN );
            }
        } );

        btnTutup.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetBehavior.setState( BottomSheetBehavior.STATE_HIDDEN );
            }
        } );


        if(getIntent() != null){
            itemId = getIntent().getStringExtra( "itemId" );
            latitude = getIntent().getDoubleExtra( "latitude",0 );
            longitude = getIntent().getDoubleExtra( "longitude", 0 );
        }

        //get City of User
        try {
            addressList = geocoder.getFromLocation( latitude,longitude, 1 );
            kotaUser = addressList.get( 0 ).getLocality();
            System.out.println("KOTA USER = " + kotaUser);
        } catch (IOException e) {
            e.printStackTrace();
        }


        getDetailItem();


        btnPesanSekarang.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(kotaUser.equals( "Kecamatan Bojonegoro" )){
                    setCart();
                }else{
                    bottomSheetBehavior.setState( BottomSheetBehavior.STATE_EXPANDED );
                }
            }
        } );

    }

    private void setCart() {
        mList = new Database( getBaseContext())
                .getCarts();
        System.out.println("SIZE = " + mList.size());

        if(mList.size() == 0){
            new Database( getBaseContext() )
                    .addToCart( new OrderModel(
                            kode_barang,
                            namaProduk2,
                            numberButton.getNumber(),
                            hargaProduk,
                            "0",
                            gambarProduk
                    ) );
            sharedPreferences.setView( "yes" );
            Toast toasty = Toasty.normal( ItemDetail.this, "Menambahkan ke keranjang", getResources().getDrawable( R.drawable.ic_shopping_cart_black_24dp ) );
            toasty.setGravity( Gravity.TOP|Gravity.CENTER_HORIZONTAL,0,0 );
            toasty.show();
        }else{
            boolean update = false;
            int position = 0;
            for(int i = 0; i < mList.size(); i++){
                System.out.println("MASUK LOOPING");
                if(mList.get( i ).getNamaProduk().equals( namaProduk2 )){
                    update = true;
                    System.out.println("BOOLEAN : " + update);
                    jumlah = mList.get( i ).getJumlah();
                    namaProduk = mList.get( i ).getNamaProduk();
                    new Database( getBaseContext() )
                            .updateCart( Integer.parseInt( numberButton.getNumber() )+ Integer.parseInt( jumlah ), namaProduk);
                    Toast toasty = Toasty.normal( ItemDetail.this, "Menambahkan ke keranjang", getResources().getDrawable( R.drawable.ic_shopping_cart_black_24dp ) );
                    toasty.setGravity( Gravity.TOP|Gravity.CENTER_HORIZONTAL,0,0 );
                    toasty.show();
                    break;
                }
            }

            if(update == false){
                new Database( getBaseContext() )
                        .addToCart( new OrderModel(
                                kode_barang,
                                namaProduk2,
                                numberButton.getNumber(),
                                hargaProduk,
                                "0",
                                gambarProduk
                        ) );
                sharedPreferences.setView( "yes" );
                Toast toasty = Toasty.normal( ItemDetail.this, "Menambahkan ke keranjang", getResources().getDrawable( R.drawable.ic_shopping_cart_black_24dp ) );
                toasty.setGravity( Gravity.TOP|Gravity.CENTER_HORIZONTAL,0,0 );
                toasty.show();
            }

        }
    }

    private void getDetailItem() {
            if(getIntent() != null){
                namaProduk2 = getIntent().getStringExtra( "nama" );
                gambarProduk = getIntent().getStringExtra( "gambar" );
                hargaProduk = getIntent().getStringExtra( "harga" );
                stokProduk = getIntent().getStringExtra( "stok" );
                terjualProduk = getIntent().getStringExtra( "terjual" );
                kode_barang = getIntent().getStringExtra( "kode_barang" );

            Glide.with(ItemDetail.this)
                    .load( gambarProduk )
                    .into( gambarItem );

            collapsingToolbarLayout.setTitle( namaProduk2 );
            namaItem.setText( namaProduk );
            hargaItem.setText( "Rp." + hargaProduk );
            stokBarang.setText( stokProduk );
            barangTerjual.setText( terjualProduk );

            }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult( requestCode, resultCode, data );

    }
}
