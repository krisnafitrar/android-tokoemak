package com.example.tokoemak.tokoemak.model;

public class DiskonModel {
    String jumlah,limit;
    UserDiskonModel userDiskonModel;

    public DiskonModel() {
    }

    public DiskonModel(String jumlah, String limit, UserDiskonModel userDiskonModel) {
        this.jumlah = jumlah;
        this.limit = limit;
        this.userDiskonModel = userDiskonModel;
    }

    public UserDiskonModel getUserDiskonModel() {
        return userDiskonModel;
    }

    public void setUserDiskonModel(UserDiskonModel userDiskonModel) {
        this.userDiskonModel = userDiskonModel;
    }

    public String getJumlah() {
        return jumlah;
    }

    public void setJumlah(String jumlah) {
        this.jumlah = jumlah;
    }

    public String getLimit() {
        return limit;
    }

    public void setLimit(String limit) {
        this.limit = limit;
    }
}
