package com.example.tokoemak.tokoemak.ViewHolder;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.tokoemak.tokoemak.Interface.ItemClickListener;
import com.example.tokoemak.tokoemak.R;

public class KategoriViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    public CardView cardView;
    public ImageView gambar;
    public TextView namaKategori;

    private ItemClickListener itemClickListener;

    public KategoriViewHolder(@NonNull View itemView) {
        super( itemView );
        cardView = itemView.findViewById( R.id.cardKategori );
        gambar = itemView.findViewById( R.id.gambarKategori );
        namaKategori = itemView.findViewById( R.id.text_kategori );

        itemView.setOnClickListener( this );

    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public void onClick(View view) {
        itemClickListener.onClick( view,getAdapterPosition(), false  );
    }


}
