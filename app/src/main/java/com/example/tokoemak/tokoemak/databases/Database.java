package com.example.tokoemak.tokoemak.databases;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.support.v7.widget.RecyclerView;

import com.example.tokoemak.tokoemak.model.LokasiModel;
import com.example.tokoemak.tokoemak.model.OrderModel;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;
import java.util.List;

public class Database extends SQLiteAssetHelper {
    private static final String DB_NAME= "mydb.db";
    private static final int DB_VER=1;

    public Database(Context context) {
        super(context, DB_NAME, null, DB_VER);
//        context.openOrCreateDatabase( DB_NAME, context.MODE_PRIVATE, null );
//        setForcedUpgrade(1);
    }

    public List<OrderModel> getCarts()
    {
      SQLiteDatabase db = getReadableDatabase();
      SQLiteQueryBuilder qb = new SQLiteQueryBuilder();

      String[] sqlSelect = {"NamaProduk", "ProdukId","Jumlah", "Harga","Diskon","Gambar" };
      String table = "OrderDetail";

      qb.setTables( table );
      Cursor c = qb.query( db,sqlSelect , null, null, null, null,null   );

      final List<OrderModel> result = new ArrayList<>(  );
      if(c.moveToFirst()){
          do {
              result.add( new OrderModel( c.getString(c.getColumnIndex(  "ProdukId" ) ),
                      c.getString(c.getColumnIndex(  "NamaProduk" ) ) ,
                      c.getString(c.getColumnIndex(  "Jumlah" ) ) ,
                      c.getString(c.getColumnIndex(  "Harga" ) ),
                      c.getString(c.getColumnIndex(  "Diskon" ) ),
                      c.getString(c.getColumnIndex(  "Gambar" ) )
                ));
            }while (c.moveToNext());
      }
      return result;
    };

    public List<LokasiModel> getLocation()
    {
        SQLiteDatabase db = getReadableDatabase();
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();

        String[] sqlSelect = {"IdUser", "Latitude","Longitude","IsUsed"};
        String table = "LocationOfUser";

        qb.setTables( table );
        Cursor c = qb.query( db,sqlSelect , null, null, null, null,null   );
        final List<LokasiModel> result = new ArrayList<>();
        if(c.moveToFirst()){
            do{
                result.add( new LokasiModel( c.getString( c.getColumnIndex( "IdUser" ) ) ,
                        c.getString( c.getColumnIndex( "Latitude" ) ),
                        c.getString( c.getColumnIndex( "Longitude" ) ),
                        c.getInt( c.getColumnIndex( "IsUsed" ) )
                ) );
            }while (c.moveToNext());
        }
        return result;
    }

    public void addToCart(OrderModel orderModel)
    {
        SQLiteDatabase db = getWritableDatabase();
        String query = String.format( "INSERT INTO OrderDetail(ProdukId,NamaProduk,Jumlah,Harga,Diskon,Gambar) VALUES('%s','%s','%s','%s','%s','%s' );",
          orderModel.getProdukId()
        , orderModel.getNamaProduk()
        , orderModel.getJumlah()
        , orderModel.getHarga()
        , orderModel.getDiskon()
        , orderModel.getGambar()
        );

        db.execSQL( query );
    };

    public void addLocation(LokasiModel lokasiModel)
    {
        SQLiteDatabase db = getWritableDatabase();
        String query = String.format( "INSERT INTO LocationOfUser(IdUser,Latitude,Longitude,IsUsed) VALUES('%s','%s','%s',%s);",
                lokasiModel.getIduser()
                , lokasiModel.getLatitude()
                , lokasiModel.getLongitude()
                , lokasiModel.getIsUsed()
        );

        db.execSQL( query );
    }

    public void updateCart(int jumlah,String namaProduk){
        SQLiteDatabase db = getWritableDatabase();
        String query = "UPDATE OrderDetail SET Jumlah ='" + jumlah + "'" + " WHERE NamaProduk ='" + namaProduk  + "'";

        db.execSQL( query );

    }


    public void updateLatLong(String iduser,String lat,String longi)
    {
        SQLiteDatabase db = getWritableDatabase();
        String query = "UPDATE LocationOfUser SET Latitude ='" + lat + "'" + ", Longitude='" + longi + "'" + " WHERE IdUser='" + iduser + "'";

        db.execSQL( query );
    }

    public void setIsUsed(String idUser, int isUsed)
    {
        SQLiteDatabase db = getWritableDatabase();
        String query = "UPDATE LocationOfUser SET IsUsed=" + isUsed  + " WHERE IdUser='" + idUser + "'";

        db.execSQL( query );
    }

    public void cleanLocation()
    {
        SQLiteDatabase db = getReadableDatabase();
        String query = String.format("DELETE FROM LocationOfUser");

        db.execSQL( query );
    }

    public void cleanCart()
    {
        SQLiteDatabase db = getReadableDatabase();
        String query = String.format("DELETE FROM OrderDetail");

        db.execSQL( query );
    };

    public void deleteItem(String nama){
        SQLiteDatabase db = getWritableDatabase();
        String query = "DELETE FROM OrderDetail WHERE NamaProduk='" + nama + "'";

        db.execSQL( query );
    }
}
