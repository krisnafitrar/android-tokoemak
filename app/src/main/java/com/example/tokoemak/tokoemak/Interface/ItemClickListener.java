package com.example.tokoemak.tokoemak.Interface;

import android.view.View;

import com.example.tokoemak.tokoemak.ViewHolder.ChatViewHolder;

public interface ItemClickListener {
    void onClick(View view, int position, boolean isLongClick);
}
