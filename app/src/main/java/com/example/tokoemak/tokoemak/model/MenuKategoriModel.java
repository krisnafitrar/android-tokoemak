package com.example.tokoemak.tokoemak.model;

public class MenuKategoriModel {
    String kode;
    String nama;
    String gambar;

    public MenuKategoriModel() {
    }

    public MenuKategoriModel(String kode, String nama, String gambar) {
        this.kode = kode;
        this.nama = nama;
        this.gambar = gambar;
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }
}
