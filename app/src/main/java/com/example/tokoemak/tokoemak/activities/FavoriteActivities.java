package com.example.tokoemak.tokoemak.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.tokoemak.tokoemak.R;
import com.example.tokoemak.tokoemak.adapter.FavoriteAdapter;
import com.example.tokoemak.tokoemak.app.AppController;
import com.example.tokoemak.tokoemak.commons.SharedPreferences;
import com.example.tokoemak.tokoemak.databases.Database;
import com.example.tokoemak.tokoemak.model.OrderModel;
import com.example.tokoemak.tokoemak.model.ProdukModel;
import com.example.tokoemak.tokoemak.util.Server;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class FavoriteActivities extends AppCompatActivity {
    private SharedPreferences sharedPreferences;
    private HashMap<String,String> user = new HashMap<String, String>();
    private String idUser;
    private ArrayList<ProdukModel> modelList;
    public FavoriteAdapter mAdapter;
    private RecyclerView rv_produk_fav;
    private ProgressBar progressBar;
    private LinearLayout linearNullFav;
    private ImageView btnBack;
    public double mLat, mLong;
    private TextView qtyOnCart,slashCart,priceOnCart;
    public RelativeLayout inflateCart;
    private List<OrderModel> cartList = new ArrayList<>();
    private List<OrderModel> totalPrice = new ArrayList<>();
    private Locale localeID = new Locale("in", "ID");
    private NumberFormat formatHarga = NumberFormat.getCurrencyInstance( localeID );

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext( CalligraphyContextWrapper.wrap( newBase ) );
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        CalligraphyConfig.initDefault( new CalligraphyConfig.Builder().setDefaultFontPath( "fonts/Poppins-Regular.ttf" )
                .setFontAttrId( com.example.tokoemak.tokoemak.R.attr.fontPath )
                .build());
        setContentView( com.example.tokoemak.tokoemak.R.layout.added_favorite );

        getDataFromIntent();
        sharedPreferences = new SharedPreferences( this );
        user = sharedPreferences.getUserDetails();
        idUser = user.get( sharedPreferences.KEY_IDUSER );

        btnBack = findViewById( com.example.tokoemak.tokoemak.R.id.backArrowFav );
        progressBar = findViewById( com.example.tokoemak.tokoemak.R.id.progressBarFav );
        TextView titleToolbar = findViewById( com.example.tokoemak.tokoemak.R.id.textAddedToolbarFavorit );
        linearNullFav = findViewById( com.example.tokoemak.tokoemak.R.id.linearNullFav );
        TextView titleFav = findViewById( com.example.tokoemak.tokoemak.R.id.textFav );
        titleFav.setTypeface( Typeface.createFromAsset( getAssets(),"fonts/Poppins-Medium.ttf" ) );
        titleToolbar.setTypeface( Typeface.createFromAsset( getAssets(),"fonts/Poppins-Medium.ttf" ) );
        rv_produk_fav = findViewById( com.example.tokoemak.tokoemak.R.id.rv_produk_fav );

        btnBack.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        } );

        initInflaterViews();

//        initFav();
        setTypeface();

        inflateCart.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( FavoriteActivities.this,CartActivity.class );
                intent.putExtra( "lat", mLat );
                intent.putExtra( "long", mLong );
                startActivity( intent );
            }
        } );


    }


    public void setValueCart()
    {
        qtyOnCart.setText( checkCart() + " item" );
        priceOnCart.setText( formatHarga.format(getTotalPrice()) + " (est)" );
    }


    public int checkCart()
    {
        int x = 0;

        cartList = new Database( FavoriteActivities.this ).getCarts();

        for (OrderModel orderModel:cartList)
            x += Integer.parseInt( orderModel.getJumlah() );

        return x;

    }


    public int getTotalPrice()
    {
        int x = 0;

        totalPrice = new Database( FavoriteActivities.this ).getCarts();

        for (OrderModel model:totalPrice)
            x += (Integer.parseInt( model.getHarga() ) * Integer.parseInt( model.getJumlah() ));

        return x;
    }


    private void initInflaterViews() {
        inflateCart = findViewById( R.id.inflateCartFav );
        qtyOnCart = findViewById( R.id.qtyOnCartFav );
        slashCart = findViewById( R.id.slashCartFav );
        priceOnCart = findViewById( R.id.priceOnCartFav );
    }


    private void getDataFromIntent() {
        if(getIntent() != null){
            mLat = getIntent().getDoubleExtra( "lat",0 );
            mLong = getIntent().getDoubleExtra( "long",0 );
        }
    }


    public void initFav() {
        modelList = new ArrayList<>();
        getProduk(idUser);
        rv_produk_fav.setLayoutManager( new GridLayoutManager( this,2 ) );
        rv_produk_fav.setHasFixedSize( true );
        mAdapter = new FavoriteAdapter( FavoriteActivities.this,modelList );
        rv_produk_fav.setAdapter( mAdapter );
        rv_produk_fav.setNestedScrollingEnabled( false );
    }


    public void setTypeface()
    {
        Typeface bFont = Typeface.createFromAsset( getAssets(), "fonts/Poppins-Bold.ttf" );
        qtyOnCart.setTypeface( bFont );
        slashCart.setTypeface( bFont );
        priceOnCart.setTypeface( bFont );
    }

    private void getProduk(final String idUser) {
        String urlWithParams = Server.REST_API_PRODUK_FAV + "?id=" + idUser + "&act=get";
        linearNullFav.setVisibility( View.VISIBLE );
        inflateCart.setVisibility( View.GONE );
        StringRequest stringRequest = new StringRequest( Request.Method.GET, urlWithParams, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    JSONArray jsonArray = new JSONArray( response );
                    for(int i = 0; i < jsonArray.length(); i++){
                        try{
                            JSONObject data = jsonArray.getJSONObject( i );
                            ProdukModel model = new ProdukModel();
                            model.setKode_barang( data.getString( "idbarang" ) );
                            model.setNama( data.getString( "namabarang" ) );
                            model.setJenis( data.getString("namajenis"));
                            model.setHarga( data.getString( "harga_jual" ) );
                            model.setStok( data.getString( "stok" ) );
                            model.setGambar(data.getString("gambar"));
                            model.setDeskripsi( data.getString( "deskripsi" ) );
                            model.setDiskon( data.getString( "diskon" ) );
                            modelList.add(model);
                            linearNullFav.setVisibility( View.GONE );
                            progressBar.setVisibility( View.GONE );
                            if(checkCart() > 0){
                                inflateCart.setVisibility( View.VISIBLE );
                                setValueCart();
                            }
                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                    mAdapter.notifyDataSetChanged();
                }catch (JSONException exception){
                    exception.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility( View.GONE );
            }
        } );

        AppController.getInstance().addToRequestQueue(stringRequest,"json_obj_req");
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initFav();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }



}
