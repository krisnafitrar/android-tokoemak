package com.example.tokoemak.tokoemak.ViewHolder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.tokoemak.tokoemak.Interface.ItemClickListener;
import com.example.tokoemak.tokoemak.R;
import com.varunest.sparkbutton.SparkButton;

public class PenawaranViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private ItemClickListener itemClickListener;
    public ImageView gambarPenawaran;
    public SparkButton likePenawaran;
    public TextView judulPenawaran,descPenawaran, orderNowPenawaran;

    public PenawaranViewHolder(@NonNull View itemView) {
        super( itemView );
        gambarPenawaran = itemView.findViewById( R.id.imgPenawaran );
        judulPenawaran = itemView.findViewById( R.id.namaPenawaran );
        descPenawaran = itemView.findViewById( R.id.desPenawaran );
        orderNowPenawaran = itemView.findViewById( R.id.orderNowPenawaran );
        likePenawaran = itemView.findViewById( R.id.likePenawaran );

        itemView.setOnClickListener( this );
    }


    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public void onClick(View v) {
        itemClickListener.onClick( v , getAdapterPosition(), false );
    }
}
