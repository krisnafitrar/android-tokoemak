package com.example.tokoemak.tokoemak.ViewHolder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.tokoemak.tokoemak.Interface.ItemClickListener;
import com.example.tokoemak.tokoemak.R;
import com.shuhart.stepview.StepView;

public class ItemDetailViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    public TextView namaItem,hargaItem,jumlahItem;
    private ItemClickListener itemClickListener;

    public ItemDetailViewHolder(@NonNull View itemView) {
        super( itemView );
        namaItem = itemView.findViewById( R.id.namaCartList );
        hargaItem = itemView.findViewById( R.id.hargaCartList );
        jumlahItem = itemView.findViewById( R.id.jumlahBarangDetailOrder );
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public void onClick(View view) {
        itemClickListener.onClick( view,getAdapterPosition(), false  );
    }
}
