package com.example.tokoemak.tokoemak.activities;

import android.arch.core.executor.TaskExecutor;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tokoemak.tokoemak.R;
import com.example.tokoemak.tokoemak.commons.SharedPreferences;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class VerifikasiActivity extends AppCompatActivity  {
    private Button btnVerify;
    private TextView title,timerAndSend,textDesc;
    private String phoneNumber,iduser,nama,email;
    private int isFinished;
    private RelativeLayout relativeLayout;
    private String verificationId;
    private FirebaseAuth mAuth;
    private ProgressBar progressBar;
    private MaterialEditText materialEditText;
    private ImageView btnBack,btnHelp;
    private SharedPreferences sharedPreferences;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext( CalligraphyContextWrapper.wrap( newBase ) );
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        CalligraphyConfig.initDefault( new CalligraphyConfig.Builder().setDefaultFontPath( "fonts/Poppins-Regular.ttf" )
                .setFontAttrId( R.attr.fontPath )
                .build());
        setContentView( R.layout.activity_verifikasi );
        btnVerify = findViewById( R.id.btn_verify );
        title = findViewById( R.id.title_verif );
        mAuth = FirebaseAuth.getInstance();
        progressBar = findViewById( R.id.progressBarVerif );
        materialEditText = findViewById( R.id.textBoxPhoneVerif );
        relativeLayout = findViewById( R.id.root_verify );
        btnBack = findViewById( R.id.btnBackVerif );
        btnHelp = findViewById( R.id.btnHelpVerif );
        timerAndSend = findViewById( R.id.timerSend );
        textDesc = findViewById( R.id.txtDesVerif );
        sharedPreferences = new SharedPreferences( this );

        timerAndSend.setEnabled( false );

        title.setTypeface( Typeface.createFromAsset( getAssets(),"fonts/Poppins-Medium.ttf" ) );
        btnVerify.setTypeface( Typeface.createFromAsset( getAssets(),"fonts/Poppins-Medium.ttf" ) );

        btnVerify.setEnabled( false );
        btnVerify.setBackgroundResource( R.color.light_gray );

        btnBack.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        } );

        btnHelp.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText( VerifikasiActivity.this, "Masukkan kode yang kami kirim lewat SMS", Toast.LENGTH_SHORT ).show();
            }
        } );


        materialEditText.addTextChangedListener( new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() > 3){
                    btnVerify.setEnabled( true );
                    btnVerify.setBackgroundResource( R.color.colorAccent );
                }else{
                    btnVerify.setEnabled( false );
                    btnVerify.setBackgroundResource( R.color.light_gray );
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        } );

        if(getIntent() != null){
            phoneNumber = getIntent().getStringExtra( "nomortlp" );
            iduser = getIntent().getStringExtra( "iduser" );
            nama = getIntent().getStringExtra( "nama" );
            email = getIntent().getStringExtra( "email" );
        }

        textDesc.setText( "Kamu tinggal memasukkan kode verifikasi yang kami SMS ke nomor HP-mu yang terdaftar +62" + phoneNumber.substring( 1 ) );

        //kirim verifikasi
        sendVerification("+62" + phoneNumber.substring( 1 ));

        final CountDownTimer countDownTimer = new CountDownTimer( 60000,1000 ){

            @Override
            public void onTick(long millisUntilFinished) {
                timerAndSend.setText( "00:" + millisUntilFinished / 1000 );
            }

            @Override
            public void onFinish() {
                timerAndSend.setText( "Kirim Ulang" );
                timerAndSend.setEnabled( true );
                timerAndSend.setTextColor( Color.parseColor( "#46a94c" ) );
                isFinished = 0;
                progressBar.setVisibility( View.GONE );
            }
        }.start();

        if(isFinished == 0){
            timerAndSend.setEnabled( true );
            timerAndSend.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText( VerifikasiActivity.this, "Kirim lagi", Toast.LENGTH_SHORT ).show();
                    countDownTimer.start();
                    timerAndSend.setEnabled( false );
                    timerAndSend.setTextColor( Color.parseColor( "#000000" ) );
                    sendVerification( "62" + phoneNumber.substring( 1 ) );
                    progressBar.setVisibility( View.VISIBLE );
                }
            } );
        }


        btnVerify.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String manualCode = materialEditText.getText().toString();
                verifyCode( manualCode );
            }
        } );

    }

    private void verifyCode(String code){
        PhoneAuthCredential phoneAuthCredential = PhoneAuthProvider.getCredential( verificationId, code );
            signInWithCredential(phoneAuthCredential);
    }


    private void signInWithCredential(PhoneAuthCredential phoneAuthCredential) {
        mAuth.signInWithCredential( phoneAuthCredential )
                .addOnCompleteListener( new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            progressBar.setVisibility( View.GONE );
                            sharedPreferences.createLoginSession( iduser,nama, phoneNumber,email,getDate() + "," + getTime() );
                            Intent intent = new Intent( VerifikasiActivity.this, MainActivity.class );
                            intent.setFlags( Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK );
                            startActivity( intent );
                        }else{
                            Toast.makeText( VerifikasiActivity.this, task.getException().getMessage(), Toast.LENGTH_LONG ).show();
                        }
                    }
                } );
    }


    private void sendVerification(String number) {
        progressBar.setVisibility( View.VISIBLE );
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                number,
                60,
                TimeUnit.SECONDS,
                TaskExecutors.MAIN_THREAD,
                mCallback
        );

    }

    private PhoneAuthProvider.OnVerificationStateChangedCallbacks
            mCallback = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

        @Override
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent( s, forceResendingToken );
            verificationId = s;
        }

        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
            String code = phoneAuthCredential.getSmsCode();
            if(code != null){
                materialEditText.setText( code );
                verifyCode( code );
            }
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            Toast.makeText( VerifikasiActivity.this, e.getMessage(), Toast.LENGTH_LONG ).show();
        }
    };

    private String getDate() {
        DateFormat dateFormat = new SimpleDateFormat("dd MMMM YYYY");
        Date date = new Date();

        return dateFormat.format(date);
    }

    private String getTime() {
        DateFormat dateFormat = new SimpleDateFormat("HH:mm");
        Date date = new Date();
        return dateFormat.format(date);
    }
}
