package com.example.tokoemak.tokoemak.fragments;


import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.tokoemak.tokoemak.Interface.ItemClickListener;
import com.example.tokoemak.tokoemak.R;
import com.example.tokoemak.tokoemak.ViewHolder.KategoriViewHolder;
import com.example.tokoemak.tokoemak.ViewHolder.OrderViewHolder;
import com.example.tokoemak.tokoemak.activities.ChatActivity;
import com.example.tokoemak.tokoemak.activities.OrderDetail;
import com.example.tokoemak.tokoemak.app.AppController;
import com.example.tokoemak.tokoemak.commons.SharedPreferences;
import com.example.tokoemak.tokoemak.model.KategoriModel;
import com.example.tokoemak.tokoemak.model.RequestModel;
import com.example.tokoemak.tokoemak.util.Server;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.Inflater;

import cn.pedant.SweetAlert.SweetAlertDialog;
import es.dmoral.toasty.Toasty;


/**
 * A simple {@link Fragment} subclass.
 */
public class OrdersFragment extends Fragment {
    public RecyclerView recyclerView;
    public RecyclerView.LayoutManager layoutManager;
    private FirebaseRecyclerOptions<RequestModel> options;
    private FirebaseRecyclerAdapter<RequestModel, OrderViewHolder> adapter;
    private FirebaseDatabase db;
    private DatabaseReference referenceRequest;
    private SharedPreferences sharedPreferences;
    private HashMap<String,String> user = new HashMap<String,String>(  );
    private Toolbar toolbar;
    private String number;
    private View rootView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private LinearLayout linearNullOrder;

    public OrdersFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
//        if(adapter.getItemCount() > 0){
            rootView = inflater.inflate( R.layout.ongoing_order, container, false );
            TextView texttoolbar = rootView.findViewById( R.id.textToolbarOrder );
            texttoolbar.setTypeface( Typeface.createFromAsset( getActivity().getAssets(),"fonts/Poppins-Medium.ttf" ) );
            TextView judul = rootView.findViewById( R.id.judulOrder );
            judul.setTypeface( Typeface.createFromAsset( getActivity().getAssets(),"fonts/Poppins-Medium.ttf" ) );
            sharedPreferences = new SharedPreferences( getContext() );
            user = sharedPreferences.getUserDetails();
            db = FirebaseDatabase.getInstance();
            referenceRequest = db.getReference("request");
            number = user.get( sharedPreferences.KEY_NUMBER );
            swipeRefreshLayout = rootView.findViewById( R.id.swipeRefreshOrder );
            linearNullOrder = rootView.findViewById( R.id.linearNullOrder );

            Toasty.Config.getInstance()
                    .tintIcon( true )
                    .setToastTypeface( Typeface.createFromAsset( getActivity().getAssets(), "fonts/Poppins-Regular.ttf" ) )
                    .allowQueue( true )
                    .apply();
            toolbar = rootView.findViewById( R.id.toolbarOrdersbaru );

            checkConnection();

            recyclerView = rootView.findViewById( R.id.rv_listOnGoingOrder );

            setOrderList();

            toolbar.inflateMenu( R.menu.menu_toolbar_order );
            toolbar.setOnMenuItemClickListener( new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                int id = menuItem.getItemId();
                switch (id){
                    case R.id.menuChat:
                        if(!isNetworkAvailable()){
                            setToastMessage( "Koneksi tidak stabil" );
                        }else{
                            startActivity( new Intent( getActivity(), ChatActivity.class ) );
                        }
                        break;
                }
                return false;
                }
        } );

            swipeRefreshLayout.setColorScheme(R.color.steel_blue,R.color.orange,R.color.colorAccent);
            swipeRefreshLayout.setOnRefreshListener( new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    if(!isNetworkAvailable()){
                        swipeRefreshLayout.setRefreshing( true );
                    }else{
                        swipeRefreshLayout.setRefreshing( false );
                        if(adapter.getItemCount() < 1){
                            linearNullOrder.setVisibility( View.VISIBLE );
                        }
                    }
                }
            } );

            return rootView;
    }

    private void checkMenu() {
        if(adapter.getItemCount() < 1){
            Menu menu = toolbar.getMenu();
            menu.getItem( 0 ).setVisible( false );
            menu.getItem( 0 ).setEnabled( false );
        }
    }


    private void setOrderList() {
        getOrderList(number);
        layoutManager = new LinearLayoutManager( getContext() );
        recyclerView.setLayoutManager( layoutManager );
        recyclerView.setHasFixedSize( true );
        setAdapter();
    }


    private void setAdapter()
    {
        adapter.startListening();
        adapter.notifyDataSetChanged();
        recyclerView.setAdapter( adapter );

    }

    private void getOrderList(String number) {
        options = new FirebaseRecyclerOptions.Builder<RequestModel>()
                .setQuery(referenceRequest.orderByChild( "phone" )
                                .equalTo( number )
                        , RequestModel.class).build();
        adapter = new FirebaseRecyclerAdapter<RequestModel, OrderViewHolder>(options) {

            @Override
            protected void onBindViewHolder(@NonNull OrderViewHolder holder, final int position, @NonNull final RequestModel model) {
                if (adapter.getItemCount() > 0) {
                    linearNullOrder.setVisibility( View.GONE );
                } else {
                    linearNullOrder.setVisibility( View.VISIBLE );
                }

                holder.kode.setText( "TM-" + adapter.getRef( position ).getKey() );
                holder.status.setText( convertKodeStatus(model.getStatus()) );
                holder.alamat.setText( setAlamat(model.getAlamat()) );

                holder.kode.setTypeface( Typeface.createFromAsset( getActivity().getAssets(),"fonts/Poppins-Medium.ttf" ));

                holder.stepView.getState().stepsNumber( 4 )
                        .commit();
                holder.stepView.go( Integer.parseInt( model.getStatus() ), true );
                holder.stepView.done( true );

                holder.setItemClickListener( new ItemClickListener() {
                    @Override
                    public void onClick(View view, int position, boolean isLongClick) {
                        String umpanBalik,status;

                        umpanBalik = "Barang anda sedang ";
                        status = convertKodeStatus( model.getStatus() );

                        if (status.equals( "Terkirim" )) {
                            umpanBalik = "Barang anda telah";
                        }
                        Toast.makeText( getActivity(), umpanBalik + status, Toast.LENGTH_SHORT ).show();
                    }
                } );

                holder.cancel.setOnClickListener( new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new SweetAlertDialog( getActivity(), SweetAlertDialog.WARNING_TYPE )
                                .setTitleText( "Apakah anda yakin?" )
                                .setContentText( "Pesanan akan dibatalkan" )
                                .setConfirmText( "Batalkan" )
                                .setConfirmClickListener( new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        deleteOrderFromServer(adapter.getRef( position ).getKey(),sDialog,adapter,position);
                                        if(adapter.getItemCount() < 2){
                                            linearNullOrder.setVisibility( View.VISIBLE );
                                        }
                                    }
                                } )
                                .show();
                    }
                } );

                holder.detail.setOnClickListener( new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent( getActivity(), OrderDetail.class );
                        intent.putExtra( "kode", adapter.getRef( position ).getKey() );
                        startActivity( intent );
                    }
                } );

                if(holder.status.getText().toString().equals( "Dibatalkan" )){
                    new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Maaf Ya Heh ..")
                            .setContentText("Orderanmu ditolak!")
                            .show();

                    Thread timer = new Thread(){
                        @Override
                        public void run() {
                            try{
                                sleep( 3000 );
                            }catch (Exception e){
                                e.printStackTrace();
                            }finally {
                                referenceRequest.child( adapter.getRef( position ).getKey() ).removeValue();
                            }
                        }
                    };

                    timer.start();
                }else if(holder.status.getText().toString().equals( "Diterima" )){
                    referenceRequest.child( adapter.getRef( position ).getKey() ).removeValue();
                }


                if(!holder.status.getText().toString().equals( "Diproses" )){
                    holder.cancel.setTextColor( getResources().getColor( R.color.white ) );
                    holder.cancel.setBackgroundResource(R.drawable.btn_disable);
                    holder.cancel.setEnabled( false );
                }else{
                    holder.cancel.setTextColor( getResources().getColor( R.color.white ) );
                    holder.cancel.setBackgroundResource( R.drawable.btn_tambah_item );
                    holder.cancel.setEnabled( true );
                }

                if(holder.status.getText().toString().equals( "Diproses" )){
                    holder.imgStatus.setImageResource( R.drawable.waitingconfirm );
                }else if(holder.status.getText().toString().equals( "Dikemas" )){
                    holder.imgStatus.setImageResource( R.drawable.orderconfirmed );
                }else if(holder.status.getText().toString().equals( "Dikirim" )){
                    holder.imgStatus.setImageResource( R.drawable.deliveryorder );
                }else{
                    holder.imgStatus.setImageResource( R.drawable.orderaccepted );
                }

            }

            @NonNull
            @Override
            public OrderViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                View view = LayoutInflater.from( viewGroup.getContext() ).inflate( R.layout.ongoingorder_layout, viewGroup, false );
                return new OrderViewHolder(view);
            }
        };
    }


    private void deleteOrderFromServer(final String key, final SweetAlertDialog sDialog, final FirebaseRecyclerAdapter<RequestModel, OrderViewHolder> adapter, final int position) {
        StringRequest stringRequest = new StringRequest( Request.Method.POST, Server.REST_API_TRANSAKSI, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject( response );
                    if(jsonObject.getBoolean( "status" )){
                        referenceRequest.child( adapter.getRef( position ).getKey() ).removeValue();
                        sDialog
                                .setTitleText( "Dibatalkan" )
                                .setContentText( "Pesanan kamu telah dibatalkan" )
                                .setConfirmText( "OKE" )
                                .setConfirmClickListener( null )
                                .changeAlertType( SweetAlertDialog.SUCCESS_TYPE );

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                sDialog
                        .setTitleText( "Gagal" )
                        .setContentText( "Pesanan kamu gagal dibatalkan" )
                        .setConfirmText( "HMM OKE DEH" )
                        .setConfirmClickListener( null )
                        .changeAlertType( SweetAlertDialog.ERROR_TYPE );
            }
        } ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String>params = new HashMap<>();
                params.put( "global_act","delete" );
                params.put("idstatus","5");
                params.put("kodetrans",key);
                params.put( "params","dibatalkanpada" );
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue( stringRequest,"json_obj_req" );
    }


    private void checkConnection() {
        if(!isNetworkAvailable()){
            setToastMessage("Koneksi tidak stabil");
        }
    }


    private void setToastMessage(String msg) {
        Toast toasty = Toasty.normal( getActivity(), msg, getActivity().getResources().getDrawable( R.drawable.nointernet ) );
        toasty.setGravity( Gravity.TOP|Gravity.CENTER_HORIZONTAL,0,0 );
        toasty.show();
    }


    private String setAlamat(String alamat) {
        if(alamat.length() > 29){
            return alamat.substring( 0, 29 ) + " ...";
        }else{
            return alamat;
        }
    }


    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    private String convertKodeStatus(String status) {
        if(status.equals( "0" ))
            return "Diproses";
        else if(status.equals( "1" ))
            return "Dikemas";
        else if(status.equals( "2" ))
            return "Dikirim";
        else if (status.equals( "3" ))
            return "Diterima";
        else
            return "Dibatalkan";
    }

    @Override
    public void onStart() {
        super.onStart();
        if(adapter != null){
            adapter.startListening();
        }
        checkConnection();
    }

    @Override
    public void onResume() {
        super.onResume();
        if(adapter != null){
            adapter.startListening();
        }
        checkConnection();
    }

    @Override
    public void onStop() {
        super.onStop();
        if(adapter != null){
            adapter.stopListening();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
