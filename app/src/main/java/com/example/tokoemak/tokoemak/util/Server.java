package com.example.tokoemak.tokoemak.util;

public class Server {
    public static final String URL_PRODUK="http://krisnafitra.site/tokoemak-api/produk/getBarang.php";
    public static final String URL_SEARCH_PRODUK="http://krisnafitra.site/tokoemak-api/produk/searchBarang.php";

    //latest rest api
    public static final String REST_API_USERS = "http://krisnafitra.site/rest-api/api/users";
    public static final String REST_API_KATEGORI = "http://krisnafitra.site/rest-api/api/kategori";
    public static final String REST_API_PRODUK = "http://krisnafitra.site/rest-api/api/produk";
    public static final String REST_API_PRODUK_FAV = "http://krisnafitra.site/rest-api/api/ProdukFav";
    public static final String REST_API_KONTEN = "http://krisnafitra.site/rest-api/api/konten";
    public static final String REST_API_PENGATURAN = "http://krisnafitra.site/rest-api/api/pengaturan";
    public static final String REST_API_ONGKIR = "http://krisnafitra.site/rest-api/api/ongkir";
    public static final String REST_API_TRANSAKSI = "http://krisnafitra.site/rest-api/api/transaksi";
    public static final String REST_API_PROMO = "http://krisnafitra.site/rest-api/api/promo";

}
