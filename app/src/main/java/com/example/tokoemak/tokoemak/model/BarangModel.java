package com.example.tokoemak.tokoemak.model;

public class BarangModel {
    private String nama,gambar,harga,menuid;

    public BarangModel(String nama, String gambar, String harga, String menuid) {
        this.nama = nama;
        this.gambar = gambar;
        this.harga = harga;
        this.menuid = menuid;
    }

    public BarangModel() {
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }

    public String getMenuid() {
        return menuid;
    }

    public void setMenuid(String menuid) {
        this.menuid = menuid;
    }
}
