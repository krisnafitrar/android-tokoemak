package com.example.tokoemak.tokoemak.adapter;

import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.example.tokoemak.tokoemak.Interface.ItemClickListener;
import com.example.tokoemak.tokoemak.R;
import com.example.tokoemak.tokoemak.activities.CartActivity;
import com.example.tokoemak.tokoemak.databases.Database;
import com.example.tokoemak.tokoemak.model.OrderModel;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

class CartViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
    public TextView namaCartList,hargaCartList;
    public ElegantNumberButton numberButtonCart;

    private ItemClickListener itemClickListener;


    public void setNamaCartList(TextView namaCartList) {
        this.namaCartList = namaCartList;
    }


    public void setHargaCartList(TextView hargaCartList) {
        this.hargaCartList = hargaCartList;
    }

    public CartViewHolder(@NonNull View itemView) {
        super( itemView );
        namaCartList = itemView.findViewById( R.id.namaCartList );
        numberButtonCart = itemView.findViewById( R.id.number_button_cart );
        hargaCartList = itemView.findViewById( R.id.hargaCartList );
    }

    @Override
    public void onClick(View v) {
        itemClickListener.onClick( v , getAdapterPosition(), false );
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }
}

public class CartAdapter extends RecyclerView.Adapter<CartViewHolder>{

    private List<OrderModel> modelList = new ArrayList<>();
    private CartActivity mCart;

    public CartAdapter(List<OrderModel> modelList, CartActivity mContext) {
        this.modelList = modelList;
        this.mCart = mContext;
    }

    @NonNull
    @Override
    public CartViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from( mCart );
        View itemView = inflater.inflate( R.layout.cart_layout, viewGroup, false );

        return new CartViewHolder( itemView );
    }

    @Override
    public void onBindViewHolder(@NonNull final CartViewHolder cartViewHolder, final int position) {
        final Typeface styleku = Typeface.createFromAsset( mCart.getAssets(), "fonts/Poppins-Medium.ttf" );
        cartViewHolder.namaCartList.setTypeface( styleku );
        cartViewHolder.hargaCartList.setTypeface( styleku );
        cartViewHolder.namaCartList.setText( setNama(modelList.get( position ).getNamaProduk()) );
        cartViewHolder.numberButtonCart.setNumber( modelList.get( position ).getJumlah() );
        cartViewHolder.numberButtonCart.setOnValueChangeListener( new ElegantNumberButton.OnValueChangeListener() {
            @Override
            public void onValueChange(ElegantNumberButton view, int oldValue, int newValue) {
                if(newValue > 0){
                    new Database( mCart )
                            .updateCart( newValue, modelList.get( position ).getNamaProduk() );
                    mCart.loadListItem(mCart.ongkirPrice);
                }else{
                    new Database( mCart )
                            .deleteItem( modelList.get( position ).getNamaProduk() );
                    mCart.loadListItem(mCart.ongkirPrice);
                }
            }
        } );

        //init harga
        Locale localeID = new Locale("in", "ID");
        NumberFormat formatHarga = NumberFormat.getCurrencyInstance( localeID );
        int harga = (Integer.parseInt( modelList.get( position ).getHarga() )*(Integer.parseInt( modelList.get( position ).getJumlah() )));
        cartViewHolder.hargaCartList.setText( formatHarga.format(harga) );

    }

    private String setNama(String namaProduk) {
        String namaBarang = namaProduk;

        if(namaBarang.length() >= 39){
            return namaBarang.substring( 0,39 ) + " ...";
        }else{
            return namaBarang;
        }
    }

    @Override
    public int getItemCount() {
        return modelList.size();
    }
}
