package com.example.tokoemak.tokoemak.model;

public class FilterModel {
    String id,kode_kategori,nama;

    public FilterModel() {
    }

    public FilterModel(String id, String kode_kategori, String nama) {
        this.id = id;
        this.kode_kategori = kode_kategori;
        this.nama = nama;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKode_kategori() {
        return kode_kategori;
    }

    public void setKode_kategori(String kode_kategori) {
        this.kode_kategori = kode_kategori;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }
}
