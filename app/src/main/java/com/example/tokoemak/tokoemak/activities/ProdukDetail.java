package com.example.tokoemak.tokoemak.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.location.Location;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.example.tokoemak.tokoemak.R;
import com.example.tokoemak.tokoemak.app.AppController;
import com.example.tokoemak.tokoemak.commons.SharedPreferences;
import com.example.tokoemak.tokoemak.databases.Database;
import com.example.tokoemak.tokoemak.model.OrderModel;
import com.example.tokoemak.tokoemak.util.Server;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import es.dmoral.toasty.Toasty;
import io.supercharge.shimmerlayout.ShimmerLayout;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ProdukDetail extends AppCompatActivity {
    private ImageView gambarProduk;
    private TextView namaProduk,hargaProduk,stokProduk,terjualProduk,informasiProduk,deskripsiProduk,titleDesk,titleKeranjang,titleOrder,hargaDiskon,diskonLabel;
    private String kode,nama,harga,stok,terjual,gambar,deskripsi,kota,hargadiskons,diskons;
    private Typeface styleFont;
    private LinearLayout btnKeranjang,btnPesan;
    private ElegantNumberButton btnQTY;
    private SharedPreferences sharedPreferences;
    private HashMap<String,String> user = new HashMap<String, String>();
    private HashMap<String,String> region = new HashMap<String, String>();
    private String idUser,jumlah,namaProduk2;
    private List<OrderModel>mList = new ArrayList<>();
    private Locale localeID = new Locale("in", "ID");
    private NumberFormat formatHarga = NumberFormat.getCurrencyInstance( localeID );
    private String[] reg;
    private ShimmerLayout shimmerLayout;
    private double latitude,longitude,mLatitude,mLongitude,distanceOnMeter,distanceOnKilo;
    private List<OrderModel> cartList = new ArrayList<>();
    private String lat,longi;
    private int finalPrice = 0;
    private Toolbar toolbar;
    private CoordinatorLayout detailLayout;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext( CalligraphyContextWrapper.wrap( newBase ) );
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        CalligraphyConfig.initDefault( new CalligraphyConfig.Builder().setDefaultFontPath( "fonts/roboto.ttf" )
                .setFontAttrId( R.attr.fontPath )
                .build());
        setContentView( R.layout.activity_produk_detail );

        sharedPreferences = new SharedPreferences( this );
        user = sharedPreferences.getUserDetails();
        idUser = user.get( sharedPreferences.KEY_IDUSER );

        detailLayout = findViewById( R.id.detailLayout );

        //get Intent
        loadDetail();

        //get latitude and longitude of store
        getLatLong();

        //init toolbar
        manageToolbar();

        //assign menuItem
        MenuItem menuItem =  toolbar.getMenu().findItem( R.id.menuLike );

        //get fav from db
        getLike(menuItem,idUser,kode);

        toolbar.setOnMenuItemClickListener( new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.menuLike:
                        //method like barang
                        setFavorit(menuItem,kode,idUser);
                        break;
                    case R.id.menuLaporkan:
                        setToastMessage( "Menu Laporkan!" );
                        break;
                }
                return false;
            }
        } );

        //manage CollapsingToolbar
        CollapsingToolbarLayout collapsingToolbarLayout = findViewById( R.id.coll );
        collapsingToolbarLayout.setExpandedTitleColor( Color.argb(0,0,0,0) );

        //init view
        initView();

        //set title toolbar
        collapsingToolbarLayout.setTitle( "Detail Produk" );
        collapsingToolbarLayout.setCollapsedTitleTypeface( styleFont );
        collapsingToolbarLayout.setCollapsedTitleTextColor( Color.parseColor( "#000000" ) );

        //set value to view
        namaProduk.setText( nama );
        namaProduk.setTypeface( styleFont );

        //assign harga produk
        finalPrice = Integer.parseInt( harga ) - ((Integer.parseInt( diskons ) * Integer.parseInt( harga )) / 100);

        if(Integer.parseInt( diskons ) > 0){
            shimmerLayout.setVisibility( View.VISIBLE );
            shimmerLayout.startShimmerAnimation();
            hargaProduk.setText( formatHarga.format(finalPrice ) );
            hargaDiskon.setText( formatHarga.format(Integer.parseInt( harga ) ) );
            hargaDiskon.setPaintFlags(hargaDiskon.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            diskonLabel.setText( diskons + "% Off" );
        }else{
            shimmerLayout.setVisibility( View.GONE );
            shimmerLayout.stopShimmerAnimation();
            hargaDiskon.setVisibility( View.GONE );
            hargaProduk.setText( formatHarga.format(Integer.parseInt( harga )) );
        }

        //set Produk information by data from intent
        stokProduk.setText( stok );
        terjualProduk.setText( "-" );
        deskripsiProduk.setText( deskripsi );
        Glide.with( this )
                .load(gambar)
                .into( gambarProduk );

        btnKeranjang.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( ProdukDetail.this,CartActivity.class );
                intent.putExtra( "lat", latitude );
                intent.putExtra( "long", longitude );
                startActivity( intent );
            }
        } );


        btnPesan.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //check jaraknya
                getDistanceOf(mLatitude,mLongitude,latitude,longitude);
                if(distanceOnKilo < 8){
                    if(Integer.parseInt( stok ) > 0){
                        setCart();
                        titleKeranjang.setText( "Keranjangmu #" + countCart() );
                    }else{
                        showSnackbar( "Maaf stok tidak tersedia" );
                    }
                }else{
                    showSnackbar( "Maaf wilayah anda di luar jangkauan" );
                }
            }
        } );

    }

    private void manageToolbar() {
        toolbar = findViewById( R.id.toolbarProdukDetail );
        toolbar.inflateMenu( R.menu.menu_toolbar );
        toolbar.setNavigationIcon( R.drawable.ic_arrow_back_black_24dp );
        toolbar.setNavigationOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        } );
    }


    private void getLatLong() {
        String urlWithParams = Server.REST_API_PENGATURAN + "?act=getlatlong&key1=mart_latitude&key2=mart_longitude";
        StringRequest stringRequest = new StringRequest( Request.Method.GET, urlWithParams, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject data = new JSONObject( response );
                    if(data.getBoolean( "status" ) == true){
                         lat = data.getString( "latitude" );
                         longi = data.getString( "longitude" );
                         mLatitude = Double.valueOf( lat );
                         mLongitude = Double.valueOf( longi );
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        } );

        AppController.getInstance().addToRequestQueue( stringRequest,"json_obj_req" );
    }


    private void getDistanceOf(double mLatitude, double mLongitude, double latitude, double longitude) {
        //set store position
        Location location = new Location( "startPoint" );
        location.setLatitude( mLatitude );
        location.setLongitude( mLongitude );

        //set user position
        Location uLocation = new Location( "endpoint" );
        uLocation.setLatitude( latitude );
        uLocation.setLongitude( longitude );

        distanceOnMeter = location.distanceTo( uLocation );
        distanceOnKilo = distanceOnMeter / 1000;
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(countCart() > 0){
            titleKeranjang.setText( "Keranjangmu #" + countCart() );
        }else{
            titleKeranjang.setText( "Keranjangmu" );
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    private void setCart() {
        mList = new Database( getBaseContext())
                .getCarts();

            boolean updateCart  = false;
            int index = 0;
            for(int i = 0; i < mList.size(); i++){
                if(mList.get( i ).getNamaProduk().equals(nama)){
                    updateCart = true;
                    index = i;
                }
            }

            if(mList.size() < 1 || updateCart == false){
                new Database( getBaseContext() )
                        .addToCart( new OrderModel(
                                kode, nama, btnQTY.getNumber(),
                                String.valueOf( finalPrice ), "0", gambar
                        ) );
                showSnackbar( "Menambahkan ke keranjang" );
            }else{
                jumlah = mList.get( index ).getJumlah();
                namaProduk2 = mList.get( index ).getNamaProduk();
                new Database( getBaseContext() )
                        .updateCart( Integer.parseInt( btnQTY.getNumber() )+ Integer.parseInt( jumlah ), namaProduk2 );
                showSnackbar( "Menambahkan ke keranjang" );
            }

        }


    private void showSnackbar(String msg)
    {
        Snackbar.make( detailLayout,msg, Snackbar.LENGTH_SHORT ).show();
    }

    private void setToastMessage(String msg){
        Toast toasty = Toasty.normal( ProdukDetail.this, msg);
        toasty.setGravity( Gravity.TOP|Gravity.CENTER_HORIZONTAL,0,0 );
        toasty.show();
    }


    private void initView() {
        btnQTY = findViewById( R.id.number_button_produk );
        btnKeranjang = findViewById( R.id.btnKeranjangAnda );
        btnPesan = findViewById( R.id.btnPesanProduk );
        namaProduk = findViewById( R.id.namaProdukDetail );
        hargaProduk = findViewById( R.id.hargaProdukDetail );
        stokProduk = findViewById( R.id.stokProduk );
        terjualProduk = findViewById( R.id.terjualProduk );
        gambarProduk = findViewById( R.id.gambarProdukDetail );
        informasiProduk = findViewById( R.id.informasiProduk );
        deskripsiProduk = findViewById( R.id.deskripsiDetail );
        titleDesk = findViewById( R.id.titleDeskripsi );
        titleOrder = findViewById( R.id.titleOrder );
        titleKeranjang = findViewById( R.id.titleKeranjangmu );
        hargaDiskon = findViewById( R.id.hargaDiskonProdukDetail );
        diskonLabel = findViewById( R.id.diskonLabelDetail );
        shimmerLayout = findViewById( R.id.shimmerBadgesDiskonDetail );
        styleFont = Typeface.createFromAsset( getAssets(),"fonts/Poppins-Medium.ttf" );
        informasiProduk.setTypeface( styleFont );
        titleDesk.setTypeface( styleFont );
        titleOrder.setTypeface( styleFont );
        titleKeranjang.setTypeface( styleFont );
        diskonLabel.setTypeface( styleFont );
    }


    private void getLike(final MenuItem menuItem, final String idUserr, final String kodee) {
        String urlWithParams = Server.REST_API_PRODUK_FAV + "?id=" + idUserr + "&idbarang=" + kodee + "&act=count";
        StringRequest stringRequest = new StringRequest( Request.Method.GET, urlWithParams, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    JSONObject jsonObject = new JSONObject( response );
                    int count = jsonObject.getInt( "count" );

                    if(count < 1) {
                        menuItem.setIcon( R.drawable.ic_favorite_black_24dp );
                    }else {
                        menuItem.setIcon( R.drawable.ic_favorite_pink_24dp );
                    }
                } catch (JSONException e) {
                    Toast.makeText( ProdukDetail.this, e.getMessage(), Toast.LENGTH_SHORT ).show();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText( ProdukDetail.this, error.getMessage(), Toast.LENGTH_SHORT ).show();
                error.printStackTrace();
            }
        } );

        AppController.getInstance().addToRequestQueue( stringRequest,"json_obj_req" );
    }


    private void setFavorit(final MenuItem menuItem, final String kode, final String idUser) {
        StringRequest stringRequest = new StringRequest( Request.Method.POST, Server.REST_API_PRODUK_FAV, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    JSONObject jsonObject = new JSONObject( response );
                    boolean status = jsonObject.getBoolean( "status" );
                    String act = jsonObject.getString( "act" );

                    if(status)
                        switch (act){
                            case "insert":
                                menuItem.setIcon(R.drawable.ic_favorite_pink_24dp);
                                break;
                            case "delete":
                                menuItem.setIcon(R.drawable.ic_favorite_black_24dp);
                                break;
                        }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText( ProdukDetail.this, e.getMessage(), Toast.LENGTH_SHORT ).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Toast.makeText( ProdukDetail.this, error.getMessage(), Toast.LENGTH_SHORT ).show();
            }
        } ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id", idUser);
                params.put( "idbarang",kode);

                return params;
            }
        };

        AppController.getInstance().addToRequestQueue( stringRequest,"json_obj_req" );
    }


    private void loadDetail() {
        if(getIntent() != null){
            kode = getIntent().getStringExtra( "kode_barang" );
            nama = getIntent().getStringExtra( "nama" );
            harga = getIntent().getStringExtra( "harga" );
            stok = getIntent().getStringExtra( "stok" );
            gambar = getIntent().getStringExtra( "gambar" );
            deskripsi = getIntent().getStringExtra( "deskripsi" );
            diskons = getIntent().getStringExtra( "diskon" );
            latitude = getIntent().getDoubleExtra( "latitude",0 );
            longitude = getIntent().getDoubleExtra( "longitude",0 );
        }
    }


    public int countCart()
    {
        int x = 0;

        cartList = new Database( ProdukDetail.this ).getCarts();

        for (OrderModel orderModel:cartList)
            x += Integer.parseInt( orderModel.getJumlah() );

        return x;

    }

}
