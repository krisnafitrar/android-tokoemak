package com.example.tokoemak.tokoemak.commons;

import android.content.Context;
import android.content.Intent;

import com.example.tokoemak.tokoemak.activities.LoginActivity;
import com.example.tokoemak.tokoemak.activities.MainActivity;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SharedPreferences {
    android.content.SharedPreferences sharedPreferences;
    android.content.SharedPreferences.Editor editor;
    Context _context;
    boolean used;


    int PRIVATE_MODE = 0;
    private static final String PREF_NAME = "AndroidHivePref";

    // All Shared Preferences Keys
    private static final String IS_LOGIN = "IsLoggedIn";

    // User name (make variable public to access from outside)
    public static final String KEY_IDUSER = "iduser";
    public static final String KEY_NAME = "name";

    // Email address (make variable public to access from outside)
    public static final String KEY_EMAIL = "email";

    public static final String KEY_NUMBER = "number";
    public static final String KEY_TANGGAL = "tanggal";

    public static final String  KEY_SIZE = "no";
    public static final String KEY_ALAMAT = "";
    public static final String KEY_REGION = "";

    public static final int DISKON = 0;
    public static final int ONGKIR = 0;
    public static final int SEMENTARA = 0;
    public static final int TOTAL = 0;
    public static final int TOTAL_AWAL = 0;
    public static final float JARAK = 0;
    public static final String LONGITUDE = "";
    public static final String LATITUDE = "";
    public static final String JADWAL_PENGIRIMAN = "";
    public static final int COUNT = 0;

    public SharedPreferences(Context context){
        this._context = context;
        sharedPreferences = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = sharedPreferences.edit();
    }

    /**
     * Create login session
     * */
    public void createLoginSession(String idUser,String username, String number,String email, String tanggal){
        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true);

        // Storing name in pref
        editor.putString( KEY_IDUSER, idUser );
        editor.putString(KEY_NAME, username);
        editor.putString(KEY_NUMBER, number);
        editor.putString( KEY_EMAIL, email );
        editor.putString(KEY_TANGGAL, tanggal );

        // Storing email in pref

        // commit changes
        editor.commit();
    }

    public void setView(String size){
        // Storing size
        editor.putString( KEY_SIZE, size );

        editor.commit();
    }

    public void setCount(int count){
        editor.putInt( String.valueOf( COUNT ), count );
        editor.commit();
    }


    public void setLatitude(String latitude){
        //storing latitude
        editor.putString( LATITUDE, latitude );
        editor.commit();
    }

    public void setLongitude(String longitude){
        editor.putString( LONGITUDE, longitude );
        editor.commit();
    }


    public void setJarak(Float jarak){
        editor.putFloat( String.valueOf( JARAK ), jarak );
        editor.commit();
    }

    public void setTotalAwal(int totalAwal){
        editor.putInt( String.valueOf( TOTAL_AWAL ), totalAwal );
        editor.commit();
    }

    public void setDiskon(int diskon){
        editor.putInt( String.valueOf( DISKON ), diskon );
        editor.commit();
    }

    public void setOngkir(int ongkir){
        editor.putInt( String.valueOf( ONGKIR ), ongkir );
        editor.commit();
    }

    public void setSementara(int sementara){
        editor.putInt( String.valueOf( SEMENTARA ), sementara );
        editor.commit();
    }

    public void setTotal(int total){
        editor.putInt( String.valueOf( TOTAL ), total );
        editor.commit();
    }

    public void setAlamat(String alamat){
        editor.putString( KEY_ALAMAT, alamat );
        editor.commit();
    }

    public void setRegion(String region){
        editor.putString( KEY_REGION, region );
        editor.commit();
    }

    public void setJadwalPengiriman(String jadwalPengiriman){
        editor.putString( JADWAL_PENGIRIMAN, jadwalPengiriman );
        editor.commit();
    }

    public HashMap<String, String> getAlamat(){
     HashMap<String,String> alamat = new HashMap<String,String>(  );

     alamat.put( KEY_ALAMAT, sharedPreferences.getString( KEY_ALAMAT , "" ));

     return alamat;
    }

    public HashMap<String, String> getRegion(){
        HashMap<String,String> region = new HashMap<String,String>(  );

        region.put( KEY_REGION, sharedPreferences.getString( KEY_REGION , "" ));

        return region;
    }

    public HashMap<Integer, Integer> getCount(){
        HashMap<Integer, Integer> count = new HashMap<Integer, Integer>(  );

        count.put( COUNT, sharedPreferences.getInt( String.valueOf( COUNT ), 0 ) );
        return count;
    }

    public HashMap<String,String> getLatitude(){
        HashMap<String,String> latitude = new HashMap<String, String>(  );

        latitude.put( LATITUDE, sharedPreferences.getString( LATITUDE, "" ) );

        return latitude;
    }

    public HashMap<String,String> getLongitude(){
        HashMap<String,String> longitude = new HashMap<String, String>(  );

        longitude.put( LONGITUDE, sharedPreferences.getString( LONGITUDE, "" ) );

        return longitude;
    }

    public HashMap<String,String> getJadwal(){
        HashMap<String,String> jadwal = new HashMap<String, String>(  );

        jadwal.put( JADWAL_PENGIRIMAN, sharedPreferences.getString( JADWAL_PENGIRIMAN, "" ) );

        return jadwal;
    }



    public HashMap<Float, Float> getJarak(){
        HashMap<Float,Float> jarak = new HashMap<Float,Float>(  );

        jarak.put( JARAK, sharedPreferences.getFloat( String.valueOf( JARAK ), 0 ));

        return jarak;
    }


    /**
     * Get stored session data
     * */

    public HashMap<Integer, Integer> getVariableDetails(){
        HashMap<Integer, Integer> variable = new HashMap<Integer, Integer>();
        // user name
        variable.put(DISKON, sharedPreferences.getInt( String.valueOf( DISKON ), 0 ));
        variable.put(TOTAL, sharedPreferences.getInt( String.valueOf( TOTAL ), 0 ));
        variable.put(ONGKIR, sharedPreferences.getInt( String.valueOf( ONGKIR ), 0 ));
        variable.put(SEMENTARA, sharedPreferences.getInt( String.valueOf( SEMENTARA ), 0 ));
        variable.put( TOTAL_AWAL, sharedPreferences.getInt( String.valueOf( TOTAL_AWAL ), 0 ) );
        // user email id
//        user.put(KEY_EMAIL, sharedPreferences.getString(KEY_EMAIL, null));

        // return user
        return variable;
    }

    public HashMap<String, String> getUserDetails(){
        HashMap<String, String> user = new HashMap<String, String>();
        // user name
        user.put( KEY_IDUSER, sharedPreferences.getString( KEY_IDUSER, null ) );
        user.put(KEY_NAME, sharedPreferences.getString(KEY_NAME, null));

        // user email id
        user.put(KEY_EMAIL, sharedPreferences.getString(KEY_EMAIL, null));

        user.put(KEY_NUMBER, sharedPreferences.getString(KEY_NUMBER, null));
        user.put( KEY_TANGGAL, sharedPreferences.getString( KEY_TANGGAL, null ) );

        // return user
        return user;
    }

    public HashMap<String, String> getSizeView(){
        HashMap<String, String> size = new HashMap<String, String>(  );

        size.put( KEY_SIZE, sharedPreferences.getString( KEY_SIZE, "no" ) );

        return size;
    }


    /**
     * Check login method wil check user login status
     * If false it will redirect user to login page
     * Else won't do anything
     * */
    public void checkLogin(){
        // Check login status
        if(this.isLoggedIn()){
            System.out.println("##### SUDAH LOGIN");
            // user is not logged in redirect him to Login Activity
            Intent i = new Intent(_context, MainActivity.class);
            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            // Add new Flag to start new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            // Staring Login Activity
            _context.startActivity(i);
        }

    }

//    public List<VoucherModel> getModelVouchers() {
//        Gson gson = new Gson();
//        String jsonText = sharedPreferences.getString("vouchersaya", null);
//        Type collectionType = new TypeToken<List<VoucherModel>>(){}.getType();
//        modelVouchers = (List<VoucherModel>) gson.fromJson(jsonText, collectionType);
//        return modelVouchers;
//    }

//    public void setModelVouchers(VoucherModel modelVoucherList) {
//        Gson gson = new Gson();
//
//        try{
//
//            String jsonText = sharedPreferences.getString( "vouchersaya", null );
//            Type collectionType = new TypeToken<List<VoucherModel>>() {}.getType();
//            modelVouchers = (List<VoucherModel>) gson.fromJson( jsonText, collectionType );
//
//            int i = 0;
//            for (i = 0; i < modelVouchers.size(); i++) {
//                if (modelVouchers.get( i ).getId().equals(modelVoucherList.getId())) {
//                    used = true;
//                    break;
//                }
//            }
//        } catch (NullPointerException e){
//            used = false;
//        }
//
//        if(used != true){
//            try {
//                modelVouchers.add( modelVoucherList );
//            } catch (NullPointerException e){
//                this.modelVouchers = new ArrayList<>(  );
//                modelVouchers.add( modelVoucherList );
//            }
//            String jsonTextnew = gson.toJson(modelVouchers);
//            editor.putString("vouchersaya", jsonTextnew);
//            editor.apply();
//        }
//
//
//    }
    /**
     * Clear session details
     * */
    public void logoutUser(){
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();

        // After logout redirect user to Loing Activity
        Intent i = new Intent(_context, LoginActivity.class );
        // Closing all the Activities
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        // Add new Flag to start new Activity
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        // Staring Login Activity
        _context.startActivity(i);
    }

    /**
     * Quick check for login
     * **/
    // Get Login State
    public boolean isLoggedIn(){
        return sharedPreferences.getBoolean(IS_LOGIN, false);
    }

}
