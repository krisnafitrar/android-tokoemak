package com.example.tokoemak.tokoemak.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.tokoemak.tokoemak.Configuration.Konfigurasi;
import com.example.tokoemak.tokoemak.Configuration.RequestHandler;
import com.example.tokoemak.tokoemak.R;
import com.example.tokoemak.tokoemak.app.AppController;
import com.example.tokoemak.tokoemak.model.UserModel;
import com.example.tokoemak.tokoemak.util.Server;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import dmax.dialog.SpotsDialog;
import es.dmoral.toasty.Toasty;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener {
    private RelativeLayout root;
    private MaterialEditText nama;
    private MaterialEditText phone;
    private MaterialEditText email;
    private String name, phonee, emailku;
    private boolean status;
    private String url = Server.REST_API_USERS;
    private ConnectivityManager connectivityManager;
    private String tag_json_obj = "json_obj_req";
    private TextView namaDaftar,noDaftar,emailDaftar;
    private Button daftar;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext( CalligraphyContextWrapper.wrap( newBase ) );
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        CalligraphyConfig.initDefault( new CalligraphyConfig.Builder().setDefaultFontPath( "fonts/Poppins-Regular.ttf" )
                .setFontAttrId( R.attr.fontPath )
                .build() );
        setContentView( R.layout.activity_sign_up );

        TextView titleDaftar = findViewById( R.id.text_daftar );
        namaDaftar = findViewById( R.id.namaDaftar );
        noDaftar = findViewById( R.id.nomorDaftar );
        emailDaftar = findViewById( R.id.emailDaftar );

        Typeface style = Typeface.createFromAsset( getAssets(), "fonts/Poppins-Medium.ttf" );
        titleDaftar.setTypeface( style );
        namaDaftar.setTypeface( style );
        noDaftar.setTypeface( style );
        emailDaftar.setTypeface( style );

        connectivityManager = (ConnectivityManager) getSystemService( Context.CONNECTIVITY_SERVICE );
        {
            if (connectivityManager.getActiveNetworkInfo() != null
                    && connectivityManager.getActiveNetworkInfo().isConnected()
                    && connectivityManager.getActiveNetworkInfo().isAvailable()) {

            } else {
                Toast.makeText( this, "Akses internet tidak aktif", Toast.LENGTH_LONG ).show();
            }
        }

        nama = findViewById( R.id.textBoxNamaSignUp );
        phone = findViewById( R.id.textBoxPhoneSignUp );
        email = findViewById( R.id.textBoxEmailSignUp );
        root = findViewById( R.id.root_signUp );



        ImageView backArrow = findViewById( R.id.btnBackDaftar );
        ImageView help = findViewById( R.id.btnHelpDaftar );
        help.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText( SignUpActivity.this, "Lengkapi semua datamu", Toast.LENGTH_SHORT ).show();
            }
        } );
        backArrow.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        } );

        daftar = findViewById( R.id.btn_daftar );
        daftar.setTypeface( style );
        daftar.setBackgroundResource( R.color.light_gray );
        daftar.setEnabled( false );

        phone.addTextChangedListener( new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(nama.getText().toString().length() > 0 && email.getText().toString().length() > 0
                        && s.length() > 0){
                    daftar.setBackgroundResource( R.color.colorAccent );
                    daftar.setEnabled( true );
                }else{
                    daftar.setBackgroundResource( R.color.light_gray );
                    daftar.setEnabled( false );
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        } );

        daftar.setOnClickListener( this );

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_daftar:
                if (connectivityManager.getActiveNetworkInfo() != null
                        && connectivityManager.getActiveNetworkInfo().isConnected()
                        && connectivityManager.getActiveNetworkInfo().isAvailable()) {
                    showSignUp();
                } else {
                    Toast.makeText( this, "Akses internet tidak aktif", Toast.LENGTH_LONG ).show();
                }

                break;
        }
    }

    private void showSignUp() {
        name = nama.getText().toString();
        phonee = phone.getText().toString();
        emailku = email.getText().toString();

        if (TextUtils.isEmpty( nama.getText().toString() )) {
            Snackbar.make( root, "Isikan nama lengkapmu!", Snackbar.LENGTH_SHORT ).show();
        } else if (TextUtils.isEmpty( phone.getText().toString() )) {
            Snackbar.make( root, "Isikan nomor yang valid!", Snackbar.LENGTH_SHORT ).show();
        } else if (TextUtils.isEmpty( email.getText().toString() )) {
            Snackbar.make( root, "Isikan email yang valid!", Snackbar.LENGTH_SHORT ).show();
        } else {
            tambahUser( name, phonee, emailku );
        }
    }

    private void tambahUser(final String namee, final String phoneee, final String emailkuu) {
        final android.app.AlertDialog alertDialog = new SpotsDialog( SignUpActivity.this );
        alertDialog.show();

        StringRequest stringRequest = new StringRequest( Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        alertDialog.dismiss();
                        try{
                            JSONObject jsonObject = new JSONObject( response );
                            jsonObject.getBoolean( "status" );
                            if(status == true){
                                new AlertDialog.Builder( SignUpActivity.this )
                                        .setTitle( "Alert" )
                                        .setMessage( jsonObject.getString( "message" ) )
                                        .setPositiveButton( "OK", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                startActivity( new Intent( SignUpActivity.this,LoginActivity.class ) );
                                            }
                                        } ).show();
                                nama.setText( "" );
                                phone.setText( "" );
                                email.setText( "" );
                            }else{
                                new AlertDialog.Builder( SignUpActivity.this )
                                        .setTitle( "Alert" )
                                        .setMessage( jsonObject.getString( "message" ) )
                                        .setPositiveButton( "OK", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                            }
                                        } ).show();
                                nama.setText( "" );
                                phone.setText( "" );
                                email.setText( "" );
                            }

                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(),
                                error.getMessage(), Toast.LENGTH_LONG).show();
                        alertDialog.dismiss();
                    }
                } ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("nama", namee);
                params.put("phone", "0" + phoneee);
                params.put("email", emailkuu);
                return params;
            }
        };
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(stringRequest, tag_json_obj);

    }
}
