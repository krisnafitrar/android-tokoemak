package com.example.tokoemak.tokoemak.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tokoemak.tokoemak.R;
import com.example.tokoemak.tokoemak.commons.SharedPreferences;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class LandingPage extends AppCompatActivity {
    private TextView tokoemakLanding,titleLanding,btnKetentuan,btnKebijakan,textLang;
    private Typeface styleFont;
    private Button btnMasuk,btnDaftar;
    private SharedPreferences sharedPreferences;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext( CalligraphyContextWrapper.wrap( newBase ) );
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        CalligraphyConfig.initDefault( new CalligraphyConfig.Builder().setDefaultFontPath( "fonts/Poppins-Regular.ttf" )
                .setFontAttrId( R.attr.fontPath )
                .build());
        setContentView( R.layout.activity_landing_page );

        styleFont = Typeface.createFromAsset( getAssets(),"fonts/Poppins-Medium.ttf" );
        tokoemakLanding = findViewById( R.id.tokoemakLanding );
        titleLanding = findViewById( R.id.titleLanding );
        btnMasuk = findViewById( R.id.btnMasuk );
        btnDaftar = findViewById( R.id.btnDaftar );
        btnKetentuan = findViewById( R.id.btnKetentuan );
        btnKebijakan = findViewById( R.id.btnKebijakan );
        textLang = findViewById( R.id.textLang );

        textLang.setTypeface( styleFont );
        tokoemakLanding.setTypeface( styleFont );
        titleLanding.setTypeface( styleFont );
        btnDaftar.setTypeface( styleFont );
        btnMasuk.setTypeface( styleFont );

        sharedPreferences = new SharedPreferences( this );

        btnMasuk.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity( new Intent( LandingPage.this,LoginActivity.class ) );
            }
        } );

        btnDaftar.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity( new Intent( LandingPage.this,SignUpActivity.class ) );
            }
        } );

        btnKebijakan.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText( LandingPage.this, "Ini tombol kebijakan", Toast.LENGTH_SHORT ).show();
            }
        } );

        btnKetentuan.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText( LandingPage.this, "Ino tombol ketentuan", Toast.LENGTH_SHORT ).show();
            }
        } );

        if(sharedPreferences.isLoggedIn()){
            sharedPreferences.checkLogin();
            finish();
        }

    }
}
