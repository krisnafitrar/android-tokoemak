package com.example.tokoemak.tokoemak.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.tokoemak.tokoemak.R;
import com.example.tokoemak.tokoemak.model.BannerModel;
import com.example.tokoemak.tokoemak.model.KontenModel;

import java.util.ArrayList;
import java.util.List;

public class BannerAdapter extends RecyclerView.Adapter<BannerAdapter.ViewProcessHolder> {
    private Context mContext;
    private ArrayList<KontenModel> modelList;

    public BannerAdapter(Context mContext, ArrayList<KontenModel> modelList) {
        this.mContext = mContext;
        this.modelList = modelList;
    }

    @NonNull
    @Override
    public BannerAdapter.ViewProcessHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from( mContext ).inflate( R.layout.banner_iklan, viewGroup, false );
        ViewProcessHolder viewProcessHolder = new ViewProcessHolder( view );
        return viewProcessHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull BannerAdapter.ViewProcessHolder viewProcessHolder, int i) {
        KontenModel bannerModel = modelList.get( i );
        ImageView imgBanner = viewProcessHolder.gambar;

        Glide.with( mContext )
                .load( bannerModel.getGambar() )
                .into( imgBanner );
    }

    @Override
    public int getItemCount() {
        return modelList.size();
    }

    public class ViewProcessHolder extends RecyclerView.ViewHolder {
        ImageView gambar;
        public ViewProcessHolder(@NonNull View itemView) {
            super( itemView );
            gambar = itemView.findViewById( R.id.imgBanner );
        }
    }
}
