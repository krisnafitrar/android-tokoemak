package com.example.tokoemak.tokoemak.fragments;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tokoemak.tokoemak.R;
import com.example.tokoemak.tokoemak.activities.ConnectedDevice;
import com.example.tokoemak.tokoemak.activities.LandingPage;
import com.example.tokoemak.tokoemak.activities.LoginActivity;
import com.example.tokoemak.tokoemak.activities.MainActivity;
import com.example.tokoemak.tokoemak.activities.ScanQRActivity;
import com.example.tokoemak.tokoemak.activities.UpdateUser;
import com.example.tokoemak.tokoemak.commons.SharedPreferences;
import com.example.tokoemak.tokoemak.databases.Database;
import com.example.tokoemak.tokoemak.model.DiskonModel;
import com.example.tokoemak.tokoemak.model.UserDiskonModel;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;

import static android.app.Activity.RESULT_OK;


/**
 * A simple {@link Fragment} subclass.
 */
public class AccountFragment extends Fragment {
    Button logout;
    SharedPreferences sharedPreferences;
    LinearLayout linearDevice;
    private HashMap<String,String> user = new HashMap<String, String>();
    private TextView namaUser, nomorUser, emailUser, ubahProfil;
    private FirebaseDatabase db = FirebaseDatabase.getInstance();
    private DatabaseReference reference = db.getReference("lokasi");



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate( R.layout.fragment_account, container, false );

        //init
        namaUser = rootView.findViewById( R.id.namaUserAkun );
        nomorUser = rootView.findViewById( R.id.nomorUserAkun );
        emailUser = rootView.findViewById( R.id.emailUserAkun );
        ubahProfil = rootView.findViewById( R.id.ubahProfil );
        Typeface typeface = Typeface.createFromAsset( getActivity().getAssets(), "fonts/Poppins-Medium.ttf" );
        linearDevice = rootView.findViewById( R.id.linearDevice );
        logout = rootView.findViewById( R.id.btn_logout );
        sharedPreferences = new SharedPreferences( getActivity() );
        user = sharedPreferences.getUserDetails();
        db = FirebaseDatabase.getInstance();


        TextView toolbar = rootView.findViewById( R.id.textToolbarAkun );
        toolbar.setTypeface( Typeface.createFromAsset( getActivity().getAssets(),"fonts/Poppins-Medium.ttf" ) );


        namaUser.setTypeface( typeface );
        setUserData(user.get( sharedPreferences.KEY_NAME ),user.get( sharedPreferences.KEY_EMAIL ),user.get( sharedPreferences.KEY_NUMBER ));


        linearDevice.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity( new Intent( getActivity(), ConnectedDevice.class ) );
            }
        } );

        ubahProfil.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( getActivity(),UpdateUser.class );
                startActivity( intent );
            }
        } );

        logout.setTypeface( Typeface.createFromAsset( getActivity().getAssets(),"fonts/Poppins-Medium.ttf" ) );

        logout.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth.getInstance().signOut();
                Database database = new Database( getContext() );
                database.cleanCart();
                new Database( getContext() ).cleanLocation();
                sharedPreferences.logoutUser();
                sharedPreferences.setView( "no" );
                sharedPreferences.setTotalAwal( 0 );
                sharedPreferences.setOngkir( 0 );
                sharedPreferences.setDiskon( 0 );
                sharedPreferences.setSementara( 0 );
                sharedPreferences.setTotal( 0 );
                sharedPreferences.setLatitude( "" );
                sharedPreferences.setLongitude( "" );
                reference.child( user.get( sharedPreferences.KEY_NUMBER ) ).removeValue();
                Intent intent = new Intent( getContext(), LandingPage.class );
                intent.setFlags( Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK );
                startActivity( intent );
            }
        } );

        return rootView;
    }


    public void setUserData(String name,String emailnya,String nomornya) {
        namaUser.setText( name );
        emailUser.setText( emailnya );
        nomorUser.setText( "+62 " + nomornya.substring( 1 ) );
    }

    @Override
    public void onResume() {
        super.onResume();
        initDataUser();
    }

    @Override
    public void onStart() {
        super.onStart();
        initDataUser();
    }

    private void initDataUser() {
        sharedPreferences = new SharedPreferences( getActivity() );
        user = sharedPreferences.getUserDetails();
        setUserData(user.get( sharedPreferences.KEY_NAME ),user.get( sharedPreferences.KEY_EMAIL ),user.get( sharedPreferences.KEY_NUMBER ));
    }
}
