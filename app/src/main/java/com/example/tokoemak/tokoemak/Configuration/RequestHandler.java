package com.example.tokoemak.tokoemak.Configuration;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

public class RequestHandler {

    public String sendPostRequest(String requestURL, HashMap<String,String> postDataParams){
        //membuat url dari php
        URL url;

        StringBuilder stringBuilder = new StringBuilder(  );

        try{
            //assignment url
            url = new URL( requestURL );

            //membuat koneksi HttpURLConnection
            HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();

            //konfigurasi koneksi
            httpURLConnection.setReadTimeout( 15000 );
            httpURLConnection.setConnectTimeout( 15000 );
            httpURLConnection.setRequestMethod( "POST" );
            httpURLConnection.setDoInput( true );
            httpURLConnection.setDoOutput( true );

            //membuat keluaran stream
            OutputStream os = httpURLConnection.getOutputStream();

            //menulis parameter untuk permintaan
            //menggunakan metode getPostDataString yang didefinisikan di bawah ini
            BufferedWriter bufferedWriter = new BufferedWriter( new OutputStreamWriter( os, "UTF-8" ) );
            bufferedWriter.write(getPostDataString( postDataParams ));
            bufferedWriter.flush();
            bufferedWriter.close();
            os.close();

            int responseCode = httpURLConnection.getResponseCode();

            if(responseCode == HttpURLConnection.HTTP_OK){
                BufferedReader bufferedReader = new BufferedReader( new InputStreamReader( httpURLConnection.getInputStream() ) );
                stringBuilder = new StringBuilder();
                String response;
                //reading server response
                while((response = bufferedReader.readLine()) != null){
                    stringBuilder.append(response);
                }

            }

        }catch (Exception e){
            e.printStackTrace();
        }

        return stringBuilder.toString();
    };

    private String getPostDataString(HashMap<String, String> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for (Map.Entry<String, String> entry : params.entrySet()) {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }
        return result.toString();
    }
}
