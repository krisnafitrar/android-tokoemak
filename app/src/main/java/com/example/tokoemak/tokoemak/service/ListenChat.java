package com.example.tokoemak.tokoemak.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;

import com.example.tokoemak.tokoemak.R;
import com.example.tokoemak.tokoemak.activities.ChatActivity;
import com.example.tokoemak.tokoemak.activities.MainActivity;
import com.example.tokoemak.tokoemak.model.ChatModel;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

import androidx.annotation.Nullable;

public class ListenChat extends Service implements ChildEventListener {
    private FirebaseDatabase db;
    private DatabaseReference ref;
    private com.example.tokoemak.tokoemak.commons.SharedPreferences sharedPreferences;
    private HashMap<String,String> user = new HashMap<String, String>(  );
    private String nomortlp;
    private String shortMsg;
    private MediaPlayer soundChat;

    public void ListenChat()
    {
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sharedPreferences = new com.example.tokoemak.tokoemak.commons.SharedPreferences( this );
        user = sharedPreferences.getUserDetails();
        nomortlp = user.get( sharedPreferences.KEY_NUMBER );
        db = FirebaseDatabase.getInstance();
        ref = db.getReference( "chat" );
        soundChat = MediaPlayer.create(getBaseContext(),R.raw.chat);
        setShortMessage();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        ref.addChildEventListener( this );
        return super.onStartCommand( intent, flags, startId );
    }

    @Override
    public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @android.support.annotation.Nullable String s) {

    }

    private void showNotification() {
        String channelId = "tokoemak";
        Intent intent = new Intent( getBaseContext(), ChatActivity.class );
        PendingIntent contentIntent = PendingIntent.getActivity(getBaseContext(),0,intent,PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder( getBaseContext(), channelId);
        Uri soundUri = RingtoneManager.getDefaultUri( RingtoneManager.TYPE_RINGTONE );
        builder.setAutoCancel( true )
                .setDefaults( Notification.DEFAULT_ALL )
                .setWhen( System.currentTimeMillis() )
                .setTicker("Pesan Baru")
                .setContentTitle( "Admin" )
                .setContentText( getShortMsg() )
                .setContentIntent( contentIntent )
                .setPriority( Notification.PRIORITY_MAX )
                .setSmallIcon( R.mipmap.ic_newest_logo )
                .setSound( soundUri );


        NotificationManager notificationManager = (NotificationManager) getBaseContext().getSystemService( Context.NOTIFICATION_SERVICE );
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            NotificationChannel channel = new NotificationChannel(
                    channelId,
                    "chat tokoemak",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
            builder.setChannelId(channelId);
        }


        notificationManager.notify(0, builder.build());
    }

    private void setShortMessage() {
        ref.child( nomortlp ).limitToLast( 1 ).addChildEventListener( new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @android.support.annotation.Nullable String s) {
                ChatModel chatModel = dataSnapshot.getValue( ChatModel.class );
                shortMsg = chatModel.getMessage();
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @android.support.annotation.Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @android.support.annotation.Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        } );

    }

    public String getShortMsg() {
        return shortMsg;
    }

    @Override
    public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @android.support.annotation.Nullable String s) {
        soundChat.start();
        ref.child( nomortlp ).limitToLast( 1 ).addChildEventListener( new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @android.support.annotation.Nullable String s) {
                ChatModel chatModel = dataSnapshot.getValue( ChatModel.class );
                if(chatModel.getUsername().equals( "Admin" )){
                    showNotification();
                }
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @android.support.annotation.Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @android.support.annotation.Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        } );
    }

    @Override
    public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

    }

    @Override
    public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @android.support.annotation.Nullable String s) {

    }

    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) {

    }
}
