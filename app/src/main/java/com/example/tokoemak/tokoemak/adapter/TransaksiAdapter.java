package com.example.tokoemak.tokoemak.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tokoemak.tokoemak.R;
import com.example.tokoemak.tokoemak.fragments.HistoryFragment;
import com.example.tokoemak.tokoemak.model.TransaksiModel;

import java.util.ArrayList;

public class TransaksiAdapter extends RecyclerView.Adapter<TransaksiAdapter.ViewProcessHolder> {
    private HistoryFragment mContext;
    private ArrayList<TransaksiModel> mList;

    public TransaksiAdapter(HistoryFragment mContext, ArrayList<TransaksiModel> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @NonNull
    @Override
    public TransaksiAdapter.ViewProcessHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from( mContext.getActivity() ).inflate( R.layout.layout_history,viewGroup,false  );
        return new ViewProcessHolder( view );
    }

    @Override
    public void onBindViewHolder(@NonNull TransaksiAdapter.ViewProcessHolder viewProcessHolder, int i) {
        TransaksiModel model = mList.get( i );
        ImageView imageView;
        TextView kode,status,time;
        RelativeLayout btnDetail;

        imageView = viewProcessHolder.imgHis;
        kode = viewProcessHolder.txtKode;
        status = viewProcessHolder.txtStatus;
        time = viewProcessHolder.txtTime;
        btnDetail = viewProcessHolder.btnHistory;

        imageView.setImageResource( R.drawable.ic_shopping_cart_white_24dp );
        if(model.getIdstatus().equals( "5" )){
            imageView.setBackgroundResource( R.drawable.back_circle_disable );
            kode.setTextColor( Color.parseColor("#C0C0C0") );
            status.setTextColor( Color.parseColor( "#C0C0C0" ) );
            time.setTextColor( Color.parseColor( "#C0C0C0" ) );
        }else{
            imageView.setBackgroundResource( R.drawable.back_language);
            kode.setTextColor( Color.parseColor( "#000000" ) );
            status.setTextColor( Color.parseColor( "#000000" ) );
            time.setTextColor( Color.parseColor( "#000000" ) );
        }

        kode.setText("TM-" +  model.getKodetransaksi() );
        status.setText( getStatus(model.getIdstatus()) );
        if(model.getIdstatus().equals( "5" )){
            time.setText( model.getDibatalkanpada() );
        }else{
            time.setText( model.getDiterimapada() );
        }

        kode.setTypeface( Typeface.createFromAsset( mContext.getActivity().getAssets(),"fonts/Poppins-Medium.ttf" ) );
        status.setTypeface( Typeface.createFromAsset( mContext.getActivity().getAssets(),"fonts/Poppins-Medium.ttf" ) );

        btnDetail.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mContext.loadBottomSheetsDetail();
            }
        } );

    }

    private String getStatus(String idstatus) {
        if(idstatus.equals( "5" )){
            return "Pesanan dibatalkan";
        }else{
            return "Pesanan diterima";
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewProcessHolder extends RecyclerView.ViewHolder {
        ImageView imgHis;
        TextView txtKode,txtStatus,txtTime;
        RelativeLayout btnHistory;
        public ViewProcessHolder(@NonNull View itemView) {
            super( itemView );
            imgHis = itemView.findViewById( R.id.imgHis );
            txtKode = itemView.findViewById( R.id.kodetransaksihistori );
            txtStatus = itemView.findViewById( R.id.statusPesananHistori );
            txtTime = itemView.findViewById( R.id.timeHistory );
            btnHistory = itemView.findViewById( R.id.btnHistory );
        }
    }
}
