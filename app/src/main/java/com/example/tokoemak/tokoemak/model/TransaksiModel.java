package com.example.tokoemak.tokoemak.model;

public class TransaksiModel {
    String idtransaksi,kodetransaksi,idusers,waktu,jadwalpengiriman,alamat,totalestimasi,ongkir,diskon,total,idstatus,latitude,longitude,diterimapada,dibatalkanpada;

    public TransaksiModel() {
    }

    public TransaksiModel(String idtransaksi, String kodetransaksi, String idusers, String waktu, String jadwalpengiriman, String alamat, String totalestimasi, String ongkir, String diskon, String total, String idstatus, String latitude, String longitude, String diterimapada, String dibatalkanpada) {
        this.idtransaksi = idtransaksi;
        this.kodetransaksi = kodetransaksi;
        this.idusers = idusers;
        this.waktu = waktu;
        this.jadwalpengiriman = jadwalpengiriman;
        this.alamat = alamat;
        this.totalestimasi = totalestimasi;
        this.ongkir = ongkir;
        this.diskon = diskon;
        this.total = total;
        this.idstatus = idstatus;
        this.latitude = latitude;
        this.longitude = longitude;
        this.diterimapada = diterimapada;
        this.dibatalkanpada = dibatalkanpada;
    }

    public String getIdtransaksi() {
        return idtransaksi;
    }

    public void setIdtransaksi(String idtransaksi) {
        this.idtransaksi = idtransaksi;
    }

    public String getKodetransaksi() {
        return kodetransaksi;
    }

    public void setKodetransaksi(String kodetransaksi) {
        this.kodetransaksi = kodetransaksi;
    }

    public String getIdusers() {
        return idusers;
    }

    public void setIdusers(String idusers) {
        this.idusers = idusers;
    }

    public String getWaktu() {
        return waktu;
    }

    public void setWaktu(String waktu) {
        this.waktu = waktu;
    }

    public String getJadwalpengiriman() {
        return jadwalpengiriman;
    }

    public void setJadwalpengiriman(String jadwalpengiriman) {
        this.jadwalpengiriman = jadwalpengiriman;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getTotalestimasi() {
        return totalestimasi;
    }

    public void setTotalestimasi(String totalestimasi) {
        this.totalestimasi = totalestimasi;
    }

    public String getOngkir() {
        return ongkir;
    }

    public void setOngkir(String ongkir) {
        this.ongkir = ongkir;
    }

    public String getDiskon() {
        return diskon;
    }

    public void setDiskon(String diskon) {
        this.diskon = diskon;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getIdstatus() {
        return idstatus;
    }

    public void setIdstatus(String idstatus) {
        this.idstatus = idstatus;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getDiterimapada() {
        return diterimapada;
    }

    public void setDiterimapada(String diterimapada) {
        this.diterimapada = diterimapada;
    }

    public String getDibatalkanpada() {
        return dibatalkanpada;
    }

    public void setDibatalkanpada(String dibatalkanpada) {
        this.dibatalkanpada = dibatalkanpada;
    }
}
