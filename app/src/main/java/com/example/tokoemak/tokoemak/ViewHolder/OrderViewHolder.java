package com.example.tokoemak.tokoemak.ViewHolder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.tokoemak.tokoemak.Interface.ItemClickListener;
import com.example.tokoemak.tokoemak.R;
import com.shuhart.stepview.StepView;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class OrderViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    public TextView kode,status,nomor,alamat,cancel,detail;
    public CircleImageView imgStatus;
    private ItemClickListener itemClickListener;
    public StepView stepView;

    public OrderViewHolder(@NonNull View itemView) {
        super( itemView );
        kode = itemView.findViewById( R.id.kodeOrders );
        status = itemView.findViewById( R.id.statusOrders );
        alamat = itemView.findViewById( R.id.alamatOrders );
        cancel = itemView.findViewById( R.id.cancelOrder );
        detail = itemView.findViewById( R.id.detailOrder );
        stepView = itemView.findViewById( R.id.step_view );
        imgStatus = itemView.findViewById( R.id.imgStatusOrder );
        itemView.setOnClickListener( this );
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public void onClick(View v) {
        itemClickListener.onClick( v, getAdapterPosition(), false );
    }


}
