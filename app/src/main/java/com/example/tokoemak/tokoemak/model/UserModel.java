package com.example.tokoemak.tokoemak.model;

public class UserModel {
    private String nama,email;

    public UserModel() {
    }

    public UserModel(String nama, String email) {
        this.nama = nama;
        this.email = email;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
