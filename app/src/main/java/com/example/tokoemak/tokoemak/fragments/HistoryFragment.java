package com.example.tokoemak.tokoemak.fragments;


import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.tokoemak.tokoemak.R;
import com.example.tokoemak.tokoemak.activities.ScanQRActivity;
import com.example.tokoemak.tokoemak.activities.MainActivity;
import com.example.tokoemak.tokoemak.adapter.CartAdapter;
import com.example.tokoemak.tokoemak.adapter.TransaksiAdapter;
import com.example.tokoemak.tokoemak.app.AppController;
import com.example.tokoemak.tokoemak.commons.SharedDiskon;
import com.example.tokoemak.tokoemak.commons.SharedPreferences;
import com.example.tokoemak.tokoemak.databases.Database;
import com.example.tokoemak.tokoemak.model.DiskonModel;
import com.example.tokoemak.tokoemak.model.OrderModel;
import com.example.tokoemak.tokoemak.model.RequestModel;
import com.example.tokoemak.tokoemak.model.TransaksiModel;
import com.example.tokoemak.tokoemak.model.UserDiskonModel;
import com.example.tokoemak.tokoemak.util.Server;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import cn.pedant.SweetAlert.SweetAlertDialog;
import dmax.dialog.SpotsDialog;
import es.dmoral.toasty.Toasty;


/**
 * A simple {@link Fragment} subclass.
 *
 */



public class HistoryFragment extends Fragment {
    private TextView textJudul;
    private Typeface typeface;
    private TransaksiAdapter mAdapter;
    private ArrayList<TransaksiModel>mList;
    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private SharedPreferences sharedPreferences;
    private HashMap<String,String>user = new HashMap<>();
    private String iduser;
    private LinearLayout linearNull;
    private SwipeRefreshLayout swipeRefreshLayout;

    public HistoryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View rootView = inflater.inflate( R.layout.layout_history_fragment, container, false );
        textJudul = rootView.findViewById( R.id.textHistory );
        typeface = Typeface.createFromAsset( getActivity().getAssets(), "fonts/Poppins-Medium.ttf" );
        textJudul.setTypeface( typeface );

        TextView toolbar = rootView.findViewById( R.id.textToolbarHistory );
        toolbar.setTypeface( Typeface.createFromAsset( getActivity().getAssets(),"fonts/Poppins-Medium.ttf" ) );
        sharedPreferences = new SharedPreferences( getActivity() );
        user = sharedPreferences.getUserDetails();
        iduser = user.get( sharedPreferences.KEY_IDUSER );

        linearNull = rootView.findViewById( R.id.linearHistoryNull );
        recyclerView = rootView.findViewById( R.id.rv_history );
        swipeRefreshLayout = rootView.findViewById( R.id.swipeRefreshHistory );

        initHistory(iduser);

        swipeRefreshLayout.setColorScheme(R.color.steel_blue,R.color.orange,R.color.colorAccent);
        swipeRefreshLayout.setOnRefreshListener( new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(!isNetworkAvailable()){
                    swipeRefreshLayout.setRefreshing( false );
                }else{
                    initHistory( iduser );
                    swipeRefreshLayout.setRefreshing( false );
                }
            }
        } );

        return rootView;

    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    private void initHistory(String iduser) {
        mList = new ArrayList<>();
        getHistory(iduser);
        recyclerView.setLayoutManager( new LinearLayoutManager( getContext() ) );
        recyclerView.setHasFixedSize( true );
        mAdapter = new TransaksiAdapter( HistoryFragment.this,mList );
        recyclerView.setNestedScrollingEnabled( false );
        recyclerView.addItemDecoration( new DividerItemDecoration( recyclerView.getContext(),DividerItemDecoration.VERTICAL ) );
        recyclerView.setAdapter( mAdapter );
    }


    private void getHistory(final String iduser) {
        String urlWithParams = Server.REST_API_TRANSAKSI + "?iduser=" + iduser;
        StringRequest stringRequest = new StringRequest( Request.Method.GET, urlWithParams, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                linearNull.setVisibility( View.VISIBLE );
                try {
                    JSONArray jsonArray = new JSONArray( response );
                    for(int i=0; i < jsonArray.length(); i++){
                        try{
                            JSONObject data = jsonArray.getJSONObject( i );
                            TransaksiModel model = new TransaksiModel();
                            model.setKodetransaksi( data.getString( "kodetransaksi" ) );
                            model.setIdusers( data.getString( "iduser" ) );
                            model.setWaktu( data.getString( "waktu" ) );
                            model.setJadwalpengiriman( data.getString( "jadwalpengiriman" ) );
                            model.setAlamat( data.getString( "alamat" ) );
                            model.setTotalestimasi( data.getString( "totalestimasi" ) );
                            model.setOngkir( data.getString( "ongkir" ) );
                            model.setDiskon( data.getString( "diskon" ) );
                            model.setTotal( data.getString( "total" ) );
                            model.setIdstatus( data.getString( "idstatus" ) );
                            model.setLatitude( data.getString( "latitude" ) );
                            model.setLongitude( data.getString( "longitude" ) );
                            model.setDiterimapada( data.getString( "diterimapada" ) );
                            model.setDibatalkanpada( data.getString( "dibatalkanpada" ) );
                            mList.add( model );
                            linearNull.setVisibility( View.GONE );
                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                    mAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        } );

        AppController.getInstance().addToRequestQueue( stringRequest,"json_obj_req" );
    }


    public void loadBottomSheetsDetail()
    {
        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog( getActivity() );
        bottomSheetDialog.setContentView( R.layout.bottom_sheets_riwayat );
        bottomSheetDialog.setCanceledOnTouchOutside( true );

        bottomSheetDialog.show();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach( context );
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult( requestCode, resultCode, data );

    }

    @Override
    public void onStop() {
        super.onStop();
    }



}
