package com.example.tokoemak.tokoemak.ViewHolder;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.tokoemak.tokoemak.Interface.ItemClickListener;
import com.example.tokoemak.tokoemak.R;

public class ChatViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    private ItemClickListener itemClickListener;
    public TextView username,message,time,usernameRight,messageRight,timeRight;
    public LinearLayout cardChat,cardChatRight;

    public ChatViewHolder(@NonNull View itemView) {
        super( itemView );
        username = itemView.findViewById( R.id.usernameChat );
        message = itemView.findViewById( R.id.textChat );
//        time = itemView.findViewById( R.id.timeChat );
        usernameRight = itemView.findViewById( R.id.usernameChatRight );
        messageRight = itemView.findViewById( R.id.textChatRight );
//        timeRight = itemView.findViewById( R.id.timeChatRight );
        cardChat = itemView.findViewById( R.id.cardChat );
        cardChatRight = itemView.findViewById( R.id.cardChatRight );

        itemView.setOnClickListener( this );
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }


    @Override
    public void onClick(View v) {
        itemClickListener.onClick( v , getAdapterPosition(), true );
    }
}
