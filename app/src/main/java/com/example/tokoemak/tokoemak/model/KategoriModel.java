package com.example.tokoemak.tokoemak.model;

public class KategoriModel {
    String nama;
    String image;

    public KategoriModel(String nama, String image) {
        this.nama = nama;
        this.image = image;
    }

    public KategoriModel() {
    }

    public String getNama() {

        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
