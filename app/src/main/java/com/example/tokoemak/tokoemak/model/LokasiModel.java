package com.example.tokoemak.tokoemak.model;

public class LokasiModel {
    private String iduser;
    private String latitude;
    private String longitude;
    private int isUsed;


    public LokasiModel() {
    }

    public LokasiModel(String iduser, String latitude, String longitude,int IsUsed) {
        this.iduser = iduser;
        this.latitude = latitude;
        this.longitude = longitude;
        this.isUsed = IsUsed;
    }

    public int getIsUsed() {
        return isUsed;
    }

    public void setIsUsed(int isUsed) {
        this.isUsed = isUsed;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getIduser() {
        return iduser;
    }

    public void setIduser(String iduser) {
        this.iduser = iduser;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
}
