package com.example.tokoemak.tokoemak.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.tokoemak.tokoemak.R;
import com.example.tokoemak.tokoemak.app.AppController;
import com.example.tokoemak.tokoemak.commons.SharedPreferences;
import com.example.tokoemak.tokoemak.fragments.AccountFragment;
import com.example.tokoemak.tokoemak.util.Server;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONObject;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class UpdateUser extends AppCompatActivity{
    private MaterialEditText edtName,edtNumber,edtEmail;
    private SharedPreferences sharedPreferences;
    private HashMap<String,String> user = new HashMap<String, String>(  );
    private Button btnUbah;
    private String url = Server.REST_API_USERS;
    private String tag_json_obj = "json_obj_req";
    private boolean status;
    private ImageView imageView;
    private Typeface styleFont;
    private TextView titleUbahProfil,titleUpdateNama,titleUpdateTlp,titleUpdateEmail;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext( CalligraphyContextWrapper.wrap( newBase ) );
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        CalligraphyConfig.initDefault( new CalligraphyConfig.Builder().setDefaultFontPath( "fonts/Poppins-Regular.ttf" )
                .setFontAttrId( R.attr.fontPath )
                .build());
        setContentView( R.layout.activity_update_user );

        //inisialisasi view
        sharedPreferences = new SharedPreferences( this );
        user = sharedPreferences.getUserDetails();
        edtName = findViewById( R.id.textBoxNamaProfil );
        edtEmail = findViewById( R.id.textBoxEmailProfil );
        edtNumber = findViewById( R.id.textBoxPhoneProfil );
        btnUbah = findViewById( R.id.btnUbah );
        imageView = findViewById( R.id.backArrowProfile );
        titleUbahProfil = findViewById( R.id.titleUbahProfil );
        titleUpdateNama = findViewById( R.id.titleUpdateNama );
        titleUpdateTlp = findViewById( R.id.titleUpdateTlp );
        titleUpdateEmail = findViewById( R.id.titleUpdateEmail );
        styleFont = Typeface.createFromAsset( getAssets(),"fonts/Poppins-Medium.ttf" );

        //set Typeface
        titleUbahProfil.setTypeface( styleFont );
        titleUpdateNama.setTypeface( styleFont );
        titleUpdateTlp.setTypeface( styleFont );
        titleUpdateEmail.setTypeface( styleFont );


        //memberi event click pada imageView BackArrow
        imageView.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        } );

        //set inputan
        edtName.setText( user.get( sharedPreferences.KEY_NAME ) );
        edtNumber.setText( user.get( sharedPreferences.KEY_NUMBER ).substring( 1 ) );
        edtEmail.setText( user.get( sharedPreferences.KEY_EMAIL ) );
        edtNumber.setEnabled( false );

        //update profile
        btnUbah.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateProfil(user.get( sharedPreferences.KEY_IDUSER ), edtName.getText().toString(),edtEmail.getText().toString(),edtNumber.getText().toString() );
            }
        } );
    }

    private void updateProfil(final String s, final String nama, final String email, final String key)
    {
        StringRequest stringRequest = new StringRequest( Request.Method.PUT, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    JSONObject jsonObject = new JSONObject( response );
                    status = jsonObject.getBoolean( "status" );
                    //check tag successnya, jika 1 maka lolos
                    if(status == true){
                        new AlertDialog.Builder( UpdateUser.this )
                                .setTitle( "Berhasil" )
                                .setMessage( jsonObject.getString( "message" ) )
                                .setPositiveButton( "OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        onBackPressed();
                                    }
                                } ).show();
                        sharedPreferences.createLoginSession( s, nama, "0" + key,email,user.get( sharedPreferences.KEY_TANGGAL ));
                    }else{
                        new AlertDialog.Builder( UpdateUser.this )
                                .setTitle( "Gagal" )
                                .setMessage( jsonObject.getString( "message" ) )
                                .setPositiveButton( "OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                } ).show();
                    }

                }catch (Exception e){
                    Toast.makeText(getApplicationContext(),
                            e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(UpdateUser.this, error.getMessage(),Toast.LENGTH_SHORT ).show();
            }
        } ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put( "nama", nama );
                params.put("email", email);
                params.put( "phone", "0" + key );
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue( stringRequest,tag_json_obj );
    }
}
