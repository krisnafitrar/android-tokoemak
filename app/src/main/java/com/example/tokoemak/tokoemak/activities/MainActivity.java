package com.example.tokoemak.tokoemak.activities;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import com.example.tokoemak.tokoemak.R;
import com.example.tokoemak.tokoemak.commons.SharedPreferences;
import com.example.tokoemak.tokoemak.fragments.AccountFragment;
import com.example.tokoemak.tokoemak.fragments.HistoryFragment;
import com.example.tokoemak.tokoemak.fragments.HomeFragment;
import com.example.tokoemak.tokoemak.fragments.InboxFragment;
import com.example.tokoemak.tokoemak.fragments.OrdersFragment;
import com.example.tokoemak.tokoemak.service.ListenChat;
import com.example.tokoemak.tokoemak.service.ListenOrder;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity {
    private BottomNavigationView bottomNavigationView;
    private HomeFragment homeFragment;
    private HistoryFragment historyFragment;
    private OrdersFragment ordersFragment;
    private AccountFragment accountFragment;
    private InboxFragment inboxFragment;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext( CalligraphyContextWrapper.wrap( newBase ) );
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        CalligraphyConfig.initDefault( new CalligraphyConfig.Builder().setDefaultFontPath( "fonts/roboto.ttf" )
                .setFontAttrId( R.attr.fontPath )
                .build());
        setContentView( R.layout.activity_main );

        homeFragment = new HomeFragment();
        historyFragment = new HistoryFragment();
        ordersFragment = new OrdersFragment();
        accountFragment = new AccountFragment();
        inboxFragment = new InboxFragment();

        bottomNavigationView = findViewById( R.id.bottomNavbar );
        bottomNavigationView.setOnNavigationItemSelectedListener( new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                int id = menuItem.getItemId();

                if(id == R.id.navHome){
                    setFragment(homeFragment);
                    return true;
                }else if(id == R.id.navHistory){
                    setFragment( historyFragment );
                    return true;
                }else if(id == R.id.navOrders){
                    setFragment( ordersFragment );
                    return true;
                }else if(id == R.id.navInbox){
                    setFragment( inboxFragment );
                    return true;
                }else if(id == R.id.navAccount){
                    setFragment( accountFragment );
                    return true;
                }

                return false;
            }
        } );

        bottomNavigationView.setSelectedItemId( R.id.navHome );

        //register service
        Intent service = new Intent( MainActivity.this, ListenOrder.class );
        startService( service );

        Intent serviceChat = new Intent( MainActivity.this, ListenChat.class );
        startService( serviceChat );

    }


    public void setFragment(Fragment fragment){
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.enter_right_to_left,R.anim.exit_right_to_left,R.anim.enter_left_to_right,R.anim.exit_left_to_right );
        fragmentTransaction.replace( R.id.frame, fragment );
        fragmentTransaction.commit();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
