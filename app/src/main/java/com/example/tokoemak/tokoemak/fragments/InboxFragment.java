package com.example.tokoemak.tokoemak.fragments;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.tokoemak.tokoemak.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class InboxFragment extends Fragment {
    private TextView titleToolbar,titleInbox;
    private Typeface mBold;
    private SwipeRefreshLayout refreshLayout;

    public InboxFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate( R.layout.fragment_inbox, container, false );
        titleToolbar = rootView.findViewById( R.id.textToolbarInbox );
        titleInbox = rootView.findViewById( R.id.judulInbox );
        mBold = Typeface.createFromAsset( getActivity().getAssets(), "fonts/Poppins-Medium.ttf" );

        titleToolbar.setTypeface( mBold );
        titleInbox.setTypeface( mBold );


        return rootView;
    }

}
