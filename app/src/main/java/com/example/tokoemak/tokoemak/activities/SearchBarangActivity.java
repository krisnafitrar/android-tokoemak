package com.example.tokoemak.tokoemak.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.tokoemak.tokoemak.R;
import com.example.tokoemak.tokoemak.adapter.ProdukAdapter;
import com.example.tokoemak.tokoemak.app.AppController;
import com.example.tokoemak.tokoemak.model.ProdukModel;
import com.example.tokoemak.tokoemak.util.Server;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import io.supercharge.shimmerlayout.ShimmerLayout;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SearchBarangActivity extends AppCompatActivity {
    private ImageView btnGoHome,btnSearch,btnShowFav;
    private TextView titleToolbar,produkFound;
    private Typeface typeface;
    private EditText edtSearch;
    private String keyword;
    private RecyclerView rvProduk;
    private ArrayList<ProdukModel> modelList;
    public ProdukAdapter mAdapter;
    private ShimmerLayout shimmerLayout,shimmerLokasi;
    private String[] taglineSearch;
    private ArrayList arrayList = new ArrayList();
    private int count = 0;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext( CalligraphyContextWrapper.wrap( newBase ) );
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        CalligraphyConfig.initDefault( new CalligraphyConfig.Builder().setDefaultFontPath( "fonts/Poppins-Regular.ttf" )
                .setFontAttrId( R.attr.fontPath )
                .build());
        setContentView( R.layout.activity_search_barang );

        //init view
        btnShowFav = findViewById( R.id.btnShowFavSearch );
        shimmerLokasi = findViewById( R.id.shimmerLokasiSearch );
        shimmerLayout = findViewById( R.id.shimmerLayoutSearch );
        btnGoHome = findViewById( R.id.btnBackToHome );
        btnSearch = findViewById( R.id.btnSearchBarang );
        edtSearch = findViewById( R.id.keywordSearch );
        rvProduk = findViewById( R.id.rv_search_produk );
        produkFound = findViewById( R.id.textProdukFound );
        typeface = Typeface.createFromAsset( getAssets(), "fonts/Poppins-Medium.ttf" );
        produkFound.setTypeface( typeface );
        modelList = new ArrayList<>();

        arrayList.add( "Mau cari apa?" );
        arrayList.add( "Keknya kamu laper ya?" );
        arrayList.add( "Jam segini butuh ngemil?" );

        count = arrayList.size();

        setTagSearch(count);

//        setTaglineSearch(arrayList.size());

        btnSearch.setEnabled( false );
        shimmerLokasi.startShimmerAnimation();

        btnShowFav.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity( new Intent( SearchBarangActivity.this,FavoriteActivities.class ) );
            }
        } );

        btnSearch.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                modelList.clear();
                keyword = edtSearch.getText().toString().toLowerCase();
                shimmerLayout.setVisibility( View.VISIBLE );
                shimmerLayout.startShimmerAnimation();
                getProdukByKeyword(keyword);
                shimmerLayout.stopShimmerAnimation();
                shimmerLayout.setVisibility( View.GONE );
                rvProduk.setLayoutManager( new GridLayoutManager( SearchBarangActivity.this,2 ) );
                rvProduk.setHasFixedSize( true );
                mAdapter = new ProdukAdapter( SearchBarangActivity.this,modelList );
                rvProduk.setAdapter( mAdapter );
                rvProduk.setNestedScrollingEnabled( false );
            }
        } );

        edtSearch.addTextChangedListener( new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() > 2){
                    btnSearch.setEnabled( true );
                    btnSearch.setImageResource( R.drawable.ic_search_success_24dp );
                }else{
                    btnSearch.setEnabled( false );
                    btnSearch.setImageResource( R.drawable.ic_search_grey_24dp );
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        } );

        //set action / behavior
        btnGoHome.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        } );

    }

    private void setTagSearch(int count) {

    }

    private void getProdukByKeyword(final String keyword) {
        StringRequest stringRequest = new StringRequest( Request.Method.POST, Server.URL_SEARCH_PRODUK, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    System.out.println("response : " + response);
                    produkFound.setVisibility( View.VISIBLE );
                    produkFound.setText( "Produk tidak ditemukan" );
                    JSONArray jsonArray = new JSONArray( response );
                    for(int i = 0; i < jsonArray.length(); i++){
                        try{
                            JSONObject data = jsonArray.getJSONObject( i );
                            ProdukModel model = new ProdukModel();
                            model.setKode_barang( data.getString( "idbarang" ) );
                            model.setNama( data.getString( "namabarang" ) );
                            model.setJenis( getJenis(data.getString("idjenis")) );
                            model.setHarga( data.getString( "harga" ) );
                            model.setStok( data.getString( "stok" ) );
                            model.setGambar(data.getString("gambar"));
                            model.setDeskripsi( data.getString( "deskripsi" ) );
                            model.setIddiskon( data.getString( "iddiskon" ) );
                            model.setDiskon( data.getString( "diskon" ) );
                            model.setPotongan( data.getString( "potongan" ) );
                            model.setHargadiskon( data.getString( "hargadiskon" ) );
                            modelList.add(model);
                            if(jsonArray.length() > 0){
                                produkFound.setText( jsonArray.length() + " Produk Ditemukan" );
                            }
                            produkFound.setVisibility( View.VISIBLE );
//                            shimmerLayout.stopShimmerAnimation();
//                            shimmerLayout.setVisibility( View.GONE );
                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                    mAdapter.notifyDataSetChanged();
                }catch (JSONException exception){
                    exception.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d( "volley", "error : " + error.getMessage() );
            }
        } ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("keyword", keyword );
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(stringRequest,"json_obj_req");
    }

    private String getJenis(String idjenis) {
        return idjenis.equals( "2" )?"Beras":"";
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        shimmerLokasi.startShimmerAnimation();
    }

    @Override
    protected void onPause() {
        super.onPause();
        shimmerLokasi.stopShimmerAnimation();
    }

    @Override
    protected void onStop() {
        super.onStop();
        shimmerLokasi.stopShimmerAnimation();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        shimmerLokasi.startShimmerAnimation();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

}
