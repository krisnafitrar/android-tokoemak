package com.example.tokoemak.tokoemak.model;

public class OngkirModel {
    String perkilo,minimal;

    public OngkirModel() {
    }

    public OngkirModel(String perkilo, String minimal) {
        this.perkilo = perkilo;
        this.minimal = minimal;
    }

    public String getPerkilo() {
        return perkilo;
    }

    public void setPerkilo(String perkilo) {
        this.perkilo = perkilo;
    }

    public String getMinimal() {
        return minimal;
    }

    public void setMinimal(String minimal) {
        this.minimal = minimal;
    }
}
