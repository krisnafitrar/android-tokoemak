package com.example.tokoemak.tokoemak.model;

public class PenawaranModel {
    String gambar,nama,deskripsi,menuid;

    public PenawaranModel(String gambar, String nama, String deskripsi, String menuid) {
        this.gambar = gambar;
        this.nama = nama;
        this.deskripsi = deskripsi;
        this.menuid = menuid;
    }

    public PenawaranModel() {
    }

    public String getMenuid() {
        return menuid;
    }

    public void setMenuid(String menuid) {
        this.menuid = menuid;
    }

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

}
