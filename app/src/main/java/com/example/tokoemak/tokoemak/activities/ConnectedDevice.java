package com.example.tokoemak.tokoemak.activities;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toolbar;

import com.example.tokoemak.tokoemak.R;
import com.example.tokoemak.tokoemak.commons.SharedPreferences;

import java.lang.reflect.Field;
import java.util.HashMap;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ConnectedDevice extends AppCompatActivity {
    private TextView androidType,terhubungPada,versiAndroid,titlePerangkat,titlePastikan,titleTerhubung,titlePerangkatIni,titleKamuTerhubung;
    private SharedPreferences sharedPreferences;
    private HashMap<String,String> user = new HashMap<String,String>();
    private ImageView backArrow;
    private Typeface styleFont;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext( CalligraphyContextWrapper.wrap( newBase ) );
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        CalligraphyConfig.initDefault( new CalligraphyConfig.Builder().setDefaultFontPath( "fonts/Poppins-Medium.ttf" )
                .setFontAttrId( R.attr.fontPath )
                .build());
        setContentView( R.layout.activity_connected_device );

        //init
        sharedPreferences = new SharedPreferences( this );
        user = sharedPreferences.getUserDetails();
        androidType = findViewById( R.id.merkHP );
        terhubungPada = findViewById( R.id.terhubungPada );
        versiAndroid = findViewById( R.id.versiAndroid );
        backArrow = findViewById( R.id.backArrowDevice );
        titlePerangkat = findViewById( R.id.titlePerangkatTerhubung );
        titlePastikan = findViewById( R.id.titlePastikanAkunmu );
        titleTerhubung = findViewById( R.id.terhubungPada );
        titlePerangkatIni = findViewById( R.id.titlePerangkatIni );
        titleKamuTerhubung = findViewById( R.id.titleKamuTerhubung );
        styleFont = Typeface.createFromAsset( getAssets(),"fonts/Poppins-Medium.ttf" );

        titlePerangkat.setTypeface( styleFont );
        titlePastikan.setTypeface( styleFont );
        titlePerangkatIni.setTypeface( styleFont );
        titleTerhubung.setTypeface( styleFont );
        androidType.setTypeface( styleFont );
        titleKamuTerhubung.setTypeface( styleFont );

        backArrow.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        } );

        Field[] fields = Build.VERSION_CODES.class.getFields();
        String osName = fields[Build.VERSION.SDK_INT + 1].getName();

        androidType.setText( Build.MODEL );
        terhubungPada.setText( "Terhubung pada " + user.get( sharedPreferences.KEY_TANGGAL ) );
        versiAndroid.setText( "Versi " + String.valueOf( Build.VERSION.SDK_INT ) + " " + osName );
    }
}
