package com.example.tokoemak.tokoemak.fragments;


import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.example.tokoemak.tokoemak.Interface.ItemClickListener;
import com.example.tokoemak.tokoemak.R;
import com.example.tokoemak.tokoemak.ViewHolder.FiturViewHolder;
import com.example.tokoemak.tokoemak.ViewHolder.KategoriViewHolder;
import com.example.tokoemak.tokoemak.ViewHolder.PenawaranViewHolder;
import com.example.tokoemak.tokoemak.activities.CartActivity;
import com.example.tokoemak.tokoemak.activities.ItemActivity;
import com.example.tokoemak.tokoemak.activities.NewsActivity;
import com.example.tokoemak.tokoemak.activities.ProdukActivity;
import com.example.tokoemak.tokoemak.activities.ScanQRActivity;
import com.example.tokoemak.tokoemak.activities.SearchBarangActivity;
import com.example.tokoemak.tokoemak.activities.UpdateUser;
import com.example.tokoemak.tokoemak.adapter.BannerAdapter;
import com.example.tokoemak.tokoemak.adapter.MenuKategoriAdapter;
import com.example.tokoemak.tokoemak.adapter.NewsAdapter;
import com.example.tokoemak.tokoemak.app.AppController;
import com.example.tokoemak.tokoemak.commons.CurrentUser;
import com.example.tokoemak.tokoemak.commons.SharedPreferences;
import com.example.tokoemak.tokoemak.model.BannerModel;
import com.example.tokoemak.tokoemak.model.BeritaModel;
import com.example.tokoemak.tokoemak.model.FiturModel;
import com.example.tokoemak.tokoemak.model.KategoriModel;
import com.example.tokoemak.tokoemak.model.KontenModel;
import com.example.tokoemak.tokoemak.model.MenuKategoriModel;
import com.example.tokoemak.tokoemak.model.PenawaranModel;
import com.example.tokoemak.tokoemak.model.UserDiskonModel;
import com.example.tokoemak.tokoemak.service.ListenOrder;
import com.example.tokoemak.tokoemak.util.Server;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.zxing.Result;
import com.rbrooks.indefinitepagerindicator.IndefinitePagerIndicator;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.varunest.sparkbutton.SparkEventListener;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import es.dmoral.toasty.Toasty;
import io.supercharge.shimmerlayout.ShimmerLayout;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {
    private RecyclerView fiturRecyclerView,menuRecyclerView,bannerRecyclerView,recyclerViewPenawaran,newsRecyclerView;
    private FirebaseDatabase database;
    private DatabaseReference kategori,refDiskon;
    private DatabaseReference fitur;
    private DatabaseReference penawaran;
    private FirebaseRecyclerOptions<KategoriModel> options;
    private FirebaseRecyclerAdapter<KategoriModel, KategoriViewHolder> adapter;
    private FirebaseRecyclerOptions<FiturModel> optionsFitur;
    private FirebaseRecyclerAdapter<FiturModel, FiturViewHolder> adapterFitur;
    private FirebaseRecyclerOptions<PenawaranModel> optionsPenawaran;
    private FirebaseRecyclerAdapter<PenawaranModel, PenawaranViewHolder> adapterPenawaran;

    private FirebaseDatabase db = FirebaseDatabase.getInstance();
    private DatabaseReference news = db.getReference("berita");

    private SharedPreferences sharedPreferences;
    private HashMap<String, String> user = new HashMap<String, String>();
    private ScrollView scrollView;
    private TextView judulKategori,judulFitur,judulBerita,judulPenawaran,nameUser,maknews,textVoucher,textUsing;
    private LinearLayout layoutSearch;
    private ImageView scanQR,showVoucher;
    private LinearLayout linearLayoutCoupon;
    private ArrayList<MenuKategoriModel>menuList;
    private MenuKategoriAdapter menuKategoriAdapter;
    private ArrayList<KontenModel> modelList;
    private ArrayList<KontenModel> modelNewsList;
    private BannerAdapter mAdapter;
    private NewsAdapter mNewsAdapter;
    private Typeface style;
    private LinearLayout linearPoinUser;
    private ShimmerLayout shimmerLayout,shimmerBanner;
    private IndefinitePagerIndicator pagerIndicator;
    private Locale localeID = new Locale("in", "ID");
    private NumberFormat formatHarga = NumberFormat.getCurrencyInstance( localeID );
    public String [] diskonVoucher;

    public HomeFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View rootView = inflater.inflate( R.layout.fragment_home, container, false );

        sharedPreferences = new SharedPreferences( getActivity() );

        //init view
        shimmerLayout = rootView.findViewById( R.id.shimmerLayoutMenu );
        shimmerBanner = rootView.findViewById( R.id.shimmerLayoutBanner );
        scanQR = rootView.findViewById( R.id.scanQR );
        showVoucher = rootView.findViewById( R.id.iconVoucher );
        linearLayoutCoupon = rootView.findViewById( R.id.linearLayoutCoupon );
        menuRecyclerView = rootView.findViewById( R.id.rv_menuKategori );
        nameUser = rootView.findViewById( R.id.nameUser );
        bannerRecyclerView = rootView.findViewById( R.id.rv_bannerHome );
        nameUser = rootView.findViewById( R.id.nameUser );
        scrollView = rootView.findViewById( R.id.myView );
        recyclerViewPenawaran = rootView.findViewById( R.id.rv_penawaran );
        newsRecyclerView = rootView.findViewById( R.id.rv_newsHome );
        linearPoinUser = rootView.findViewById( R.id.linearPoinUser );
        pagerIndicator = rootView.findViewById( R.id.pagerIndicator );
        textVoucher = rootView.findViewById( R.id.textVoucher );
        textUsing = rootView.findViewById( R.id.textUsingVoucher );

        //find recyclerview
        fiturRecyclerView = rootView.findViewById( R.id.rv_fitur );
        menuRecyclerView = rootView.findViewById( R.id.rv_menuKategori );
        bannerRecyclerView = rootView.findViewById( R.id.rv_bannerHome );

        layoutSearch = rootView.findViewById( R.id.linearSearch );

        setOnClickButton();
        initKategoriMenu();
        initBannerHome();
        initNewsHome();

        //init firebase
        database = FirebaseDatabase.getInstance();
        kategori = database.getReference("kategori");
        fitur = database.getReference("fitur");
        penawaran = database.getReference("penawaran");
        refDiskon = database.getReference( "diskon" );

        judulKategori = rootView.findViewById( R.id.judulKategori );
        judulFitur = rootView.findViewById( R.id.judulFitur );
        judulBerita = rootView.findViewById( R.id.judulBacaBerita );
        judulPenawaran = rootView.findViewById( R.id.judulPenawaran );
        maknews = rootView.findViewById( R.id.textMaknews );

        setTypeface();
        setUsername();
        detectCoupon();
        initFitur();
        initPenawaran();

        if(!isNetworkAvailable()){
            setToastMessage( "Tidak ada koneksi internet" );
        }

        return rootView;
    }

    private void setTypeface() {
        style = Typeface.createFromAsset( getActivity().getAssets(), "fonts/Poppins-Medium.ttf" );
        nameUser.setTypeface( style );
        judulKategori.setTypeface( style );
        judulFitur.setTypeface( style );
        judulBerita.setTypeface( style );
        judulPenawaran.setTypeface( style );
        maknews.setTypeface( style );
        textVoucher.setTypeface( style );
        textUsing.setTypeface( style );
    }


    private void setUsername() {
        //set nama user
        String nickName;
        user = sharedPreferences.getUserDetails();
        nickName = user.get( sharedPreferences.KEY_NAME );

        //set nickname
        nameUser.setText( nickName );
    }


    private void setOnClickButton() {

        layoutSearch.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity( new Intent( getActivity(), SearchBarangActivity.class ) );
            }
        } );


        scanQR.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity( new Intent( getActivity(), ScanQRActivity.class ) );
            }
        } );

        showVoucher.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setToastMessage( "Masih tahap pengembangan :)" );
            }
        } );

        linearPoinUser.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setToastMessage( "Masih tahap pengembangan :)" );
            }
        } );
    }


    private void setToastMessage(String msg)
    {
        Toast toasty = Toasty.normal( getActivity() , msg);
        toasty.setGravity( Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0 );
        toasty.show();
    }

    private void initPenawaran()
    {
        optionsPenawaran = new FirebaseRecyclerOptions.Builder<PenawaranModel>()
                .setQuery(penawaran, PenawaranModel.class).build();

        adapterPenawaran = new FirebaseRecyclerAdapter<PenawaranModel, PenawaranViewHolder>(optionsPenawaran) {
            @Override
            protected void onBindViewHolder(@NonNull final PenawaranViewHolder holder, int position, @NonNull final PenawaranModel model) {
                holder.judulPenawaran.setTypeface( style );
                holder.judulPenawaran.setText( model.getNama() );
                holder.descPenawaran.setText( model.getDeskripsi() );
                try{
                    Glide.with( getContext() )
                            .load( model.getGambar() )
                            .into( holder.gambarPenawaran );
                }catch (Exception e){
                    e.printStackTrace();
                }

                holder.setItemClickListener( new ItemClickListener() {
                    @Override
                    public void onClick(View view, int position, boolean isLongClick) {

                    }
                } );

                holder.orderNowPenawaran.setOnClickListener( new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent( getActivity(), ProdukActivity.class );
                        intent.putExtra( "idkategori", model.getMenuid() );
                        startActivity( intent );
                    }
                } );

                holder.likePenawaran.setEventListener( new SparkEventListener() {
                    @Override
                    public void onEvent(ImageView button, boolean buttonState) {

                    }

                    @Override
                    public void onEventAnimationEnd(ImageView button, boolean buttonState) {

                    }

                    @Override
                    public void onEventAnimationStart(ImageView button, boolean buttonState) {

                    }
                } );
            }

            @NonNull
            @Override
            public PenawaranViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                View view = LayoutInflater.from( viewGroup.getContext() ).inflate( R.layout.item_penawaran, viewGroup, false );
                return new PenawaranViewHolder(view);
            }
        };

        LinearLayoutManager linearLayoutManagerku = new LinearLayoutManager( getActivity(), LinearLayoutManager.HORIZONTAL, false );
        RecyclerView.LayoutManager layoutManager = linearLayoutManagerku;
        recyclerViewPenawaran.setLayoutManager( layoutManager );
        adapterPenawaran.startListening();
        recyclerViewPenawaran.setAdapter( adapterPenawaran );
    }

    private void initFitur() {
        optionsFitur = new FirebaseRecyclerOptions.Builder<FiturModel>()
                .setQuery(fitur, FiturModel.class).build();

        adapterFitur = new FirebaseRecyclerAdapter<FiturModel, FiturViewHolder>(optionsFitur) {
            @Override
            protected void onBindViewHolder(@NonNull final FiturViewHolder holder, int position, @NonNull FiturModel model) {
                Glide.with( getContext() )
                        .load( model.getGambar() )
                        .into( holder.imgFitur );
                holder.namaFitur.setTypeface( style );
                holder.namaFitur.setText( model.getNama() );
                holder.descFitur.setText( model.getDeskripsi() );
                Typeface styleku = Typeface.createFromAsset( getActivity().getAssets(), "fonts/Sequel.ttf" );
                holder.descFitur.setTypeface( styleku );
                holder.setItemClickListener( new ItemClickListener() {
                    @Override
                    public void onClick(View view, int position, boolean isLongClick) {

                    }
                } );

                holder.orderNow.setOnClickListener( new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        scrollView.scrollTo( 0,0 );
                    }
                } );
            }

            @NonNull
            @Override
            public FiturViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                View view = LayoutInflater.from( viewGroup.getContext() ).inflate( R.layout.item_fitur, viewGroup, false );
                return new FiturViewHolder(view);
            }
        };

        LinearLayoutManager fiturLinearLayout = new LinearLayoutManager( getContext(), LinearLayoutManager.HORIZONTAL, false );
        RecyclerView.LayoutManager rvLilayoutku = fiturLinearLayout;
        fiturRecyclerView.setLayoutManager( rvLilayoutku );
        adapterFitur.startListening();
        fiturRecyclerView.setAdapter( adapterFitur );
    }


    private void initNewsHome()
    {
        modelNewsList = new ArrayList<>();
        loadNewsHome( "home_news" );
        mNewsAdapter = new NewsAdapter( getActivity(),modelNewsList );
        newsRecyclerView.setLayoutManager( new LinearLayoutManager( getActivity(), LinearLayoutManager.HORIZONTAL,false ) );
        newsRecyclerView.setHasFixedSize( true );
        newsRecyclerView.setAdapter( mNewsAdapter );
    }

    private void loadNewsHome(final String idkontenkategori)
    {
        String urlWithParams = Server.REST_API_KONTEN + "?idkontenkategori=" + idkontenkategori;
        StringRequest stringRequest = new StringRequest( Request.Method.GET, urlWithParams, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    System.out.println("Response" + response);
                    JSONArray jsonArray = new JSONArray( response );
                    for(int i = 0; i < jsonArray.length(); i++){
                        try{
                            JSONObject data = jsonArray.getJSONObject( i );
                            KontenModel model = new KontenModel();
                            model.setIdkonten( data.getString( "idkonten" ) );
                            model.setIdkontenkategori( data.getString( "idkontenkategori" ) );
                            model.setNama( data.getString( "nama" ) );
                            model.setGambar( data.getString( "gambar" ) );
                            model.setDeskripsi( data.getString( "deskripsi" ) );
                            model.setUrl( data.getString( "url" ) );
                            modelNewsList.add( model );
                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                }catch (JSONException e){
                    e.printStackTrace();
                }
                mNewsAdapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                System.out.println("error : " + error.getMessage());
            }
        } );
        AppController.getInstance().addToRequestQueue(stringRequest,"json_obj_req");
    }

    private void initKategoriMenu()
    {
        shimmerLayout.setVisibility( View.VISIBLE );
        shimmerLayout.startShimmerAnimation();
        menuList  = new ArrayList<>(  );
        loadKategoriMenu();
        menuKategoriAdapter = new MenuKategoriAdapter(HomeFragment.this,menuList);
        menuRecyclerView.setLayoutManager( new GridLayoutManager( getActivity(),2, GridLayoutManager.HORIZONTAL,false ) );
        menuRecyclerView.setHasFixedSize( true );
        menuRecyclerView.setAdapter( menuKategoriAdapter );
    }

    private void initBannerHome()
    {
        shimmerBanner.setVisibility( View.VISIBLE );
        shimmerBanner.startShimmerAnimation();
        modelList = new ArrayList<>(  );
        loadBannerHome("home_banner");
        mAdapter = new BannerAdapter( getActivity(),modelList );
        bannerRecyclerView.setLayoutManager( new LinearLayoutManager( getActivity(),LinearLayoutManager.HORIZONTAL,false ) );
        bannerRecyclerView.setHasFixedSize( true );
        bannerRecyclerView.setAdapter( mAdapter );
        pagerIndicator.attachToRecyclerView( bannerRecyclerView );
    }

    private void loadBannerHome(final String idkontenkategori) {
        pagerIndicator.setVisibility( View.GONE );
        String urlWithParams = Server.REST_API_KONTEN + "?idkontenkategori=" + idkontenkategori;
        StringRequest stringRequest = new StringRequest( Request.Method.GET, urlWithParams, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    JSONArray jsonArray = new JSONArray( response );
                    for(int i = 0; i < jsonArray.length(); i++){
                        try{
                            JSONObject data = jsonArray.getJSONObject( i );
                            KontenModel model = new KontenModel();
                            model.setIdkonten( data.getString( "idkonten" ) );
                            model.setIdkontenkategori( data.getString( "idkontenkategori" ) );
                            model.setNama( data.getString( "nama" ) );
                            model.setGambar( data.getString( "gambar" ) );
                            model.setDeskripsi( data.getString( "deskripsi" ) );
                            model.setUrl( data.getString( "url" ) );
                            modelList.add( model );
                            shimmerBanner.setVisibility( View.GONE );
                            shimmerBanner.stopShimmerAnimation();
                            pagerIndicator.setVisibility( View.VISIBLE );
                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                }catch (JSONException e){
                    e.printStackTrace();
                }
                mAdapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                System.out.println("error : " + error.getMessage());
            }
        } ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("idkontenkategori", idkontenkategori );
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(stringRequest,"json_obj_req");
    }


    private void loadKategoriMenu() {
        String urlWithParams = Server.REST_API_KATEGORI + "?idrole=3";
        StringRequest stringRequest = new StringRequest( Request.Method.GET, urlWithParams, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray( response );
                    for(int i = 0; i < jsonArray.length(); i++){
                        try {
                            JSONObject data = jsonArray.getJSONObject( i );
                            MenuKategoriModel menuKategoriModel = new MenuKategoriModel();
                            menuKategoriModel.setKode( data.getString( "idkategori" ));
                            menuKategoriModel.setNama( data.getString( "kategori" ));
                            menuKategoriModel.setGambar( data.getString( "icon" ));
                            menuList.add( menuKategoriModel );
                            shimmerLayout.stopShimmerAnimation();
                            shimmerLayout.setVisibility( View.GONE );
                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                    menuKategoriAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        } );

        AppController.getInstance().addToRequestQueue(stringRequest, "json_obj_req");
    }


    private void detectCoupon(){
        linearLayoutCoupon.setVisibility( View.GONE );
        String iduser = user.get( sharedPreferences.KEY_IDUSER );
        String urlWithParams = Server.REST_API_PROMO + "?iduser=" + iduser + "&act=get";
        StringRequest stringRequest = new StringRequest( Request.Method.GET, urlWithParams, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject data = new JSONObject( response );
                    String jumlahDiskon = formatHarga.format( Integer.parseInt( data.getString( "diskon" ) ) );
                    diskonVoucher = jumlahDiskon.split( "\\." );
                    textVoucher.setText( "Diskon " + diskonVoucher[0] + "rb" + " terpasang" );
                    linearLayoutCoupon.setVisibility( View.VISIBLE );
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        } );

        AppController.getInstance().addToRequestQueue( stringRequest,"json_obj_req" );
    }


    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    public void onStart() {
        super.onStart();
        if(adapter != null){
            adapter.startListening();
        }

        if(adapterFitur != null){
            adapterFitur.startListening();
        }

        if(adapterPenawaran != null){
            adapterPenawaran.startListening();
        }

        if(!isNetworkAvailable()){
            setToastMessage( "Tidak ada koneksi internet" );
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if(adapter != null){
            adapter.stopListening();
        }
        if(adapterFitur != null){
            adapterFitur.stopListening();
        }
        if(adapterPenawaran != null){
            adapterPenawaran.stopListening();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(adapter != null){
            adapter.startListening();
        }
        if(adapterFitur != null){
            adapterFitur.startListening();
        }

        if(adapterPenawaran != null){
            adapterPenawaran.startListening();
        }

        if(!isNetworkAvailable()){
            setToastMessage( "Tidak ada koneksi internet" );
        }else{
            detectCoupon();
        }
    }

}
