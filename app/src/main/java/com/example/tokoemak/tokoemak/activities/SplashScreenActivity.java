package com.example.tokoemak.tokoemak.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.example.tokoemak.tokoemak.R;

public class SplashScreenActivity extends AppCompatActivity {
    private TextView titleSplash;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        checkSDKVersion();
        initViews();
        showNextActivity();
    }

    private void checkSDKVersion() {
        // In Activity's onCreate() for instance
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }
    }

    private void initViews() {
        titleSplash = findViewById( R.id.titleSplash );
        Typeface style = Typeface.createFromAsset( getAssets(), "fonts/neo.ttf" );
        titleSplash.setTypeface( style );
    }

    private void showNextActivity() {
        Thread timer = new Thread(  ){
            @Override
            public void run() {
                try {
                    sleep( 3000 );
                }catch (InterruptedException e){
                    e.printStackTrace();
                }finally {
                    startActivity( new Intent( SplashScreenActivity.this, LandingPage.class ) );
                    finish();
                }
            }
        };

        timer.start();
    }
}
