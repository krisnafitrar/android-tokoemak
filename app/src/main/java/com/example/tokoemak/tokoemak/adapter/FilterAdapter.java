package com.example.tokoemak.tokoemak.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.tokoemak.tokoemak.R;
import com.example.tokoemak.tokoemak.activities.ItemActivity;
import com.example.tokoemak.tokoemak.app.AppController;
import com.example.tokoemak.tokoemak.model.FilterModel;
import com.example.tokoemak.tokoemak.model.ProdukModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class FilterAdapter extends RecyclerView.Adapter<FilterAdapter.ViewProcessHolder> {
    private Context mContext;
    private ArrayList<FilterModel>mList;
    private ArrayList<ProdukModel> filterList;
    private ProdukAdapter mAdapter;

    public FilterAdapter(Context mContext, ArrayList<FilterModel> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @NonNull
    @Override
    public FilterAdapter.ViewProcessHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from( mContext ).inflate( R.layout.filter_produk_layout,viewGroup,false );
        return new ViewProcessHolder( view );
    }

    @Override
    public void onBindViewHolder(@NonNull FilterAdapter.ViewProcessHolder viewProcessHolder, int i) {
        final FilterModel filterModel = mList.get( i );
        CardView cardFilter;
        TextView nameFilter;
        int id = Integer.parseInt( filterModel.getId() );

        cardFilter = viewProcessHolder.card;
        nameFilter = viewProcessHolder.name;

        nameFilter.setText( filterModel.getNama() );
        if(Integer.parseInt( filterModel.getId() ) % 2 == 0){
            cardFilter.setCardBackgroundColor( Color.parseColor("#88cf81") );
        }else{
            cardFilter.setCardBackgroundColor( Color.parseColor("#84afd7") );
        }

        cardFilter.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ItemActivity itemActivity = new ItemActivity();
                filterList = new ArrayList<>(  );
                loadFiltering(itemActivity.kodeKategori,filterModel.getNama(),itemActivity);
//                mAdapter = new ProdukAdapter( mContext, filterList);
                itemActivity.recyclerViewProduk.setAdapter( mAdapter );

                Toast.makeText( mContext, "Filter : " + filterModel.getNama(), Toast.LENGTH_SHORT ).show();
            }
        } );


    }

    private void loadFiltering(String kodeKategori, String nama, ItemActivity itemActivity) {
        final String kode = kodeKategori;
        final String name = nama;
        StringRequest stringRequest = new StringRequest( Request.Method.POST, itemActivity.url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    JSONArray jsonArray = new JSONArray( response );
                    for(int i = 0; i < jsonArray.length(); i++){
                        try{
                            JSONObject data = jsonArray.getJSONObject( i );
                            System.out.println(data.getString( "nama" ));
                            ProdukModel model = new ProdukModel();
                            model.setKode_barang( data.getString( "kode_barang" ) );
                            model.setNama( data.getString( "nama" ) );
                            model.setHarga( data.getString( "harga" ) );
                            model.setStok( data.getString( "stok" ) );
                            model.setGambar(data.getString("gambar"));
                            filterList.add(model);
                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                    mAdapter.notifyDataSetChanged();
                }catch (JSONException exception){
                    exception.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d( "volley", "error : " + error.getMessage() );
            }
        } ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("kode_kategori", kode );
                params.put( "nama",name );
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(stringRequest,itemActivity.tag_json_obj);

    }


    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewProcessHolder extends RecyclerView.ViewHolder {
        CardView card;
        TextView name;
        public ViewProcessHolder(@NonNull View itemView) {
            super( itemView );
            card = itemView.findViewById( R.id.cardFilter );
            name = itemView.findViewById( R.id.filterProduk );
        }
    }
}
