package com.example.tokoemak.tokoemak.app;

import android.content.Context;
import android.os.Vibrator;
import android.view.View;
import android.view.animation.AnimationUtils;

import com.example.tokoemak.tokoemak.R;

public class ShakeLib {
    private Context mContext;
    private Vibrator vibrate;

    public ShakeLib(Context mContext)
    {
        this.mContext = mContext;
    }


    public void setShakeView(View mView)
    {
        mView.startAnimation( AnimationUtils.loadAnimation( mContext,R.anim.shake ) );
    }

}
