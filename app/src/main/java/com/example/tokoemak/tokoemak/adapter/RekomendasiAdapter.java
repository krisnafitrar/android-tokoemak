package com.example.tokoemak.tokoemak.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.tokoemak.tokoemak.R;
import com.example.tokoemak.tokoemak.activities.ProdukDetail;
import com.example.tokoemak.tokoemak.model.ProdukModel;

import java.util.ArrayList;

public class RekomendasiAdapter extends RecyclerView.Adapter<RekomendasiAdapter.ViewProcessHolder> {
    private Context mContext;
    private ArrayList<ProdukModel> mList;

    public RekomendasiAdapter(Context mContext, ArrayList<ProdukModel> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @NonNull
    @Override
    public RekomendasiAdapter.ViewProcessHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from( mContext ).inflate( R.layout.layout_produk_rekomendasi,viewGroup,false );
        return new ViewProcessHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RekomendasiAdapter.ViewProcessHolder viewProcessHolder, int i) {
        LinearLayout linearProduk;
        final ImageView gambarProduk;
        TextView namaProduk,hargaProduk;
        final ProdukModel produkModel = mList.get( i );

        linearProduk = viewProcessHolder.linear;
        gambarProduk = viewProcessHolder.gambar;
        namaProduk = viewProcessHolder.nama;
        hargaProduk = viewProcessHolder.harga;

        Glide.with( mContext )
                .load(produkModel.getGambar())
                .into( gambarProduk );

        namaProduk.setText( setNamaProduk(produkModel.getNama()) );
        hargaProduk.setText( "Rp." + produkModel.getHarga() );

        linearProduk.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( mContext,ProdukDetail.class );
                intent.putExtra( "gambar",produkModel.getGambar() );
                intent.putExtra( "nama",produkModel.getNama() );
                intent.putExtra( "harga",produkModel.getHarga() );
                intent.putExtra( "stok",produkModel.getStok() );
                intent.putExtra( "kode_barang",produkModel.getKode_barang() );
                mContext.startActivity( intent );
            }
        } );
    }

    private String setNamaProduk(String nama) {
        if(nama.length() < 30){
            return nama;
        }else{
            return nama.substring( 0,29 ) + "...";
        }

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewProcessHolder extends RecyclerView.ViewHolder {
        ImageView gambar;
        TextView nama,kategori,harga;
        LinearLayout linear;

        public ViewProcessHolder(@NonNull View itemView) {
            super( itemView );
            linear = itemView.findViewById( R.id.linearRekomendasi );
            gambar = itemView.findViewById( R.id.gambarRekomendasi );
            nama = itemView.findViewById( R.id.namaRekomendasi );
            harga = itemView.findViewById( R.id.hargaRekomendasi );
        }
    }
}
