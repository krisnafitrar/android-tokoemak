package com.example.tokoemak.tokoemak.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.example.tokoemak.tokoemak.BuildConfig;
import com.example.tokoemak.tokoemak.Interface.ItemClickListener;
import com.example.tokoemak.tokoemak.R;
import com.example.tokoemak.tokoemak.ViewHolder.BarangViewHolder;
import com.example.tokoemak.tokoemak.ViewHolder.KategoriViewHolder;
import com.example.tokoemak.tokoemak.adapter.BannerAdapter;
import com.example.tokoemak.tokoemak.adapter.FilterAdapter;
import com.example.tokoemak.tokoemak.adapter.ProdukAdapter;
import com.example.tokoemak.tokoemak.adapter.RekomendasiAdapter;
import com.example.tokoemak.tokoemak.app.AppController;
import com.example.tokoemak.tokoemak.model.BannerModel;
import com.example.tokoemak.tokoemak.model.BarangModel;
import com.example.tokoemak.tokoemak.model.FilterModel;
import com.example.tokoemak.tokoemak.model.KategoriModel;
import com.example.tokoemak.tokoemak.model.LokasiModel;
import com.example.tokoemak.tokoemak.model.OngkirModel;
import com.example.tokoemak.tokoemak.model.ProdukModel;
import com.example.tokoemak.tokoemak.util.Server;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.common.collect.Maps;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.database.core.view.View;
import com.mancj.materialsearchbar.MaterialSearchBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Method;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import es.dmoral.toasty.Toasty;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ItemActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private FirebaseDatabase database;
    private DatabaseReference itemRef, ongkirRef;
    private String kategoriId;
    private String nomor;
    private float ongkirPerkilo = 0, ongkirMinimal = 0;
    private String alamatUserNow;
    private ImageView backArrow;
    private TextView alamatUser;
    private double longitudeku, latitudeku;
    private String newLati = "", newLongi = "";
    private LocationManager manager;
    private HashMap<String, String> latitudeNew = new HashMap<String, String>();
    private HashMap<String, String> longitudeNew = new HashMap<String, String>();
    private LinearLayout linearLayout;
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;

    //deklarasi variabel api
    public String url = Server.URL_PRODUK;

    public RecyclerView recyclerViewProduk,recyclerViewFilter;
    private ArrayList<ProdukModel> modelList;
    private ArrayList<FilterModel>filterModels;
    private FilterAdapter mAdapter3;
    public ProdukAdapter mAdapter;

    private FirebaseRecyclerOptions<BarangModel> options;
    private FirebaseRecyclerAdapter<BarangModel, BarangViewHolder> adapter;
    private com.example.tokoemak.tokoemak.commons.SharedPreferences sharedPreferences;

    //deklarasi search
    private FirebaseRecyclerOptions<BarangModel> searchOptions;
    private FirebaseRecyclerAdapter<BarangModel, BarangViewHolder> searchAdapter;
    private List<String> rekomendasiSearch = new ArrayList<>();
    private MaterialSearchBar materialSearchBar;
    private HashMap<String, String> userDetail = new HashMap<String, String>();

    private FirebaseDatabase db = FirebaseDatabase.getInstance();
    private DatabaseReference lokasiLive = db.getReference( "lokasi" );
    private Typeface stylekuu;
    private int PLACE_PICKER_REQUEST = 1;
    public String tag_json_obj = "json_obj_req";
    private String namajenis;

    TextView namaKategori, itemTersedia;
    public String kodeKategori;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext( CalligraphyContextWrapper.wrap( newBase ) );
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        CalligraphyConfig.initDefault( new CalligraphyConfig.Builder().setDefaultFontPath( "fonts/Poppins-Regular.ttf" )
                .setFontAttrId( R.attr.fontPath )
                .build() );
        setContentView( R.layout.activity_item );

        //shared preference
        sharedPreferences = new com.example.tokoemak.tokoemak.commons.SharedPreferences( this );
        userDetail = sharedPreferences.getUserDetails();
        nomor = userDetail.get( sharedPreferences.KEY_NUMBER );
        stylekuu = Typeface.createFromAsset( getAssets(), "fonts/Poppins-Bold.ttf" );

        //init toasty
        Toasty.Config.getInstance()
                .tintIcon( true )
                .setToastTypeface( Typeface.createFromAsset( getAssets(), "fonts/Poppins-Regular.ttf" ) )
                .allowQueue( true )
                .apply();


        //init database
        database = FirebaseDatabase.getInstance();
        itemRef = database.getReference( "item" );
        ongkirRef = database.getReference( "ongkir" );

        //init view
        backArrow = (ImageView) findViewById( R.id.backArrow );
        itemTersedia = findViewById( R.id.namaItemTersedia );
        alamatUser = findViewById( R.id.alamatUserNow );
        linearLayout = findViewById( R.id.linearAlamat );
        Typeface styleku = Typeface.createFromAsset( getAssets(), "fonts/Poppins-Bold.ttf" );
        namaKategori.setTypeface( styleku );
        itemTersedia.setTypeface( styleku );
        alamatUser.setTypeface( styleku );
        recyclerViewFilter = findViewById( R.id.rv_filter );

        //goto maps
        linearLayout.setOnClickListener( new android.view.View.OnClickListener() {
            @Override
            public void onClick(android.view.View v) {
                Intent intent = new Intent( ItemActivity.this, MapsActivity.class );
                intent.putExtra( "lat", latitudeku );
                intent.putExtra( "long", longitudeku );
                startActivity( intent );
            }
        } );


        //ongkir
        ongkirRef.addValueEventListener( new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                OngkirModel ongkirModel = dataSnapshot.child( "tarif" ).getValue( OngkirModel.class );
                ongkirPerkilo = Float.parseFloat( ongkirModel.getPerkilo() );
                ongkirMinimal = Float.parseFloat( ongkirModel.getMinimal() );
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        } );


        //init recyclerview produk
        recyclerViewProduk = findViewById( R.id.rv_itemBarang );

        //get Intent
        if(getIntent() != null){
            kodeKategori = getIntent().getStringExtra( "idkategori" );
        }

        //init rv produk
        modelList = new ArrayList<>();
        loadProduk(kodeKategori);
        recyclerViewProduk.setLayoutManager( new GridLayoutManager( ItemActivity.this,2 ) );
        recyclerViewProduk.setHasFixedSize( true );
//        mAdapter = new ProdukAdapter( ItemActivity.this,modelList );
        recyclerViewProduk.setAdapter( mAdapter );

        backArrow.setOnClickListener( new android.view.View.OnClickListener() {
            @Override
            public void onClick(android.view.View v) {
                onBackPressed();
            }
        } );

        //search function
        materialSearchBar = (MaterialSearchBar) findViewById( R.id.search_barItem );
        materialSearchBar.setHint( "Cari barang kesukaanmu" );

        //fungsi rekomendasi
        loadSuggest();
        materialSearchBar.setLastSuggestions( rekomendasiSearch );
        materialSearchBar.setCardViewElevation( 10 );

        materialSearchBar.addTextChangeListener( new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //ketika user mengetikkan text maka list akan tampil

                List<String> rekomendasi = new ArrayList<String>();
                for (String search : rekomendasiSearch) //loop dengan rekomendasi search
                {
                    if (search.toLowerCase().contains( materialSearchBar.getText().toLowerCase() )) {
                        rekomendasi.add( search );
                    }
                }

                materialSearchBar.setLastSuggestions( rekomendasi );
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        } );
        materialSearchBar.setOnSearchActionListener( new MaterialSearchBar.OnSearchActionListener() {
            @Override
            public void onSearchStateChanged(boolean enabled) {
                //ketika search bar tertutup
                //restore original rekomendasi adapter

                if (!enabled)
                    recyclerView.setAdapter( adapter );
            }

            @Override
            public void onSearchConfirmed(CharSequence text) {
                startSearch( text );
            }

            @Override
            public void onButtonClicked(int buttonCode) {

            }
        } );


        recyclerViewProduk.setNestedScrollingEnabled( false );
    }


    public void loadProduk(String kodeKategori) {
        final String kode = kodeKategori;
        StringRequest stringRequest = new StringRequest( Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    JSONArray jsonArray = new JSONArray( response );
                    for(int i = 0; i < jsonArray.length(); i++){
                        try{
                            JSONObject data = jsonArray.getJSONObject( i );
                            ProdukModel model = new ProdukModel();
                            model.setKode_barang( data.getString( "idbarang" ) );
                            model.setNama( data.getString( "namabarang" ) );
                            model.setJenis( getJenis(data.getString("idjenis")) );
                            model.setHarga( data.getString( "harga" ) );
                            model.setStok( data.getString( "stok" ) );
                            model.setGambar(data.getString("gambar"));
                            modelList.add(model);
                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                    mAdapter.notifyDataSetChanged();
                }catch (JSONException exception){
                    exception.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d( "volley", "error : " + error.getMessage() );
            }
        } ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("idkategori", kode );
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(stringRequest,tag_json_obj);
    }

    private String getJenis(final String idjenis) {
        StringRequest stringRequest = new StringRequest( Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    JSONArray jsonArray = new JSONArray( response );
                    for(int i = 0; i < jsonArray.length(); i++){
                        try{
                            JSONObject data = jsonArray.getJSONObject( i );
                            namajenis = data.getString( "namajenis" );
                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                }catch (JSONException exception){
                    exception.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d( "volley", "error : " + error.getMessage() );
            }
        } ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("idjenis", idjenis );
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(stringRequest,tag_json_obj);

        return namajenis;
    }

    private void startSearch(CharSequence text) {
        searchOptions = new FirebaseRecyclerOptions.Builder<BarangModel>()
                .setQuery( itemRef.orderByChild( "nama" ).equalTo( text.toString() ), BarangModel.class ).build(); //filter / bandingkan dengan text yang dimasukkan user

        searchAdapter = new FirebaseRecyclerAdapter<BarangModel, BarangViewHolder>( searchOptions ) {
            @Override
            protected void onBindViewHolder(@NonNull BarangViewHolder holder, final int position, @NonNull BarangModel model) {
                holder.namaBarang.setTypeface( stylekuu );
                holder.namaBarang.setText( setNama( model.getNama() ) );
                holder.hargaBarang.setText( "Rp." + model.getHarga() );
                System.out.println( "NAMA BARANG = " + model.getNama() );
                try {
                    Glide.with( ItemActivity.this )
                            .load( model.getGambar() )
                            .into( holder.gambar );
                } catch (Exception e) {
                    Toast.makeText( ItemActivity.this, "" + e.getMessage(), Toast.LENGTH_SHORT ).show();
                }

                holder.setItemClickListener( new ItemClickListener() {
                    @Override
                    public void onClick(android.view.View view, int position, boolean isLongClick) {
                        Intent intent = new Intent( ItemActivity.this, ItemDetail.class );
                        intent.putExtra( "itemId", searchAdapter.getRef( position ).getKey() );
                        intent.putExtra( "latitude", latitudeku );
                        intent.putExtra( "longitude", longitudeku );
                        startActivity( intent );
                    }
                } );

                holder.btnLihat.setOnClickListener( new android.view.View.OnClickListener() {
                    @Override
                    public void onClick(android.view.View v) {
                        Intent intent = new Intent( ItemActivity.this, ItemDetail.class );
                        intent.putExtra( "itemId", searchAdapter.getRef( position ).getKey() );
                        System.out.println( "KEY = " + adapter.getRef( position ).getKey() );
                        intent.putExtra( "latitude", latitudeku );
                        intent.putExtra( "longitude", longitudeku );
                        startActivity( intent );
                    }
                } );
            }

            @NonNull
            @Override
            public BarangViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
                android.view.View view = LayoutInflater.from( ItemActivity.this ).inflate( R.layout.item_barang, viewGroup, false );
                return new BarangViewHolder( view );
            }
        };
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager( this );
        RecyclerView.LayoutManager rvLilayout = linearLayoutManager;
        recyclerView.setLayoutManager( rvLilayout );
        searchAdapter.startListening();
        recyclerView.setAdapter( searchAdapter );

    }

    private void loadSuggest() {
        itemRef.orderByChild( "menuid" ).equalTo( kategoriId )
                .addValueEventListener( new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        for (DataSnapshot postSnapshoot : dataSnapshot.getChildren()) {
                            BarangModel item = postSnapshoot.getValue( BarangModel.class );
                            rekomendasiSearch.add( item.getNama() ); // ambil nama untuk rekomendasi
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                } );
    }

    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermissions() {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_FINE_LOCATION);

        if (shouldProvideRationale) {
            // previously and checked "Never ask again".
            ActivityCompat.requestPermissions(ItemActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION},
                    REQUEST_PERMISSIONS_REQUEST_CODE);
        }
    }

    private String setNama(String nama) {
        String namaItem = nama;

        if(namaItem.length() >= 14 ){
            return namaItem.substring( 0,13 ) + " ...";
        }else{
            return namaItem;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    private void initLocation(){
        //init new Latitude from sharedpref
        latitudeNew = sharedPreferences.getLatitude();
        longitudeNew = sharedPreferences.getLongitude();
        newLati = latitudeNew.get( sharedPreferences.LATITUDE );
        newLongi = longitudeNew.get( sharedPreferences.LONGITUDE );

        manager = (LocationManager) getSystemService( Context.LOCATION_SERVICE );
        System.out.println( "STRING LATIKU : " + newLati + " STRING LONGIKU : " + newLongi );

        if (manager.isProviderEnabled( LocationManager.GPS_PROVIDER )) {
            if (ActivityCompat.checkSelfPermission( this, Manifest.permission.ACCESS_FINE_LOCATION ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission( this, Manifest.permission.ACCESS_COARSE_LOCATION ) != PackageManager.PERMISSION_GRANTED) {
                onBackPressed();
            } else {
                lokasiLive.addValueEventListener( new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.child( nomor ).exists()) {
                            LokasiModel lokasiModel = dataSnapshot.child( nomor ).getValue( LokasiModel.class );
                            double userLat, userLong;
                            userLat = Double.parseDouble( lokasiModel.getLatitude() );
                            userLong = Double.parseDouble( lokasiModel.getLongitude() );
                            showNewAddress( userLat, userLong );
                        } else {
                            showLiveAddress();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        Toast.makeText( ItemActivity.this, "" + databaseError.getMessage(), Toast.LENGTH_SHORT ).show();
                    }
                } );
            }
        } else {
            requestPermissions();
            Toast toasty = Toasty.normal( ItemActivity.this, "Aktifkan GPS anda", getResources().getDrawable( R.drawable.gps ) );
            toasty.setGravity( Gravity.TOP|Gravity.CENTER_HORIZONTAL,0,0 );
            toasty.show();
            onBackPressed();
        }
    }

    private void showNewAddress(double userLat, double userLong) {
        double newLatitude = userLat;
        double newLongitude = userLong;
        double latPoint;
        double longPoint;
        double totalOngkir;
        double jarakMeter;
        double jarakKilo;
        int bulatOngkir;

        latPoint = -7.1460516;
        longPoint = 111.8768421;

        latitudeku = newLatitude;
        longitudeku = newLongitude;

        Geocoder myLocation = new Geocoder( this, Locale.getDefault() );
        List<Address> mList;

        Location startPoint = new Location( "startPoint" );
        startPoint.setLatitude( latPoint );
        startPoint.setLongitude( longPoint );

        Location endPoint = new Location( "endPoint" );
        endPoint.setLatitude( latitudeku );
        endPoint.setLongitude(longitudeku );

        jarakMeter = startPoint.distanceTo( endPoint );
        jarakKilo = jarakMeter / 1000;
        DecimalFormat df = new DecimalFormat( "#.##" );


        System.out.println( "JARAK KM = " + jarakKilo );

        if (jarakKilo <= 4) {
            totalOngkir = ongkirMinimal;
            bulatOngkir = (int) totalOngkir;
        } else {
            totalOngkir = ongkirPerkilo * jarakKilo;
            bulatOngkir = (int) totalOngkir;
        }

        sharedPreferences.setOngkir( bulatOngkir );

        try {
            sharedPreferences.setJarak( Float.valueOf( df.format( jarakKilo ) ) );
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println( "MASUK TIDAK DEFAULT" );
        try {
            mList = myLocation.getFromLocation( newLatitude, newLongitude, 1 );
            alamatUserNow = mList.get( 0 ).getAddressLine( 0 );
            sharedPreferences.setAlamat( alamatUserNow );
            alamatUser.setText( alamatUserNow.substring( 0, 29 ) + " ..." );
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(!checkPermissions()){
            requestPermissions();
            onBackPressed();
            Toast toasty = Toasty.normal( ItemActivity.this, "Izinkan akses lokasi", getResources().getDrawable( R.drawable.ic_location_on_white_24dp) );
            toasty.setGravity( Gravity.TOP|Gravity.CENTER_HORIZONTAL,0,0 );
            toasty.show();
        }else{
            initLocation();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult( requestCode, resultCode, data );
    }

    private void showLiveAddress() {
        //lokasi lama
        LocationManager lm = (LocationManager) getSystemService( Context.LOCATION_SERVICE );
        FusedLocationProviderClient fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient( this );
        if (ActivityCompat.checkSelfPermission( this, Manifest.permission.ACCESS_FINE_LOCATION ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission( this, Manifest.permission.ACCESS_COARSE_LOCATION ) != PackageManager.PERMISSION_GRANTED) {
            return;
        }else{
            fusedLocationProviderClient.getLastLocation()
                    .addOnSuccessListener( new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            System.out.println( "LOCATION ONE : " + location );
                            if (location != null) {
                                System.out.println( "LOCATION SUCCESS : " + location );
                                showMyAddress( location );
                            }
                        }
                    } ).addOnFailureListener( new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {

                }
            } );

            final LocationListener locationListener = new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    showMyAddress( location );
                }

                @Override
                public void onStatusChanged(String provider, int status, Bundle extras) {

                }

                @Override
                public void onProviderEnabled(String provider) {

                }

                @Override
                public void onProviderDisabled(String provider) {

                }
            };

            lm.requestLocationUpdates( LocationManager.GPS_PROVIDER, 2000,10,locationListener );
        }
    }

    private void showMyAddress(Location location) {
        double latPoint;
        double longPoint;
        double totalOngkir;
        double jarakMeter;
        double jarakKilo;
        int bulatOngkir;

        latPoint = -7.1460516;
        longPoint = 111.8768421;

        latitudeku = location.getLatitude();
        longitudeku = location.getLongitude();
        Geocoder myLocation = new Geocoder( this, Locale.getDefault() );
        List<Address> mList;

        Location startPoint = new Location( "startPoint" );
        startPoint.setLatitude( latPoint );
        startPoint.setLongitude( longPoint );

        Location endPoint = new Location( "endPoint" );
        endPoint.setLatitude( latitudeku );
        endPoint.setLongitude( longitudeku );

        jarakMeter = startPoint.distanceTo( endPoint );
        jarakKilo = jarakMeter / 1000;
        DecimalFormat df = new DecimalFormat( "#.#" );


        System.out.println("JARAK KM = " + jarakKilo);

        if(jarakKilo <= 4){
            totalOngkir = ongkirMinimal;
            bulatOngkir = (int) totalOngkir;
        }else{
            totalOngkir = ongkirPerkilo * jarakKilo;
            bulatOngkir = (int) totalOngkir;
        }

        sharedPreferences.setOngkir( bulatOngkir );

        try{
            sharedPreferences.setJarak( Float.valueOf( df.format( jarakKilo ) ) );
        }catch (Exception e){
            e.printStackTrace();
        }

                System.out.println("MASUK DEFAULT");
                try {
                    mList = myLocation.getFromLocation( latitudeku, longitudeku, 1 );
                    alamatUserNow = mList.get( 0 ).getAddressLine( 0 );
                    sharedPreferences.setAlamat( alamatUserNow );
                    alamatUser.setText( alamatUserNow.substring( 0,29 ) + " ..." );
//                    setLatLong(latitudeku,longitudeku);
                } catch (IOException e) {
                    e.printStackTrace();
                }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult( requestCode, permissions, grantResults );

        if(requestCode == REQUEST_PERMISSIONS_REQUEST_CODE){

        }else if(grantResults[0] == PackageManager.PERMISSION_GRANTED){

        }else{
            requestPermissions();
        }
    }


}
