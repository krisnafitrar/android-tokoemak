package com.example.tokoemak.tokoemak.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.example.tokoemak.tokoemak.R;
import com.example.tokoemak.tokoemak.activities.CartActivity;
import com.example.tokoemak.tokoemak.activities.ItemDetail;
import com.example.tokoemak.tokoemak.activities.ProdukActivity;
import com.example.tokoemak.tokoemak.activities.ProdukDetail;
import com.example.tokoemak.tokoemak.commons.SharedPreferences;
import com.example.tokoemak.tokoemak.databases.Database;
import com.example.tokoemak.tokoemak.model.OrderModel;
import com.example.tokoemak.tokoemak.model.ProdukModel;
import com.varunest.sparkbutton.SparkButton;
import com.varunest.sparkbutton.SparkEventListener;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import io.supercharge.shimmerlayout.ShimmerLayout;

public class ProdukAdapter extends RecyclerView.Adapter<ProdukAdapter.ViewProcessHolder> {
    private Context mContext;
    private ArrayList<ProdukModel>mList;
    private Typeface typeface,typeface2;
    private Locale localeID = new Locale("in", "ID");
    private NumberFormat formatHarga = NumberFormat.getCurrencyInstance( localeID );
    private Context aContext;
    private ProdukActivity produkActivity;
    private List<OrderModel> cList = new ArrayList<>();
    private String jumlah,count;

    public ProdukAdapter(Context mContext, ArrayList<ProdukModel> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    public ProdukAdapter(ProdukActivity produkActivity,ArrayList<ProdukModel> mList) {
        this.mList = mList;
        this.produkActivity = produkActivity;
    }

    @NonNull
    @Override
    public ProdukAdapter.ViewProcessHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        aContext = this.produkActivity == null ? mContext : produkActivity;
        View view = LayoutInflater.from( aContext ).inflate( R.layout.layout_produk_rekomendasi,viewGroup,false );
        return new ViewProcessHolder( view );
    }


    @Override
    public void onBindViewHolder(@NonNull ProdukAdapter.ViewProcessHolder viewProcessHolder, int i) {
        LinearLayout linearProduk;
        final ImageView gambarProduk;
        TextView namaProduk,hargaProduk,diskonLabel,hargadiskon,namaJenis;
        ShimmerLayout shimmerLayout;

        final ProdukModel produkModel = mList.get( i );
        cList = new Database( aContext ).getCarts();

        linearProduk = viewProcessHolder.linear;
        gambarProduk = viewProcessHolder.gambar;
        namaProduk = viewProcessHolder.nama;
        hargaProduk = viewProcessHolder.harga;
        shimmerLayout = viewProcessHolder.shimmerLayout;
        diskonLabel = viewProcessHolder.diskon;
        hargadiskon = viewProcessHolder.hargadiskon;
        namaJenis = viewProcessHolder.namaJenis;

        typeface = Typeface.createFromAsset( aContext.getAssets(),"fonts/Poppins-Medium.ttf" );
        typeface2 = Typeface.createFromAsset( aContext.getAssets(),"fonts/roboto.ttf" );
        hargaProduk.setTypeface( typeface );
        namaJenis.setTypeface( typeface );
        diskonLabel.setTypeface( typeface );
        namaProduk.setTypeface( typeface2 );

        Glide.with( aContext )
                .load(produkModel.getGambar())
                .into( gambarProduk );

        //set text of produk
        namaProduk.setText( setNamaProduk(produkModel.getNama()) );
        namaJenis.setText( produkModel.getJenis() );


        if(Integer.parseInt( produkModel.getDiskon() ) > 1){
            shimmerLayout.setVisibility( View.VISIBLE );
            shimmerLayout.startShimmerAnimation();
            diskonLabel.setText( produkModel.getDiskon() + "% Off" );
            hargaProduk.setText( formatHarga.format(Integer.parseInt( produkModel.getHarga() ) -
                    ((Integer.parseInt( produkModel.getDiskon() ) * Integer.parseInt( produkModel.getHarga() ))
                             / 100 ) ));
            hargadiskon.setVisibility( View.VISIBLE );
            hargadiskon.setText( formatHarga.format(Integer.parseInt( produkModel.getHarga() ) ) );
            hargadiskon.setPaintFlags(hargadiskon.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }else {
            shimmerLayout.setVisibility( View.GONE );
            hargaProduk.setText( formatHarga.format(Integer.parseInt( produkModel.getHarga() ) ) );
            hargadiskon.setVisibility( View.GONE );
        }


        linearProduk.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( aContext,ProdukDetail.class );
                intent.putExtra( "gambar",produkModel.getGambar() );
                intent.putExtra( "nama",produkModel.getNama() );
                intent.putExtra( "jenis", produkModel.getJenis() );
                intent.putExtra( "harga", produkModel.getHarga() );
                intent.putExtra( "diskon", produkModel.getDiskon() );
                intent.putExtra( "stok",produkModel.getStok() );
                intent.putExtra( "deskripsi", produkModel.getDeskripsi() );
                intent.putExtra( "kode_barang",produkModel.getKode_barang() );
                if(produkActivity != null){
                    intent.putExtra( "latitude",produkActivity.tmpLat );
                    intent.putExtra( "longitude",produkActivity.tmpLong );
                }
                aContext.startActivity( intent );
            }
        } );


    }

    private String setNamaProduk(String nama) {
        if(nama.length() < 30){
            return nama;
        }else{
            return nama.substring( 0,29 ) + "...";
        }

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }


    public class ViewProcessHolder extends RecyclerView.ViewHolder {
        ImageView gambar;
        TextView nama,harga,diskon,hargadiskon,namaJenis;
        LinearLayout linear;
        ShimmerLayout shimmerLayout;


        public ViewProcessHolder(@NonNull View itemView) {
            super( itemView );
            linear = itemView.findViewById( R.id.linearRekomendasi );
            gambar = itemView.findViewById( R.id.gambarRekomendasi );
            nama = itemView.findViewById( R.id.namaRekomendasi );
            harga = itemView.findViewById( R.id.hargaRekomendasi );
            shimmerLayout = itemView.findViewById( R.id.shimmerBadgesDiskon );
            diskon = itemView.findViewById( R.id.diskonLabel );
            hargadiskon = itemView.findViewById( R.id.hargaDiskon );
            namaJenis = itemView.findViewById( R.id.namaJenis );
        }
    }
}
