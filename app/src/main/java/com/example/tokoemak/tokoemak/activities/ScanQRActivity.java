package com.example.tokoemak.tokoemak.activities;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.budiyev.android.codescanner.CodeScanner;
import com.budiyev.android.codescanner.CodeScannerView;
import com.budiyev.android.codescanner.DecodeCallback;
import com.example.tokoemak.tokoemak.R;
import com.example.tokoemak.tokoemak.app.AppController;
import com.example.tokoemak.tokoemak.commons.SharedPreferences;
import com.example.tokoemak.tokoemak.model.DiskonModel;
import com.example.tokoemak.tokoemak.model.UserDiskonModel;
import com.example.tokoemak.tokoemak.util.Server;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.zxing.Result;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import cn.pedant.SweetAlert.SweetAlertDialog;
import es.dmoral.toasty.Toasty;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class ScanQRActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler {
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;
    private FirebaseDatabase db;
    private DatabaseReference refDiskon;
    private SharedPreferences sharedPreferences;
    private HashMap<String,String>user = new HashMap<String, String>(  );
    private CodeScanner mCodeScanner;
    private CodeScannerView mScannerView;
    private String idUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
//        scannerView = new ZXingScannerView( ScanQRActivity.this );
        setContentView( R.layout.activity_scan_qr );

        //init firebase database
        db = FirebaseDatabase.getInstance();
        refDiskon = db.getReference( "diskon" );
        Typeface styleFont = Typeface.createFromAsset( getAssets(),"fonts/Poppins-Medium.ttf" );
        TextView title = findViewById( R.id.textToolbarQRCode );
        title.setTypeface( styleFont );
        ImageView backArrow = findViewById( R.id.backArrowQR );
        backArrow.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        } );

        //init sharedpreferences
        sharedPreferences = new SharedPreferences( this );
        user = sharedPreferences.getUserDetails();
        idUser = user.get( sharedPreferences.KEY_IDUSER );


        //init scanner
        mScannerView = findViewById( R.id.scanner_view );
        mCodeScanner = new CodeScanner( this, mScannerView );

        mCodeScanner.setDecodeCallback( new DecodeCallback() {
            @Override
            public void onDecoded(@androidx.annotation.NonNull Result result) {
                verifyCode(result,idUser);
            }
        } );

        mScannerView.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCodeScanner.startPreview();
            }
        } );

    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!checkPermissions()){
            requestPermission();
        }else{
            mCodeScanner.startPreview();
        }
    }

    @Override
    protected void onPause() {
        mCodeScanner.releaseResources();
        super.onPause();
    }


    @Override
    protected void onStop() {
        super.onStop();
        mCodeScanner.stopPreview();
    }

    @Override
    public void handleResult(final Result result) {
        mCodeScanner.startPreview();
    }

    //algoritma voucher
    //1.cek voucher apakah valid
    //2. jika valid cek tanggal expired dengan tanggal scan code
    //3. jika masih berlaku maka cek apakah voucher sudah digunakan
    //4. jika belum maka gunakan voucher
    //5. jika user scan-code dan voucher sedang digunakan, maka tampilkan umpan balik

    private void verifyCode(final Result result, String idUser){
        String urlWithParams = Server.REST_API_PROMO +  "?kode=" + result.getText() + "&iduser=" + idUser + "&act=verify";
        StringRequest stringRequest = new StringRequest( Request.Method.GET, urlWithParams, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject data = new JSONObject( response );
                    if(data.getBoolean( "status" ) == true){
                        new SweetAlertDialog( ScanQRActivity.this, SweetAlertDialog.SUCCESS_TYPE )
                                .setTitleText( "Klaim Berhasil" )
                                .setContentText( data.getString( "message" ) )
                                .show();
                    }else{
                        new SweetAlertDialog( ScanQRActivity.this, SweetAlertDialog.ERROR_TYPE )
                                .setTitleText( "Klaim Gagal" )
                                .setContentText( data.getString( "message" ) )
                                .show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        } );

        AppController.getInstance().addToRequestQueue( stringRequest,"json_obj_req" );
    }


    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }


    private void requestPermission() {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.CAMERA);

        if (shouldProvideRationale) {
            // previously and checked "Never ask again".
            ActivityCompat.requestPermissions(ScanQRActivity.this,
                    new String[]{Manifest.permission.CAMERA},
                    REQUEST_PERMISSIONS_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case REQUEST_PERMISSIONS_REQUEST_CODE:{
                //jika request di cancel, array nya kosong
                if(grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    //request permission di izinkan oleh user
                }else{
                    //request tidak diizinkan dan matikan fungsi yang berkaitan
                    requestPermission();
                }
                return;
            }
        }
    }
}
