package com.example.tokoemak.tokoemak.model;

public class ProdukModel {
    private String kode_barang;
    private String nama;
    private String harga;
    private String stok;
    private String gambar;
    private String jenis;
    private String deskripsi;
    private String iddiskon;
    private String diskon;
    private String potongan;
    private String hargadiskon;

    public ProdukModel() {
    }

    public ProdukModel(String kode_barang, String nama, String harga, String stok, String gambar, String jenis, String deskripsi, String iddiskon, String diskon, String potongan, String hargadiskon) {
        this.kode_barang = kode_barang;
        this.nama = nama;
        this.harga = harga;
        this.stok = stok;
        this.gambar = gambar;
        this.jenis = jenis;
        this.deskripsi = deskripsi;
        this.iddiskon = iddiskon;
        this.diskon = diskon;
        this.potongan = potongan;
        this.hargadiskon = hargadiskon;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getJenis() {
        return jenis;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }

    public String getKode_barang() {
        return kode_barang;
    }

    public void setKode_barang(String kode_barang) {
        this.kode_barang = kode_barang;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }

    public String getStok() {
        return stok;
    }

    public void setStok(String stok) {
        this.stok = stok;
    }

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }

    public String getIddiskon() {
        return iddiskon;
    }

    public void setIddiskon(String iddiskon) {
        this.iddiskon = iddiskon;
    }

    public String getDiskon() {
        return diskon;
    }

    public void setDiskon(String diskon) {
        this.diskon = diskon;
    }

    public String getPotongan() {
        return potongan;
    }

    public void setPotongan(String potongan) {
        this.potongan = potongan;
    }

    public String getHargadiskon() {
        return hargadiskon;
    }

    public void setHargadiskon(String hargadiskon) {
        this.hargadiskon = hargadiskon;
    }
}
