package com.example.tokoemak.tokoemak.activities;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.tokoemak.tokoemak.R;
import com.example.tokoemak.tokoemak.adapter.CartAdapter;
import com.example.tokoemak.tokoemak.app.AppController;
import com.example.tokoemak.tokoemak.app.ShakeLib;
import com.example.tokoemak.tokoemak.commons.SharedDiskon;
import com.example.tokoemak.tokoemak.commons.SharedPreferences;
import com.example.tokoemak.tokoemak.databases.Database;
import com.example.tokoemak.tokoemak.model.DiskonModel;
import com.example.tokoemak.tokoemak.model.OrderModel;
import com.example.tokoemak.tokoemak.model.RequestModel;
import com.example.tokoemak.tokoemak.model.UserDiskonModel;
import com.example.tokoemak.tokoemak.util.Server;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.zxing.Result;
import com.tapadoo.alerter.Alerter;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import cn.pedant.SweetAlert.SweetAlertDialog;
import dmax.dialog.SpotsDialog;
import es.dmoral.toasty.Toasty;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class CartActivity extends AppCompatActivity {
    public RecyclerView recyclerView;
    public RecyclerView.LayoutManager rvLayout;
    public int a = 0;
    private int totalAwal;
    private int totalku;
    private int sementaraHarga;
    private int ongkir;
    private DatePickerDialog datePickerDialog;
    private Locale localeID = new Locale("in", "ID");
    private NumberFormat formatHarga = NumberFormat.getCurrencyInstance( localeID );
    private FirebaseDatabase database;
    private DatabaseReference request;
    private TextView txtTotal,txtTotalSatu,txtEstimasiHarga,txtDiskon,txtOngkir,txtAlamatUser,txtPesan,jadwalPengantaran,judulPengantaran,jamPengantaran,txtTunai,txtBayarDgn,txtJmlDgn,titleTotal,txtMetodeBayar;
    private Button pilihTanggal, pilihJam;
    private TextView judulPesanan,judulKode,judulPembayaran;
    private SharedPreferences sharedPreferences;
    private HashMap<String, String> user = new HashMap<String, String>();
    private HashMap<String, String> jadwalPengiriman = new HashMap<String, String>();
    private HashMap<Integer, Integer> variable = new HashMap<Integer, Integer>(  );
    private List<OrderModel> mList = new ArrayList<>(  );
    private CartAdapter adapter;
    private EditText edtDiskon;
    private Typeface fontBold;
    private SimpleDateFormat dateFormatter;
    private long bedaHari;
    private String jadwalDikirim,iduser;
    private int diskon = 0;
    private TextView textFragmentCart;
    private Typeface typeface;
    private ImageView btnCheckDiskon,btnMetodeBayar;
    private int firstPrice = 0;
    private Vibrator vibrate;
    private double latitude,longitude;
    private boolean paymentMethod = false;
    private RelativeLayout checkOut;
    private TextView titlePaymentMethod,titleMyWallet,titleCash,walletBalance,cashBalance;
    private LinearLayout btnCashMethod,btnWalletMethod;
    private double mLatitude,mLongitude;
    private double distanceOnMeter,distanceOnKilo;
    public int ongkirPrice = 0;
    private String fullAddress;
    private String [] streetAddress;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ImageButton btnDelete;
    private int diskonVoucher = 0;
    private String idklaim = "";


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext( CalligraphyContextWrapper.wrap( newBase ) );
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        CalligraphyConfig.initDefault( new CalligraphyConfig.Builder().setDefaultFontPath( "fonts/roboto.ttf" )
                .setFontAttrId( R.attr.fontPath )
                .build());
        mList = new Database( this )
                .getCarts();

        // cek dulu list null atau tidak
        if (mList.size() == 0) {
            setNullView();
        } else {
            //set viewnya
            setContentView( R.layout.added_cart );
            ImageView backArrowAdd = findViewById( R.id.backArrowAddedCart );
            backArrowAdd.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            } );

            getDataFromIntent();

            // Inflate the layout for this fragment
            initSharedPreferences();

            //firebase database
            database = FirebaseDatabase.getInstance();
            request = database.getReference( "request" );

            //config custom toast
            configToasty();

            //init view
            initViews();

            //set typeface
            setTypefaceTextView();

            //set RecyclerView
            initCartRecyclerView();

            //init and set delivery time and check diskon
            deliveryTimeAndDiskon();

            //check internet and load cart
            checkInternetAndLoadCart();

            //get location of user
            getLatLong();

            //set alamat by lat long
            setUserAddress( latitude,longitude );

            //destroy cart
            destroyCart();

            btnMetodeBayar.setOnClickListener( new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    loadBottomView();
                }
            } );

            checkOutOrder();

        }
    }

    private void getDataFromIntent() {
        if(getIntent() != null){
            latitude = getIntent().getDoubleExtra( "lat",0 );
            longitude = getIntent().getDoubleExtra( "long",0 );
        }
    }


    private void checkInternetAndLoadCart() {
        if (!isNetworkAvailable()){
            setToastMessage("Koneksi tidak stabil");
            checkOut.setEnabled( false );
            checkOut.setBackgroundResource( R.drawable.btn_disable );
            btnDelete.setVisibility( View.GONE );
        }

        swipeRefreshLayout.setColorScheme(R.color.steel_blue,R.color.orange,R.color.colorAccent);
        swipeRefreshLayout.setOnRefreshListener( new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(!isNetworkAvailable()){
                    swipeRefreshLayout.setRefreshing( false );
                }else{
                    swipeRefreshLayout.setRefreshing( false );
                    getLatLong();
                    checkOut.setEnabled( true );
                    checkOut.setBackgroundResource( R.drawable.btn_tambah_item );
                    btnDelete.setVisibility( View.VISIBLE );
                }
            }
        } );
    }


    private void configToasty() {
        Toasty.Config.getInstance()
                .tintIcon( true )
                .setToastTypeface( Typeface.createFromAsset( getAssets(), "fonts/Poppins-Regular.ttf" ) )
                .allowQueue( true )
                .apply();
    }


    private void deliveryTimeAndDiskon() {
        jamPengantaran.setText( "-" );

        jadwalPengantaran.setText( getTanggal() );

        dateFormatter = new SimpleDateFormat( "dd-MM-yyyy", Locale.US );

        pilihTanggal.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDateDialog();
            }
        } );

        pilihJam.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTimeDialog();
            }
        } );

        edtDiskon.addTextChangedListener( new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length()< 1){
                    btnCheckDiskon.setVisibility( View.GONE );
                }else{
                    btnCheckDiskon.setVisibility( View.VISIBLE );
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        } );

        btnCheckDiskon.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isNetworkAvailable()){
                    verifyCode( edtDiskon.getText().toString(),user.get( sharedPreferences.KEY_IDUSER ) );
                }else{
                    setToastMessage( "Internet tidak stabil!" );
                }
            }
        } );
    }

    private void verifyCode(final String kode, String idUser){
        String urlWithParams = Server.REST_API_PROMO +  "?kode=" + kode + "&iduser=" + idUser + "&act=verify";
        StringRequest stringRequest = new StringRequest( Request.Method.GET, urlWithParams, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject data = new JSONObject( response );
                    if(data.getBoolean( "status" ) == true){
                        getDiskonVoucher( ongkirPrice );
                        new SweetAlertDialog( CartActivity.this, SweetAlertDialog.SUCCESS_TYPE )
                                .setTitleText( "Klaim Berhasil" )
                                .setContentText( data.getString( "message" ) )
                                .show();
                    }else{
                        edtDiskon.setText( "" );
                        edtDiskon.setEnabled( true );
                        new SweetAlertDialog( CartActivity.this, SweetAlertDialog.ERROR_TYPE )
                                .setTitleText( "Klaim Gagal" )
                                .setContentText( data.getString( "message" ) )
                                .show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        } );

        AppController.getInstance().addToRequestQueue( stringRequest,"json_obj_req" );
    }


    private void destroyCart() {
        btnDelete.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Database hapus = new Database( CartActivity.this );
                hapus.cleanCart();
                loadRV();

                //get Recycler view
                mList = new Database( CartActivity.this ).getCarts();
                adapter = new CartAdapter( mList, CartActivity.this );
                recyclerView.setAdapter( adapter );
                recyclerView.setNestedScrollingEnabled( false );

                //set null values and view
                setNullValues();
                setNullView();
                setToastMessage( "Berhasil membersihkan cart" );
            }
        } );
    }


    private void checkOutOrder() {
        checkOut.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int jam = 0;
                if (!jamPengantaran.getText().toString().equals( "-" )) {
                    if(paymentMethod){
                        String jamAntar = jamPengantaran.getText().toString();
                        final String[] cut = jamAntar.split( ":" );
                        jam = Integer.parseInt( cut[0] );
                        if (txtTotal.getText().toString().equals( "Rp0" )) {
                            setToastMessage( "Tidak ada belanjaan" );
                        }else if (jam >= 21 || jam < 7) {
                            setToastMessage( "Jam antar: 07:00-21:00" );
                        } else {
                            showDialogCheckOut();
                        }
                    }else{
                        vibrate(200);
                        ShakeLib shakeLib = new ShakeLib( CartActivity.this );
                        shakeLib.setShakeView( txtMetodeBayar );
                    }
                }else{
                    setToastMessage( "Atur waktu pengantaran" );
                    vibrate( 200 );
                    pilihJam.startAnimation( AnimationUtils.loadAnimation( CartActivity.this, R.anim.shake ) );
                }
            }
        } );
    }


    private void initCartRecyclerView() {
        recyclerView = findViewById( R.id.rv_listCart );
        recyclerView.setHasFixedSize( true );
        rvLayout = new LinearLayoutManager( CartActivity.this );
        recyclerView.setLayoutManager( rvLayout );
    }


    private void initSharedPreferences() {
        sharedPreferences = new SharedPreferences( CartActivity.this );
        variable = sharedPreferences.getVariableDetails();
        jadwalPengiriman = sharedPreferences.getJadwal();
        jadwalDikirim = jadwalPengiriman.get( sharedPreferences.JADWAL_PENGIRIMAN );

        //init variable
        user = sharedPreferences.getUserDetails();
        totalAwal = variable.get( sharedPreferences.TOTAL_AWAL );
        sementaraHarga = variable.get( sharedPreferences.SEMENTARA );
        totalku = variable.get( sharedPreferences.TOTAL );
    }


    private void setUserAddress(double mLat, double mLong) {
        Geocoder historyLoc = new Geocoder( this,Locale.getDefault() );
        List<Address>addresses;

        try{
            addresses = historyLoc.getFromLocation( mLat,mLong,1 );
            fullAddress = addresses.get( 0 ).getAddressLine( 0 );
            streetAddress = fullAddress.split( "," );
            txtAlamatUser.setText( setAlamat( streetAddress[0] ) );
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void getOngkirPrice(double distanceOnKilo) {
        String idOngkir = distanceOnKilo > 4 ? "more_than" : "less_than";
        final int dist = distanceOnKilo > 4 ? (int) distanceOnKilo : 1;
        String urlWithParams = Server.REST_API_ONGKIR + "?idongkir=" + idOngkir;
        StringRequest stringRequest = new StringRequest( Request.Method.GET, urlWithParams, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject data = new JSONObject( response );
                    ongkirPrice = Integer.parseInt( data.getString( "biaya" ) ) * dist;
                    getDiskonVoucher(ongkirPrice);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        } );

        AppController.getInstance().addToRequestQueue( stringRequest,"json_obj_req" );
    }


    private void getDiskonVoucher(final int ongkirPrice)
    {
        String urlWithParams = Server.REST_API_PROMO + "?iduser=" + user.get( sharedPreferences.KEY_IDUSER ) + "&act=get";
        StringRequest stringRequest = new StringRequest( Request.Method.GET, urlWithParams, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject data = new JSONObject( response );
                    diskonVoucher = Integer.parseInt( data.getString( "diskon" ) );
                    idklaim = data.getString( "idklaim" );
                    edtDiskon.setText( data.getString( "kodepromo" ) );
                    edtDiskon.setEnabled( false );
                    loadListItem(ongkirPrice);
                } catch (JSONException e) {
                    loadListItem(ongkirPrice);
                    edtDiskon.setEnabled( true );
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        } );

        AppController.getInstance().addToRequestQueue( stringRequest, "json_obj_req" );
    }


    private void getLatLong() {
        String urlWithParams = Server.REST_API_PENGATURAN + "?act=getlatlong&key1=mart_latitude&key2=mart_longitude";
        StringRequest stringRequest = new StringRequest( Request.Method.GET, urlWithParams, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    //jika response sukses maka ambil lat long
                    JSONObject data = new JSONObject( response );
                    if(data.getBoolean( "status" ) == true){
                        mLatitude = Double.valueOf( data.getString( "latitude" ) );
                        mLongitude = Double.valueOf( data.getString( "longitude" ) );
                        getDistanceOf( mLatitude,mLongitude,latitude,longitude );
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        } );

        AppController.getInstance().addToRequestQueue( stringRequest,"json_obj_req" );
    }


    private void getDistanceOf(double mLatitude, double mLongitude, double latitude, double longitude) {
        //set store position
        Location location = new Location( "startPoint" );
        location.setLatitude( mLatitude );
        location.setLongitude( mLongitude );

        //set user position
        Location uLocation = new Location( "endpoint" );
        uLocation.setLatitude( latitude );
        uLocation.setLongitude( longitude );

        distanceOnMeter = location.distanceTo( uLocation );
        distanceOnKilo = distanceOnMeter / 1000;
        getOngkirPrice(distanceOnKilo);
    }


    private void loadBottomView() {
        final int wallet = 0;

        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog( this );
        bottomSheetDialog.setContentView( R.layout.bottom_sheets_payment );
        bottomSheetDialog.setCanceledOnTouchOutside( true );
        final ImageView close,icon;
        icon = findViewById( R.id.icon_carabayar );
        close = bottomSheetDialog.findViewById( R.id.close_payment );
        titlePaymentMethod = bottomSheetDialog.findViewById( R.id.titlePaymentMethod );
        titleMyWallet = bottomSheetDialog.findViewById( R.id.titleMyWallet );
        titleCash = bottomSheetDialog.findViewById( R.id.titleCash );
        walletBalance = bottomSheetDialog.findViewById( R.id.walletBalance );
        cashBalance = bottomSheetDialog.findViewById( R.id.cashBalance );
        btnCashMethod = bottomSheetDialog.findViewById( R.id.btnCashMethod );
        btnWalletMethod = bottomSheetDialog.findViewById( R.id.btnWalletMethod );

        titlePaymentMethod.setTypeface( fontBold );
        titleMyWallet.setTypeface( fontBold );
        titleCash.setTypeface( fontBold );

        cashBalance.setText( formatHarga.format(totalku) );
        walletBalance.setText( formatHarga.format( 0 ) );

        btnWalletMethod.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(wallet < 1){
                    vibrate( 200 );
                    setToastMessage( "Saldo anda tidak cukup" );
                }
            }
        } );

        btnCashMethod.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                paymentMethod = true;
                txtTunai.setText( "Tunai" );
                txtMetodeBayar.setText( "Dengan Cash" );
                txtMetodeBayar.setTextColor( Color.parseColor("#000000") );
                icon.setImageResource( R.drawable.dollar );
                bottomSheetDialog.dismiss();
            }
        } );

        close.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomSheetDialog.dismiss();
            }
        } );

        bottomSheetDialog.show();
    }


    private void vibrate(int duration)
    {
        vibrate.vibrate( duration );
    }


    private void setNullValues() {
        edtDiskon.setText( "" );
        txtTotal.setText( "Rp0" );
        txtEstimasiHarga.setText( "0" );
        txtOngkir.setText( "0" );
        txtDiskon.setText( "0" );
        txtTotalSatu.setText( "0" );

        //set SharedPreference
        sharedPreferences.setTotalAwal( 0 );
        sharedPreferences.setOngkir( 0 );
        sharedPreferences.setDiskon( 0 );
        sharedPreferences.setSementara( 0 );
        sharedPreferences.setTotal( 0 );
    }


    private void setToastMessage(String msg)
    {
        Toast toasty = Toasty.normal( this, msg);
        toasty.setGravity( Gravity.TOP | Gravity.CENTER_HORIZONTAL, 0, 0 );
        toasty.show();
    }

    private void setTypefaceTextView() {
        judulPesanan.setTypeface( fontBold );
        judulKode.setTypeface( fontBold );
        judulPembayaran.setTypeface( fontBold );
        judulPengantaran.setTypeface( fontBold );
        txtTotal.setTypeface( fontBold );
        txtPesan.setTypeface( fontBold );
        txtBayarDgn.setTypeface( fontBold );
        txtJmlDgn.setTypeface( fontBold );
        txtTotalSatu.setTypeface( fontBold );
        titleTotal.setTypeface( fontBold );
        txtTunai.setTypeface( fontBold );
    }


    private void initViews() {
        fontBold = Typeface.createFromAsset( getAssets(),"fonts/Poppins-Medium.ttf" );
        TextView texttoolbar = findViewById( R.id.textToolbarAddedCart );
        texttoolbar.setTypeface( Typeface.createFromAsset( getAssets(),"fonts/Poppins-Medium.ttf" ) );

        checkOut = findViewById( R.id.btnCheckOut );
        txtEstimasiHarga = findViewById( R.id.txtEstimasiHarga );
        txtTotalSatu = findViewById( R.id.txtTotalSatu );
        txtDiskon = findViewById( R.id.txtDiskon );
        txtOngkir = findViewById( R.id.txtOngkir );
        txtTotal = findViewById( R.id.totalHarga );
        edtDiskon = findViewById( R.id.edt_kodeVoucher );
        judulPengantaran = findViewById( R.id.judulWaktuPengantaran );
        jadwalPengantaran = findViewById( R.id.tglPengantaran );
        pilihTanggal = findViewById( R.id.btnPilihTanggal );
        jamPengantaran = findViewById( R.id.jamPengantaran );
        pilihJam = findViewById( R.id.btnPilihJam );
        txtAlamatUser = findViewById( R.id.alamatUser );
        judulPesanan = findViewById( R.id.judulPesanan );
        judulKode = findViewById( R.id.judulKodeVoucher );
        judulPembayaran = findViewById( R.id.judulDetailPembayaran );
        txtPesan = findViewById( R.id.txtPesan );
        txtBayarDgn = findViewById( R.id.txtBayarDengan );
        txtJmlDgn = findViewById( R.id.titleBayarDengan );
        titleTotal = findViewById( R.id.titleTotal );
        txtTunai = findViewById( R.id.bottomTunai );
        btnCheckDiskon = findViewById( R.id.btnCheckDiskon );
        btnMetodeBayar = findViewById( R.id.btnPilihMetodeBayar );
        txtMetodeBayar = findViewById( R.id.textPilihMetodeBayar );
        swipeRefreshLayout = findViewById( R.id.swipeRefreshCart );
        btnDelete = findViewById( R.id.btnDeleteCart );
        vibrate = (Vibrator)getSystemService( Context.VIBRATOR_SERVICE );

        String disc = isNetworkAvailable()? formatHarga.format(0):"0";
        txtDiskon.setText( disc);
    }


    private void setNullView() {
        setContentView( R.layout.fragment_cart );
        ImageView backArrow = findViewById( R.id.backArrowCart );
        backArrow.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        } );
        textFragmentCart = findViewById( R.id.textCartFragment );
        typeface = Typeface.createFromAsset( getAssets(), "fonts/Poppins-Medium.ttf" );
        TextView texttoolbar = findViewById( R.id.textToolbarCart );
        texttoolbar.setTypeface( Typeface.createFromAsset( getAssets(),"fonts/Poppins-Medium.ttf" ) );
        textFragmentCart.setTypeface( typeface );
    }


    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    private void showTimeDialog() {
        Calendar mCurrentTime = Calendar.getInstance();
        final int hour = mCurrentTime.get(Calendar.HOUR_OF_DAY);
        final int minute = mCurrentTime.get(Calendar.MINUTE);

        TimePickerDialog timePicker = new TimePickerDialog( CartActivity.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minuteOfSelected) {
                jamPengantaran.setText( hourOfDay + ":" + minuteOfSelected );
            }
        },hour,minute,true );
        timePicker.setTitle( "Pilih jam" );
        timePicker.show();
    }


    private void showDateDialog() {
        Calendar newCalendar = Calendar.getInstance();

        datePickerDialog = new DatePickerDialog(CartActivity.this, new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);

                DateFormat date = new SimpleDateFormat("dd-MM-yyyy");
                try {
                    Date tanggalAkhir = date.parse( dateFormatter.format(newDate.getTime()) );
                    Date tanggalAwal = date.parse( getTanggal() );
                    bedaHari = Math.abs(tanggalAkhir.getTime() - tanggalAwal.getTime());
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                int tanggal = Integer.parseInt( String.valueOf(TimeUnit.MILLISECONDS.toDays(bedaHari) ) );

                if(tanggal > 2){
                    setToastMessage( "Penjadwalan tidak bisa melebih 2 hari" );
                }else{
                    jadwalPengantaran.setText(dateFormatter.format(newDate.getTime()));
                }

            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));


        datePickerDialog.getDatePicker().setMinDate( System.currentTimeMillis() - 1000 );
        datePickerDialog.show();
    }


    private void showDialogCheckOut() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder( CartActivity.this );
        alertDialog.setTitle( "Checkout barang anda" );
        alertDialog.setMessage( "Apakah anda yakin akan melakukan pembelian?" );
        final android.app.AlertDialog waitingDialog = new SpotsDialog( CartActivity.this );
        waitingDialog.setTitle( "Tunggu sebentar" );
        alertDialog.setPositiveButton( "Iya", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(txtEstimasiHarga.getText().toString().equals( "Rp0" )){
                    Toast.makeText( CartActivity.this, "Silahkan belanja terlebih dahulu", Toast.LENGTH_SHORT ).show();
                }else {
                    if (!isNetworkAvailable()) {
                        setToastMessage( "Internet tidak stabil" );
                    } else {
                        checkOutBarang();
                    }
                }
            }
        } );

        alertDialog.setNegativeButton( "BATAL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        } );

        alertDialog.show();
    }


    private String getTanggal(){
        DateFormat dateFormat = new SimpleDateFormat( "dd-MM-yyyy" );
        Date date = new Date();
        return dateFormat.format( date );
    }


    private String getWaktu(){
        DateFormat dateFormat = new SimpleDateFormat( "HH:mm:ss" );
        Date date = new Date();
        return dateFormat.format( date );
    }


    private void checkOutBarang() {
        //set variabel
        String kodetrx, jadwalAntar, estimasi, diskon, ongkir, total;
        iduser = user.get( sharedPreferences.KEY_IDUSER );
        jadwalAntar = jadwalPengantaran.getText().toString() + " " + jamPengantaran.getText().toString();
        estimasi = txtEstimasiHarga.getText().toString();
        diskon = txtDiskon.getText().toString();
        ongkir = txtOngkir.getText().toString();
        total = txtTotal.getText().toString();
        kodetrx = String.valueOf( System.currentTimeMillis() );

        insertNewTransaksi( kodetrx, iduser, jadwalAntar, fullAddress,String.valueOf( this.firstPrice ),String.valueOf( diskonVoucher ),String.valueOf(  ongkirPrice ), String.valueOf( this.totalku ) );
        insertNewTransaksiDetail( kodetrx, mList,total,estimasi,jadwalAntar );

        //hapus database cart
        Database db = new Database( CartActivity.this );
        db.cleanCart();
        loadListItem(0);
        sharedPreferences.setTotalAwal( 0 );
        sharedPreferences.setOngkir( 0 );
        sharedPreferences.setDiskon( 0 );
        sharedPreferences.setSementara( 0 );
        sharedPreferences.setTotal( 0 );

        //setDefault value
        edtDiskon.setText( "" );
        txtTotal.setText( "Rp0" );
        txtEstimasiHarga.setText( "0" );
        txtOngkir.setText( "0" );
        txtDiskon.setText( "0" );
        txtTotalSatu.setText( "0" );
        txtJmlDgn.setText( "0" );

        setNullView();
    }


    private void insertNewTransaksiDetail(final String kodetrx, final List<OrderModel> mList, final String total, final String estimasi, final String jadwalAntar) {
        for(int index = 0; index < mList.size(); index++){
            final int finalIndex = index;
            StringRequest stringRequest = new StringRequest( Request.Method.POST, Server.REST_API_TRANSAKSI, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    try {
                        JSONObject jsonObject = new JSONObject( response );
                        boolean success = jsonObject.getBoolean( "status" );

                        if(success){
                            RequestModel requestModel = new RequestModel(
                                    user.get( sharedPreferences.KEY_NUMBER ),
                                    user.get( sharedPreferences.KEY_NAME ),
                                    fullAddress, total, estimasi,
                                    String.valueOf( diskonVoucher ), String.valueOf( ongkirPrice ), getTanggal().toString() + " " + getWaktu().toString(),
                                    jadwalAntar, mList );
                            request.child( kodetrx )
                                    .setValue( requestModel );

                            new SweetAlertDialog( CartActivity.this, SweetAlertDialog.SUCCESS_TYPE )
                                    .setTitleText( "Order Berhasil" )
                                    .setContentText( "Pembelian anda berhasil" )
                                    .show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    new SweetAlertDialog( CartActivity.this, SweetAlertDialog.ERROR_TYPE )
                            .setTitleText( "Order Gagal" )
                            .setContentText( "Coba beli lagi!" )
                            .show();
                }
            } ){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String,String>params = new HashMap<>();
                    params.put( "act","a_detail" );
                    params.put( "kodetrans",kodetrx );
                    params.put( "idbarang",mList.get( finalIndex ).getProdukId() );
                    params.put( "jumlah", mList.get( finalIndex ).getJumlah() );
                    return params;
                }
            };

            AppController.getInstance().addToRequestQueue( stringRequest,"json_obj_req" );
        }
    }


    private void insertNewTransaksi(final String kodetrx, final String iduser, final String jadwalAntar, final String alamatUser, final String estimasi, final String diskon, final String ongkir, final String total) {
        StringRequest stringRequest = new StringRequest( Request.Method.POST, Server.REST_API_TRANSAKSI, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject( response );
                    boolean success = jsonObject.getBoolean( "status" );

                    if(success){
                        System.out.println("Berhasil insert transaksi");
                    }else{
                        System.out.println("Gagal insert transaksi");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        } ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String>params = new HashMap<>();
                params.put( "act","a_trans" );
                params.put( "kodetrans", kodetrx);
                params.put( "iduser",iduser);
                params.put( "jadwalkirim", jadwalAntar );
                params.put( "alamat",alamatUser );
                params.put( "estimasi",estimasi );
                params.put( "ongkir",ongkir );
                params.put( "diskon",diskon );
                params.put( "total",total );
                params.put( "idklaim",idklaim );
                params.put( "latitude", String.valueOf( latitude ) );
                params.put( "longitude", String.valueOf( latitude ) );
                return params;
            }
        };

        AppController.getInstance().addToRequestQueue( stringRequest,"json_obj_req" );

    }


    public void loadRV(){
        mList = new Database(CartActivity.this).getCarts();
        adapter = new CartAdapter( mList, CartActivity.this);
        recyclerView.setAdapter( adapter );
        recyclerView.setNestedScrollingEnabled(false);
    }


    public void loadListItem(int getOngkir) {
        int hargaAwal = 0;

        mList = new Database(CartActivity.this).getCarts();
        adapter = new CartAdapter( mList, CartActivity.this);
        recyclerView.setAdapter( adapter );
        recyclerView.setNestedScrollingEnabled(false);



        //hitung jumlah total
        for (OrderModel orderModel:mList)
            hargaAwal += (Integer.parseInt( orderModel.getHarga() )) * (Integer.parseInt( orderModel.getJumlah() ));

        final int finalHargaAwal = hargaAwal;
        firstPrice = hargaAwal;

        if(getOngkir == diskonVoucher){
            txtOngkir.setPaintFlags(txtOngkir.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            txtDiskon.setPaintFlags(txtDiskon.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }

        txtEstimasiHarga.setText( formatHarga.format( finalHargaAwal ) );
        txtOngkir.setText( formatHarga.format( getOngkir) );
        txtDiskon.setText( formatHarga.format( diskonVoucher ) );

        totalku = finalHargaAwal + getOngkir - diskonVoucher;
        txtTotalSatu.setText( formatHarga.format( totalku ) );
        txtTotal.setText( txtTotalSatu.getText().toString() );
        txtBayarDgn.setText( txtTotalSatu.getText().toString() );

        if(txtEstimasiHarga.getText().toString().equals( "Rp0" )){
            setNullView();
        }

    }


    private String setAlamat(String alamat) {
        if(alamat.length() > 29){
            return alamat.substring( 0, 29 ) + " ...";
        }else{
            return alamat;
        }
    }

}
