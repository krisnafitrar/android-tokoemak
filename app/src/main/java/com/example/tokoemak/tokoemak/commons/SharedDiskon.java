package com.example.tokoemak.tokoemak.commons;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;

public class SharedDiskon {
    private SharedPreferences.Editor editor;
    public static final int DISKON = 0;
    private Context _context;
    private SharedPreferences sharedPreferences;
    private int PRIVATE_MODE = 0;
    private static final String PREF_NAME = "AndroidHivePref";

    public SharedDiskon(Context context){
        this._context = context;
        sharedPreferences = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = sharedPreferences.edit();
    }

    public void setDiskon(int diskon){
        editor.putInt( String.valueOf( DISKON ), diskon );
        editor.commit();
    }

    public HashMap<Integer,Integer> getDiskon(){
        HashMap<Integer,Integer> diskon = new HashMap<Integer, Integer>(  );
        diskon.put( Integer.valueOf( DISKON ), sharedPreferences.getInt( String.valueOf( DISKON ), 0 ) );
        return diskon;
    }


}
